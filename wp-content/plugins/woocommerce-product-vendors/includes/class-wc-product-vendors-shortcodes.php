<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Shortcodes Class. 
 *
 * Registers all shortcodes.
 *
 * @category Shortcodes
 * @package  WooCommerce Product Vendors/Shortcodes
 * @version  2.0.0
 */
class WC_Product_Vendors_Shortcodes {
	public $registration;

	/**
	 * Constructor
	 *
	 * @access public
	 * @since 2.0.0
	 * @version 2.0.0
	 * @return bool
	 */
	public function __construct() {
		$this->registration = new WC_Product_Vendors_Registration();

		add_shortcode( 'wcpv_registration', array( $this, 'render_registration_shortcode' ) );

		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

    	return true;
	}

	/**
	 * Enqueue scripts and styles
	 *
	 * @access public
	 * @since 2.0.0
	 * @version 2.0.0
	 * @return bool
	 */
	public function enqueue_scripts() {
		global $post;

		// load this script only if registration shortcode is present
		if ( is_a( $post, 'WP_Post' ) && has_shortcode( $post->post_content, 'wcpv_registration' ) ) {
			$this->registration->add_scripts();
		}

		return true;
	}

	/**
	 * Renders the registration form for new vendors
	 *
	 * @access public
	 * @since 2.0.0
	 * @version 2.0.0
	 * @param array $atts user specified attributes
	 * @return html $form
	 */
	public function render_registration_shortcode( $atts ) {
		ob_start();

		$this->registration->include_form();

		$form = ob_get_clean();

		return $form;
	}
}

new WC_Product_Vendors_Shortcodes();
