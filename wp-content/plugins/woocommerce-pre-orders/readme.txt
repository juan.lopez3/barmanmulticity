=== WooCommerce Pre-Orders ===
Author: woothemes
Requires at least: 3.8
Tested up to: 3.8.1
Stable tag: 1.2.3

== Description ==

Sell pre-orders for products in your WooCommerce store

See http://docs.woothemes.com/document/pre-orders/ for full documentation.
