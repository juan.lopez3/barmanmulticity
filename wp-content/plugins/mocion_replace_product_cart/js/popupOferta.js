/**
 * Initialize
 */
jQuery(document).ready(function ($) {

//
    var base_url = window.location.href.split("tienda/")[0];
    $("#container_oferta").hide();
    $("a.button.add_to_cart_button.ajax_add_to_cart").on('click', function (e) {

        if (typeof ($(this).parent().parent().parent().attr('data-productooferta')) !== 'undefined') {
            var producto = $(this).attr('data-product_id');
            var url = base_url + 'popup-oferta/?product_id=' + producto;
            $("div#container_oferta #content").load(url + " #oferta");
            $("div#container_oferta").show();
        }
    });
    // Get the modal
    var modal = document.getElementById('container_oferta');
    window.onclick = function (event) {
        if (event.target === modal) {
            modal.style.display = "none";
        }
    };
    // cerrar popup cuando se hace click en cerrar popup
    $("span.cerrar-popup").on('click', function (e) {
        $("div#container_oferta").hide();
        $("div#container_oferta #content").empty();
    });

    $(".festi-cart-view-cart").on('click', function(){
        $(".festi-cart-pop-up-body").addClass("loading");
        
    });


    ///////////////////////////////////////////////////////////////
    /////////////////////// CHECKOUT //////////////////////////////
    ///////////////////////////////////////////////////////////////


//    $("#content_checkout_oferta").hide();

    $(document).bind("ajaxComplete", function (event, xhr, settings) {

        // reemplazar producto oferta por producto original en carrito
        $("a.button.replace_to_cart_button.ajax_replace_to_cart").on('click', function (e) {
            e.preventDefault();
            $(this).addClass("loading");
            var producto = $(this).attr('data-product_id');
            
            $.ajax({
                url: popupoferta.ajax_url,
                type: 'post',
                data: {
                    action: 'replace_product_from_cart',
                    prod_to_remove: producto
                },
                success: function (response) {
                    //$("div#container_oferta").hide();
                    //$("div#container_oferta #content").empty();
                    // actualizar carrito
                    $(document.body).trigger('added_to_cart');
                    // actualizar checkout
                    $('body').trigger('update_checkout');
                    $("a.button.replace_to_cart_button.ajax_replace_to_cart").removeClass("loading");
                    $("a.button.replace_to_cart_button.ajax_replace_to_cart").addClass("added");
                    //$("a.button.replace_to_cart_button.ajax_replace_to_cart").removeClass("added");
                   // $("a.button.replace_to_cart_button.ajax_replace_to_cart").closest('div[class^="buy"]').hide(100);

                },
                error: function () {
                    $(this).removeClass("loading");
                }
            });
        });

// cerrar popup cuando se agrega el producto al carrito
        /*$(".container_oferta_content a.button.ajax_add_to_cart.added").each(function (index) {
            $("div#container_oferta").hide();
            $("div#container_oferta #content").empty();
        });*/

        /**
            Verify when they finish a buy n use the 
        */
        $(".container_oferta_content a.button").on('click', function(){
            $(".container_oferta_content").find(".cross-sells").removeClass("added");
            $(".container_oferta_content").find(".cross-sells").addClass("loading");
            $(this).ajaxComplete(function(){
            $(".container_oferta_content").find(".cross-sells").removeClass("loading");
            $(".container_oferta_content").find(".cross-sells").addClass("added");
            setTimeout(function(){
                $("div#container_oferta").hide();
                $("div#container_oferta #content").empty();
            },2000);
           });
        });

        //$("#content_checkout_oferta").hide();
        $("#header_checkout_oferta").on("click", function () {
            $("#content_checkout_oferta").toggle("fade");
        });

        // actualizar el checout cuanto se agrega una producto de oferta
        $("a.button.ajax_add_to_cart.added").each(function (index) {
            $('body').trigger('update_checkout');
        });
        // cerrar popup cuando se hace click en cerrar popup
        $("span.cerrar-popup").on('click', function (e) {
            $("div#container_oferta").hide();
            $("div#container_oferta #content").empty();
        });
    });
    /**
     * 
     * @returns {String}
     */
    function getBaseURL() {
        var url = location.href; // entire url including querystring - also: window.location.href;
        var baseURL = url.substring(0, url.indexOf('/', 14));
        var str = url.substring(1, url.indexOf('/', 14));
        if (url.includes("staging")) {
            return "http://barmanclub.co/staging/";
        }

        if (baseURL.indexOf('http://localhost') !== -1) {
            // Base Url for localhost
            var url = location.href; // window.location.href;
            var pathname = location.pathname; // window.location.pathname;
            var index1 = url.indexOf(pathname);
            var index2 = url.indexOf("/", index1 + 1);
            var baseLocalUrl = url.substr(0, index2);
            return baseLocalUrl + "/";
        } else {
            // Root Url for domain name
            return baseURL + "/";
        }
    }


}); //end document ready


