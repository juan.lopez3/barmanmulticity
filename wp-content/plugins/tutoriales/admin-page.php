<?php

function tutoriales_page() { ?>

    <style>

        div#contenedor_video {
            width: 60%;
            float: left;
        }

        .descripcion {
            float: right;
            display: inline;
            width: 40%;
        }

        div.contenedor_tutorial {
            display: table;
            margin-top: 30px;
            margin-bottom: 20px;
            float: left;
            margin-right: 5%;
        }
        
        div#infografias {
            margin-right: 0;
        }

    </style>

    <div class="contenedor_tutorial" id="cupones">
        <h2>CUPONES</h2>

        <div id="contenedor_video"><div id="ytplayer"></div></div>
        <div class="descripcion"></div>
    </div>

    <div class="contenedor_tutorial" id="promocion3x4">
        <h2>PROMOCIÓN 3x4</h2>

        <div id="contenedor_video"><div id="ytplayer2"></div></div>
        <div class="descripcion"></div>
    </div>

    <div class="contenedor_tutorial" id="infografias">
        <h2>INFOGRAFIAS</h2>

        <div id="contenedor_video"><div id="ytplayer3"></div></div>
        <div class="descripcion">

            <p>Pasos para agregar una infografia:</p>

            <ul>
                <li> 1- En el menu de administrador encontraremos una opcion que dice 
                    "INFOGRAFIAS", hacemos click en esa opción.</li>
                <li> 2- Agregamos un titulo, y en el campo de zip agregamos una carpeta 
                    comprimida en formato zip. Máximo puede pesar 8MB. Guardamos la infografia</li>
                <li> 3- En la edicion de una entrada, en el campo contenido agregamos la siguiente línea :

                    <p>[infografia zip="carnaval" alto="2800px" index="carnaval"]</p>

                    <p> Siendo: </br>
                        zip = nombre de nuestro archivo que subimos (requerido)</br>
                        alto = numero en pixeles del alto del contenedor</br>
                        index = nombre del archivo sin extension .html</p>

                </li>
            </ul>

        </div>
    </div>

    <script>

    // 2. This code loads the IFrame Player API code asynchronously.
        var tag = document.createElement('script');

        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    // 3. This function creates an <iframe> (and YouTube player)
    //    after the API code downloads.
        var player;

        function onYouTubeIframeAPIReady() {


            player = new YT.Player('ytplayer', {
                height: 400,
                width: 640,
                videoId: 'vr0mC0icW-Y',
            });

            player = new YT.Player('ytplayer2', {
                height: 400,
                width: 640,
                videoId: 'UjVUegctr48',
            });

            player = new YT.Player('ytplayer3', {
                height: 400,
                width: 640,
                videoId: '7U-e__Y891s',
            });

        }


        function stopVideo() {
            player.stopVideo();
        }



    </script>

    <?php

}
