<?php
/**
* Plugin Name: tutoriales
* Plugin URI: http://tutoriales.com/
* Version: 1.0 or whatever version of the plugin (pretty self explanatory)
* Author: Plugin Author's Name
* Author URI: Author's website
* License: A "Slug" license name e.g. GPL12
*/

include 'admin-page.php';

add_action( 'admin_menu', 'tutoriales_menu' );

function tutoriales_menu() {
	add_menu_page( 'Tutoriales', 'Tutoriales', 'manage_options', 'tutoriales/admin-page.php', 'tutoriales_page'  );

}