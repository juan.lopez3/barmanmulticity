<?php
/**
 * Plugin Name: mocion_validate_cupon_city
 * Description: mocion_validate_cupon_city
 * Version: 1.0
 * Author: MocionG
 */


/**
 * @todo Validar la ciudad en los cupones aplicados
 *
 * 1. Conseguir los cupones aplicados *
 * 2. conseguir la ciudad de donde se está haciendo el pedido **
 * 3. Conseguir la ciudad del cupon
 * 4. comparar la ciudad con la ciudad de los cupones
 * 5. retornar {bool} si los cupones son validos o no e imprimir mensaje
 */

function validate_copuns_per_city($valid, $coupon) {

	/**
	 * @step 1
	 */
	$coupon;
	$validCities = [];

	/**
	 * @step 2
	 */
	$ciudadEnvio = WC()->customer->get_shipping_city();

	/**
	 * @step 3
	 */
	$ciudadesID = get_group('ciudad', $coupon->id);

	if (!$ciudadesID) return true;

	foreach ($ciudadesID as $c) {
		$validCities[] = get_the_title($c['ciudad_ciudad']['1']);
	}
	/**
	 * @step 4
	 */
	if (!in_array($ciudadEnvio, $validCities)) {
		/**
		 * @step 5
		 */
		wc_add_notice("El cupon " . $coupon->code . " no es valido para esta ciudad", 'error');
		return false;
	} else {
		return true;
	}
}

add_filter('woocommerce_coupon_is_valid', 'validate_copuns_per_city', 10, 2);
