<?php
/*
Plugin Name: WooCommerce Min/Max Quantities
Plugin URI: http://woothemes.com/woocommerce
Description: Lets you define minimum/maximum allowed quantities for products, variations and orders. Requires 2.0+
Version: 2.2.8
Author: WooThemes
Author URI: http://woothemes.com
Requires at least: 3.1
Tested up to: 3.5

	Copyright: © 2009-2011 WooThemes.
	License: GNU General Public License v3.0
	License URI: http://www.gnu.org/licenses/gpl-3.0.html
*/

/**
 * Required functions
 */
if ( ! function_exists( 'woothemes_queue_update' ) )
	require_once( 'woo-includes/woo-functions.php' );

/**
 * Plugin updates
 */
woothemes_queue_update( plugin_basename( __FILE__ ), '2b5188d90baecfb781a5aa2d6abb900a', '18616' );

if ( is_woocommerce_active() ) {

	/**
	 * Localisation
	 **/
	load_plugin_textdomain( 'wc_min_max_quantity', false, dirname( plugin_basename( __FILE__ ) ) . '/' );

	/**
	 * woocommerce_min_max_quantities class
	 **/
	if ( ! class_exists( 'WC_Min_Max_Quantities' ) ) {

		class WC_Min_Max_Quantities {

			var $minimum_order_quantity;
			var $maximum_order_quantity;
			var $minimum_order_value;
			var $maximum_order_value;
			var $excludes = array();

			public function __construct() {

				if ( is_admin() )
					include_once( 'classes/class-wc-min-max-quantities-admin.php' );

				$this->minimum_order_quantity = get_option( 'woocommerce_minimum_order_quantity' );
				$this->maximum_order_quantity = get_option( 'woocommerce_maximum_order_quantity');
				$this->minimum_order_value    = get_option( 'woocommerce_minimum_order_value');
				$this->maximum_order_value    = get_option( 'woocommerce_maximum_order_value');

				// Check items
				add_action( 'woocommerce_check_cart_items', array( $this, 'check_cart_items' ) );

				// quantity selelectors (2.0+)
				add_filter( 'woocommerce_quantity_input_args', array( $this, 'fix_value' ), 10, 2 );
				add_filter( 'woocommerce_quantity_input_max',  array( $this, 'input_max' ), 10, 2 );
				add_filter( 'woocommerce_quantity_input_min',  array( $this, 'input_min' ), 10, 2 );
				add_filter( 'woocommerce_quantity_input_step',  array( $this, 'input_step' ), 10, 2 );
				add_filter( 'woocommerce_available_variation',  array( $this, 'available_variation' ), 10, 3 );

				// Prevent add to cart
				add_filter( 'woocommerce_add_to_cart_validation', array( $this, 'add_to_cart' ), 10, 4 );

				// Min add to cart ajax
				add_filter( 'woocommerce_loop_add_to_cart_link', array( $this, 'add_to_cart_link' ), 10, 2 );
		    }

		    /**
		     * Add an error
		     */
		    public function add_error( $error ) {
	    		if ( function_exists( 'wc_add_notice' ) ) {
	    			wc_add_notice( $error, 'error' );
	    		} else {
	    			global $woocommerce;
	    			$woocommerce->add_error( $error );
	    		}
	    	}

		    /**
		     * add_to_cart_link function.
		     *
		     * @access public
		     * @return void
		     */
		    public function add_to_cart_link( $html, $product ) {
			    global $woocommerce;

			    $quantity_attribute = 1;
			    $minimum_quantity   = get_post_meta( $product->id, 'minimum_allowed_quantity', true );
			    $group_of_quantity  = get_post_meta( $product->id, 'group_of_quantity', true );

			    if ( $minimum_quantity || $group_of_quantity ) {

				    $quantity_attribute = $minimum_quantity;

					if ( $group_of_quantity > 0 && $minimum_quantity < $group_of_quantity )
				    	$quantity_attribute = $group_of_quantity;

				    $html = str_replace( '<a ', '<a data-quantity="' . $quantity_attribute . '"', $html );
			    }

			    return $html;
		    }

		    /**
		     * check_cart_items function.
		     *
		     * @access public
		     * @return void
		     */
		    public function check_cart_items() {
				global $woocommerce;

				$checked_ids = $product_quantities = $category_quantities = array();
				$total_quantity = $total_cost = 0;
				$apply_cart_rules = false;

				// Count items + variations first
				foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $values ) {

					if ( ! isset( $product_quantities[ $values['product_id'] ] ) )
						$product_quantities[ $values['product_id'] ] = 0;

					if ( $values['variation_id'] ) {

						if ( ! isset( $product_quantities[ $values['variation_id'] ] ) )
							$product_quantities[ $values['variation_id'] ] = 0;

						$min_max_rules = get_post_meta( $values['variation_id'], 'min_max_rules', true );

						if ( $min_max_rules == 'yes' )
							$product_quantities[ $values['variation_id'] ] += $values['quantity'];
						else
							$product_quantities[ $values['product_id'] ] += $values['quantity'];
					} else {
						$product_quantities[ $values['product_id'] ] += $values['quantity'];
					}
				}

				// Check cart items
				foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $values ) {

					if ( $values['variation_id'] ) {
						$min_max_rules = get_post_meta( $values['variation_id'], 'min_max_rules', true );

						if ( $min_max_rules == 'yes' )
							$checking_id = $values['variation_id'];
						else
							$checking_id = $values['product_id'];

					} else {
						$checking_id = $values['product_id'];
					}

					// Get categories and counts
					$terms = get_the_terms( $values['product_id'], 'product_cat' );
					$found_term_ids = array();

					if ( $terms ) {
						foreach ( $terms as $term ) {

							if ( 'yes' == get_post_meta( $checking_id, 'minmax_category_group_of_exclude', true ) )
								continue;

							if ( in_array( $term->term_id, $found_term_ids ) )
								continue;

							$found_term_ids[] = $term->term_id;
							$category_quantities[ $term->term_id ] = isset( $category_quantities[ $term->term_id ] ) ? $category_quantities[ $term->term_id ] + $values['quantity'] : $values['quantity'];

							// Record count in parents of this category too
							$parents = get_ancestors( $term->term_id, 'product_cat' );

							foreach ( $parents as $parent ) {
								if ( in_array( $parent, $found_term_ids ) )
									continue;

								$found_term_ids[] = $parent;
								$category_quantities[ $parent ] = isset( $category_quantities[ $parent ] ) ? $category_quantities[ $parent ] + $values['quantity'] : $values['quantity'];
							}
						}
					}

					// Check item rules once per product ID
					if ( in_array( $checking_id, $checked_ids ) )
						continue;

					$product             = $values['data'];

					// Cart rules
					$minmax_do_not_count = apply_filters( 'wc_min_max_quantity_minmax_do_not_count', get_post_meta( $checking_id, 'minmax_do_not_count', true ), $checking_id, $cart_item_key, $values );
					$minmax_cart_exclude = apply_filters( 'wc_min_max_quantity_minmax_cart_exclude', get_post_meta( $checking_id, 'minmax_cart_exclude', true ), $checking_id, $cart_item_key, $values );

					if ( $minmax_do_not_count == 'yes' || $minmax_cart_exclude == 'yes' ) {
						// Do not count
						$this->excludes[] = $product->get_title();
					} else {
						$total_quantity += $product_quantities[ $checking_id ];
						$total_cost     += $product->get_price() * $product_quantities[ $checking_id ];
					}

					if ( $minmax_cart_exclude != 'yes' )
						$apply_cart_rules = true;

					$checked_ids[] = $checking_id;
					
					$minimum_quantity    = apply_filters( 'wc_min_max_quantity_minimum_allowed_quantity', get_post_meta( $checking_id, 'minimum_allowed_quantity', true ), $checking_id, $cart_item_key, $values );
					$maximum_quantity    = apply_filters( 'wc_min_max_quantity_maximum_allowed_quantity', get_post_meta( $checking_id, 'maximum_allowed_quantity', true ), $checking_id, $cart_item_key, $values );
					$group_of_quantity   = apply_filters( 'wc_min_max_quantity_group_of_quantity', get_post_meta( $checking_id, 'group_of_quantity', true ), $checking_id, $cart_item_key, $values );

					$this->check_rules( $product, $product_quantities[ $checking_id ], $minimum_quantity, $maximum_quantity, $group_of_quantity );
				}

				// Cart rules
				if ( $apply_cart_rules ) {

					$excludes = '';

					if ( sizeof( $this->excludes ) > 0 ) {
						$excludes = ' (' . __( 'excludes ', 'wc_min_max_quantity' ) . implode( ', ', $this->excludes ) . ')';
					}

					// Check cart quantity
					$quantity = $this->minimum_order_quantity;
					if ( $quantity > 0 && $total_quantity < $quantity ) {
						$this->add_error( sprintf(__('The minimum allowed order quantity is %s - please add more items to your cart', 'wc_min_max_quantity'), $quantity ) . $excludes );

						return;

					}

					$quantity = $this->maximum_order_quantity;
					if ( $quantity > 0 && $total_quantity > $quantity ) {

						$this->add_error( sprintf(__('The maximum allowed order quantity is %s - please remove some items from your cart.', 'wc_min_max_quantity'), $quantity ) );

						return;

					}

					// Check cart value
					if ( $this->minimum_order_value && $total_cost && $total_cost < $this->minimum_order_value ) {

						$this->add_error( sprintf(__('The minimum allowed order value is %s - please add more items to your cart', 'wc_min_max_quantity'), woocommerce_price( $this->minimum_order_value ) ) . $excludes );

						return;

					}

					if ( $this->maximum_order_value && $total_cost && $total_cost > $this->maximum_order_value ) {

						$this->add_error( sprintf(__('The maximum allowed order value is %s - please remove some items from your cart.', 'wc_min_max_quantity'), woocommerce_price( $this->maximum_order_value ) ) );

						return;

					}
				}

				// Check category rules
				foreach ( $category_quantities as $category => $quantity ) {
					$group_of_quantity = get_woocommerce_term_meta( $category, 'group_of_quantity', true );

					if ( $group_of_quantity > 0 && ( $quantity % $group_of_quantity ) > 0 ) {

						$term          = get_term_by( 'id', $category, 'product_cat' );
						$product_names = array();

						foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $values ) {
							if ( 'yes' == get_post_meta( $values['product_id'], 'minmax_category_group_of_exclude', true ) ||  'yes' == get_post_meta( $values['variation_id'], 'minmax_category_group_of_exclude', true ) )
								continue;
							if ( has_term( $category, 'product_cat', $values['product_id'] ) ) {
								$product_names[] = $values['data']->get_title();
							}
						}

						$this->add_error( sprintf( __( 'Items in the <strong>%s</strong> category (<em>%s</em>) must be bought in groups of %d. Please add another %d to continue.', 'wc_min_max_quantity' ), $term->name, implode( ', ', $product_names ), $group_of_quantity, $group_of_quantity - ( $quantity % $group_of_quantity ) ) );

						return;
					}
				}
		    }

		    /**
		     * check_rules function.
		     *
		     * @access public
		     * @return void
		     */
		    public function check_rules( $product, $quantity, $minimum_quantity, $maximum_quantity, $group_of_quantity ) {
			    global $woocommerce;

			    if ( $minimum_quantity > 0 && $quantity < $minimum_quantity ) {

					$this->add_error( sprintf( __( 'The minimum allowed quantity for %s is %s - please increase the quantity in your cart.', 'wc_min_max_quantity' ), $product->get_title(), $minimum_quantity ) );

				} elseif ( $maximum_quantity > 0 && $quantity > $maximum_quantity ) {

					$this->add_error( sprintf( __( 'The maximum allowed quantity for %s is %s - please decrease the quantity in your cart.', 'wc_min_max_quantity' ), $product->get_title(), $maximum_quantity ) );

				}

				if ( $group_of_quantity > 0 && ( $quantity % $group_of_quantity ) ) {

					$this->add_error( sprintf( __( '%s must be bought in groups of %d. Please add another %d to continue.', 'wc_min_max_quantity' ), $product->get_title(), $group_of_quantity, $group_of_quantity - ( $quantity % $group_of_quantity ) ) );

				}
		    }

			/**
			 * add_to_cart function.
			 *
			 * @access public
			 * @param mixed $pass
			 * @param mixed $product_id
			 * @param mixed $quantity
			 * @return void
			 */
			public function add_to_cart( $pass, $product_id, $quantity, $variation_id = 0 ) {
				global $woocommerce;

				$rule_for_variaton = false;

				if ( $variation_id ) {

					$min_max_rules    = get_post_meta( $variation_id, 'min_max_rules', true );

					if ( $min_max_rules == 'yes' ) {

						$maximum_quantity  = get_post_meta( $variation_id, 'maximum_allowed_quantity', true );
						$rule_for_variaton = true;

					} else {

						$maximum_quantity = get_post_meta( $product_id, 'maximum_allowed_quantity', true );

					}

				} else {

					$maximum_quantity = get_post_meta( $product_id, 'maximum_allowed_quantity', true );

				}

				if ( ! $maximum_quantity )
					return $pass;

				$total_quantity = $quantity;

				// Count items
				foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $values ) {
					if ( $rule_for_variaton ) {
						if ( $values['variation_id'] == $variation_id ) {
							$total_quantity += $values['quantity'];
						}
					} else {
						if ( $values['product_id'] == $product_id ) {
							$total_quantity += $values['quantity'];
						}
					}
				}

				if ( $total_quantity > 0 && $total_quantity > $maximum_quantity ) {

					if ( function_exists( 'get_product' ) ) {
						$_product = get_product( $product_id );
					} else {
						$_product = new WC_Product( $product_id );
					}

					$this->add_error( sprintf( __( 'The maximum allowed quantity for %s is %d (you currently have %s in your cart).', 'wc_min_max_quantity' ), $_product->get_title(), $maximum_quantity, $total_quantity - $quantity ) );

					$pass = false;
				}

				return $pass;
			}

			/**
			 * fix_value function.
			 *
			 * @access public
			 * @return void
			 */
			function fix_value( $data, $theproduct = null ) {
				if ( ! is_singular( 'product' ) ) {
					return $data;
				}
				if ( is_null( $theproduct ) ) {
					global $product;
					$theproduct = $product;
				}

				$group_of_quantity = get_post_meta( $theproduct->id, 'group_of_quantity', true );
				if ( $group_of_quantity ) {
					$data['input_value'] = 0;
				}
				return $data;
			}

			/**
			 * input_min function.
			 *
			 * @access public
			 * @param mixed $data
			 * @param mixed $product
			 * @return void
			 */
			function input_min( $data, $product ) {
				if ( $product->variation_id )
					$check_id = $product->variation_id;
				else
					$check_id = $product->id;

				$minimum_quantity = get_post_meta( $check_id, 'minimum_allowed_quantity', true );
				if ( $minimum_quantity ) {
					return $minimum_quantity;
				}
				$group_of_quantity = get_post_meta( $check_id, 'group_of_quantity', true );
				if ( $group_of_quantity ) {
					return $group_of_quantity;
				}
				return $data;
			}

			/**
			 * input_max function.
			 *
			 * @access public
			 * @param mixed $data
			 * @param mixed $product
			 * @return void
			 */
			function input_max( $data, $product ) {
				if ( $product->variation_id )
					$check_id = $product->variation_id;
				else
					$check_id = $product->id;

				$maximum_quantity = get_post_meta( $check_id, 'maximum_allowed_quantity', true );
				if ( $maximum_quantity ) {
					return $maximum_quantity;
				}
				return $data;
			}

			/**
			 * input_step function.
			 *
			 * @access public
			 * @param mixed $data
			 * @param mixed $product
			 * @return void
			 */
			function input_step( $data, $product ) {
				if ( is_cart() )
					return $data;

				$minimum_quantity  = get_post_meta( $product->id, 'minimum_allowed_quantity', true );
				$group_of_quantity = get_post_meta( $product->id, 'group_of_quantity', true );

				if ( $group_of_quantity && ( ! $minimum_quantity || $minimum_quantity % $group_of_quantity == 0 ) ) {
					return $group_of_quantity;
				}

				return $data;
			}

			/**
			 * available_variation function.
			 *
			 * @access public
			 * @param mixed $data
			 * @param mixed $product
			 * @return void
			 */
			function available_variation( $data, $product, $variation ) {
				$min_max_rules = get_post_meta( $variation->variation_id, 'min_max_rules', true );

				if ( $min_max_rules == 'yes' ) 
					$checking_id = $variation->variation_id;
				else
					$checking_id = $variation->id;

				$minimum_quantity  = get_post_meta( $checking_id, 'minimum_allowed_quantity', true );
				$maximum_quantity  = get_post_meta( $checking_id, 'maximum_allowed_quantity', true );
				$group_of_quantity = get_post_meta( $checking_id, 'group_of_quantity', true );

				// Only enforce min qty if set at variation level
				if ( $min_max_rules == 'yes' && $minimum_quantity ) {
					$data['min_qty'] = $minimum_quantity;
				} elseif ( $group_of_quantity ) {
					$data['min_qty'] = 0; // 0 so steps work correctly
				} else {
					$data['min_qty'] = 1;
				}

				if ( $maximum_quantity ) {
					$data['max_qty'] = $maximum_quantity;
				}

				return $data;
			}

		}

		$WC_Min_Max_quantities = new WC_Min_Max_quantities();
	}
}