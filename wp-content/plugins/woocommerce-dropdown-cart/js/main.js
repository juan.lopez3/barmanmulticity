jQuery(function($){
    jQuery('.widget_shopping_mini_cart_content').on('click', '.dropdown-total', function(){
        jQuery(this).next().slideToggle();
        return false;
    });

    jQuery('body').bind('adding_to_cart', function(){
        jQuery('.widget_shopping_mini_cart_content').show();
    });

    jQuery('body').bind('added_to_cart', function(){
        jQuery('.widget_shopping_mini_cart_content').addClass('loading');
        var this_page = window.location.toString();
        this_page = this_page.replace( 'add-to-cart', 'added-to-cart' );

        jQuery('.widget_shopping_mini_cart_content').load( this_page + ' .dropdown-cart-button', function() {
            jQuery('.widget_shopping_mini_cart_content').removeClass('loading');
        });
    });
    
    //Estos parametros se pasan con wp_localize_script en dropdown-cart.php
	jQuery(document).ready(function(){
		
		if (typeof dropdownCartParams  != 'undefined' && dropdownCartParams['cart_dropdown']){
			$('#'+dropdownCartParams['cart_id']).hide();
		}
	});
			

    
});


