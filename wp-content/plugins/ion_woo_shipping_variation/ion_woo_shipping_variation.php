<?php

/**
 * Plugin Name: 	Mocion Woocommerce variation-shipping
 * Description: 	syncronize variations and shipping methods to allow product price per shipping method
 * Version: 		0.0.1
 * Author: 		mocion
 * Author URI: 		http://mocionsoft.com/
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly
    
define('SHIPPING_VARIATION_PER_CITY_PATH', plugin_dir_path(__FILE__));
require_once SHIPPING_VARIATION_PER_CITY_PATH . 'Woo_connect_product_variations_with_shipping_methods.class.php';
require_once SHIPPING_VARIATION_PER_CITY_PATH . 'citiesdropdown/citiesdropdown.php';
require_once SHIPPING_VARIATION_PER_CITY_PATH . 'available_variations.php';

/**
 * syncronize variations and shipping methods to allow product price per shipping method
 * @class		WooCommerce_Variation_Shipping
 */
class WooCommerce_Variation_Shipping {

    public $file = __FILE__;
    private static $instance;
    private $shipping_taxonomy_name = 'product_shipping_class';
    private $shipping_method_post_type = 'was';
    private $product_attribute_shipping_name = 'pa_shipping';

    public function __construct() {

        // Check if WooCommerce is active
        if (!function_exists('is_plugin_active_for_network')) :
            require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
        endif;

        if (!in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) :
            if (!is_plugin_active_for_network('woocommerce/woocommerce.php')) :
                return;
            endif;
        endif;

        // Initialize plugin parts
        $this->init();
    }

    /**
     * Instance.
     * An global instance of the class. Used to retrieve the instance to use on other files/plugins/themes
     * @return object Instance of the class.
     */
    public static function instance() {

        if (is_null(self::$instance)) :
            self::$instance = new self();
        endif;
        return self::$instance;
    }

    /**
     * Init.
     * Initialize plugin parts.
     */
    public function init() {
        // Add hooks/filters
        
        $syncquer = new Woo_connect_product_variations_with_shipping_methods($this->shipping_taxonomy_name, $this->shipping_method_post_type, $this->product_attribute_shipping_name);
        $this->init_sync_shipping_and_prod_variations_action_hooks($syncquer);
        
        $this->init_action_hooks();
        //require_once plugin_dir_path( __FILE__ ) . '/includes/class-was-match-conditions.php';
        //$this->matcher = new WAS_Match_Conditions();
    }


	private function init_sync_shipping_and_prod_variations_action_hooks($sync){

        //sync shipping class and shipping method
        add_action('save_post_' . $this->shipping_method_post_type, 
                                    array($sync, 'sync_shippingclass_and_productatribute'));
        add_action('wp_trash_post', array($sync, 'sync_trash_shipping_class'));
        add_action('trash_post',    array($sync, 'sync_trash_shipping_class'));
        add_action('delete_post',   array($sync, 'sync_trash_shipping_class'));		
		
	}
    /**
     * Hooks.
     * Initialize plugin hooks like actions and filters.
     * //add_action('woocommerce_checkout_update_order_review', array($this, 'checkout_review_variation_update'));
     */
    private function init_action_hooks() {

        add_action('wp_enqueue_scripts', array($this, 'ion_enqueue_scripts'));
        // Same handler function...
        add_action('wp_ajax_change_product_variation', array($this, 'change_product_variation_callback'));

        //Se invoca en el WC_AJAX::update_order_review
        //Cambiamos como se muestra la variacion del producto de un texto a un dropdown, para poder cambiarlo en el carrito        
        add_filter('woocommerce_get_item_data', array($this, 'add_variation_select_to_product'), 10, 2);
        add_filter('woocommerce_dropdown_variation_attribute_options_args', array($this, 'filter_dropdown_variation_attribute_options_by_city'), 10, 1);
        
        
        
        //Que hace esto?
        add_filter('woocommerce_product_is_visible', array($this, 'filtrar_visibilidad_productos'), 10, 2); 
        /**
         * @TODO @BLOCKED se comento por pruebas
         * @TODO @BLOCKED revisar el ajax cuando termina de cargar el carrito
         * 17-NOV-2016
         */
        //add_action('woocommerce_checkout_update_order_review', array($this, 'checkout_update_available_shippment_per_city'));
        

        //Ocultamos el rango variacion en el precio, por defecto se muestra el rango de precio para todas las variaciones, a petición del cliente
        add_filter('woocommerce_variable_sale_price_html', array($this, 'barman_alter_variation_sale_price'), 10, 2);
        add_filter('woocommerce_variable_price_html',      array($this, 'barman_alter_variation_price'), 10, 2);
        
        //Estetico quitamos un link que no deberia verse
        add_filter('woocommerce_reset_variations_link', array($this, 'remove_woocommerce_reset_variations_link'));

		//a cada tipo de envio agregamos los terminos y condiciones que fueron añadidos usando magic_fields en el tipo de contenido perteneciente al tipo de envio
        add_filter('woocommerce_shipping_package_name', array($this, 'agregar_terminos_y_condiciones_al_tipo_de_envio'), 10, 3);
                
		//Si dos variaciones tienen el mismo precio extrañamente no se muestra el precio del producto
        add_filter('woocommerce_available_variation', array($this, 'mostar_precio_para_variaciones_conel_mismo_precio'), 10, 3);
       
    }

    function mostar_precio_para_variaciones_conel_mismo_precio($value, $object = null, $variation = null) {
        if ($value['price_html'] == '') {
            $value['price_html'] = '<span class="price">' . $variation->get_price_html() . '</span>';
        }
        return $value;
    }

    //Ocultamos productos que no tienen una variacion(metodo de entrega) para la ciudad actual
    function filtrar_visibilidad_productos($visible, $id) {
        $product = new WC_Product_Variable($id);
        $all_variaciones = $product->get_available_variations();
        $variaciones = $this->filter_available_shipping_variations($all_variaciones);
        return (!empty($variaciones));
    }

//Ocultamos el rango variacion en el precio
    function barman_alter_variation_sale_price($price, $product) {  
        if ($product->product_type === 'variable') {

            $variacion = $this->seleccion_variacion_con_precio_mas_barato($product);
            //$precio = $this->seleccion_precio_mas_barato_variaciones($product);
            if ($variacion['display_price'] == $variacion['display_regular_price']){
                return wc_price($variacion['display_price']);
            }else{
              return '<del><span class="amount">&#36;&nbsp;'.$variacion['display_regular_price']
                      .'</span></del> <ins><span class="amount">&#36;&nbsp;'.$variacion['display_price'].'</span></ins>';
            }
        }
        return $price;
    }

//Ocultamos el rango variacion en el precio y seleccionamos el precio más barato para la ciudad
    function barman_alter_variation_price($price, $product) {
        if ($product->product_type === 'variable') {

            $variacion = $this->seleccion_variacion_con_precio_mas_barato($product);
            //$precio = $this->seleccion_precio_mas_barato_variaciones($product);
            if ($variacion['display_price'] == $variacion['display_regular_price']){
                return wc_price($variacion['display_price']);
            }else{
              return '<del><span class="amount">&#36;&nbsp;'.$variacion['display_regular_price']
                      .'</span></del> <ins><span class="amount">&#36;&nbsp;'.$variacion['display_price'].'</span></ins>';
            }
        }
        return $price;
    }

    public function seleccion_variacion_con_precio_mas_barato($product) {
        $precio = null;
        $variacion_seleccionada = null;
        $all_variaciones = $product->get_available_variations();

        $variaciones = $this->filter_available_shipping_variations($all_variaciones);

        //Escogemos el menor precio siempre por ciudad
        foreach ($variaciones as $variacion) {
            if ($variacion['display_price'] < $precio || $precio == null) {
                $precio = $variacion['display_price'];
                $variacion_seleccionada = $variacion;
            }
        }

        return $variacion_seleccionada;
    }

    private function seleccion_precio_mas_barato_variaciones($product) {
        $precio = null;
        $variacion_seleccionada = null;
        $all_variaciones = $product->get_available_variations();
        $variaciones = $this->filter_available_shipping_variations($all_variaciones);
        //Escogemos el menor precio siempre por ciudad
        foreach ($variaciones as $variacion) {
            if ($variacion['display_price'] < $precio || $precio == null) {
                $precio = $variacion['display_price'];
                $variacion_seleccionada = $variacion;
            }
        }
        /*
          $primera_variacion =  reset($variaciones);
          $variacion_id = $primera_variacion['variation_id'];
          foreach($variaciones as $variacion){
          if ($variacion['variation_id'] == $product['variation_id']){
          $variacion_id = $product['variation_id'];
          }
          }
         */
        return $precio;
    }

    /**
      //La información esta en el shipping-method
      //Aqui estan son los paquetes
      //Cargar el shipping-method a partir del shipping-package
     * @param String $name
     * @param Integer $i
     * @param WC_Package $package
     * @return null
     */
    function agregar_terminos_y_condiciones_al_tipo_de_envio($name, $i, $package) {
        if (!isset($package['package_grouping_rule']))
            return $name;
        //ALgunas veces es un array no entiendo porque
        $grouping_rule = (is_array($package['package_grouping_rule'])) ? reset($package['package_grouping_rule']) : $package['package_grouping_rule'];

        if (empty($package['package_grouping_rule']) || $grouping_rule !== "shipping-class"):return $name;
        endif;
        //ALgunas veces es un array no entiendo porque
        $shippingclass_name = (is_array($package['package_grouping_value'])) ? reset($package['package_grouping_value']) : $package['package_grouping_value'];

        //traemos el post que representa el metodo de envio relacionado con el shipping_class
        $condiciones = array(
            'name' => $shippingclass_name,
            'post_type' => 'was',
        );
        $resultado = get_posts($condiciones);

        if (!$resultado):return $name;
        endif;

        $metodo = reset($resultado);
        $descripcion = get_post_custom_values('descripcion', $metodo->ID);
        $terminos = get_post_custom_values('terminosycondiciones', $metodo->ID);
        $terminos = (is_array($terminos) && !empty($terminos)) ? reset($terminos) : "";

        $condiciones = '<div id="' . $shippingclass_name . '" class="popup-condiciones-entrega">
        <div class="cerrar-condiciones">x</div>
        <div class="condiciones-entrega-content">
           <h4>Terminos y Condiciones de ' . $shippingclass_name . ' </h4>
              <div class="terminos_contenido"><pre>' . $terminos . '</pre></div>
        </div>
    </div>';

        if (!array($descripcion) || empty($descripcion)):return $name;
        endif;

        $descripcion = reset($descripcion);
        $descripcion_html = "<span class='resumen-metodo-envio'>" . $descripcion . "</span>"
                . "<span id='" . $shippingclass_name . "' class='condiciones-entrega checkout'> Ver Condiciones de entrega</span>$condiciones";

        return $descripcion_html;
    }

    function remove_woocommerce_reset_variations_link($link) {
        $link = "";
        return $link;
    }

    /**
     * 
     * @param type $p1 ni idea que variable es la primera
     * @param WC_Product_Variable $variation
     */
    function alter_variation_dropdown($p1, $variation) {
        
    }

    /* add_filter( 'woocommerce_dropdown_variation_attribute_options_args', 'mmx_remove_select_text');
      function mmx_remove_select_text( $args ){

      return $args;
      } */

    function filter_dropdown_variation_attribute_options_by_city($variaton_options) {

        //quitamos la opcion por defecto que no trae ningun valor
        $variaton_options['show_option_none'] = '';


        //Filtrar por ciudad las variaciones
        $producto = $variaton_options['product']; //WC_Product_Variable
        $all_variaciones = $producto->get_available_variations();

        $variacion_seleccionada = null;

        $variaciones_disponibles = $this->filter_available_shipping_variations($all_variaciones);
        $opciones_disponibles = array();

        foreach ($variaciones_disponibles as $variacion) {

            $opciones_disponibles[] = $variacion['attributes']['attribute_pa_shipping'];
            if ($variacion_seleccionada == null || $variacion['display_price'] < $variacion_seleccionada['display_price']) {
                $variacion_seleccionada = $variacion;
            }
        }

        $variaton_options['options'] = array_intersect($variaton_options['options'], $opciones_disponibles);
        $opcion_seleccionada = reset($variaton_options['options']);
        if ($variacion_seleccionada) {
            $opcion_seleccionada = $variacion_seleccionada['attributes']['attribute_pa_shipping'];
        }


        if ($opcion_seleccionada):
            $variaton_options['selected'] = $opcion_seleccionada;
        endif;

        return $variaton_options;
    }

    function change_product_variation_callback() {
        ad("a cambiar la variacion");
        $variation_id = trim($_POST['variation_id']);
        $posicion = trim($_POST['posicion']);
        $producto_id = trim($_POST['product_id']);

        $product_key = $this->change_product_variation_in_cart($producto_id, $variation_id, true, $posicion);

        wp_die();
    }

    function ion_enqueue_scripts() {
        wp_register_script('barmancart', plugin_dir_url(__FILE__) . '/barman_cart.js', array('jquery'), '', true);
        // in JavaScript, object properties are accessed as ajax_object.ajax_url, ajax_object.we_value
        wp_localize_script('barmancart', 'barmancart', array('ajax_url' => admin_url('admin-ajax.php'), 'we_value' => 1234));
        wp_enqueue_script('barmancart');
    }

    
    function product_not_in_this_city(){
        $mensaje = '<div class="alert alert-warning alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert">
                <span aria-hidden="true">×</span>
                <span class="sr-only">Cerrar</span>
            </button>
            <strong>
                Por el momento distribución de productos únicamente en Bogotá, Medellín y  Cali.
                <!-- <span class="condiciones-entrega">Ver condiciones de entrega</span> -->
            </strong>
        </div>';

        echo $mensaje;
    }
    
    function add_variation_select_to_product($item_data, $cart_item) {
        //d(WC()->cart->get_applied_coupons());
        //d($cart_item);
        if (!isset($cart_item['variation_id'])):return;
        endif;


        //get_available_variations
        $producto = new WC_Product_Variable($cart_item['product_id']);
        $all_variaciones = $producto->get_available_variations();
        $variaciones = $all_variaciones;
        $variaciones = $this->filter_available_shipping_variations($all_variaciones);

        $variaciones = filter_available_shipping_variations_by_coupons($variaciones);

        $output = '';

        //Sino existen variaciones disponibles para la ciudad actual borramos el producto del carrito<div class="alert alert-warning alert-dismissible fade in" role="alert">

        
        if (empty($variaciones)) {

            foreach (WC()->cart->cart_contents as $key => &$producto_carro) {
                if ($cart_item['product_id'] != $producto_carro['product_id'])
                    continue;

                WC()->cart->remove_cart_item($key);
                //add_action( 'admin_notices', 'product_not_in_this_city' );
            }

            return $item_data;
        }
        //
        foreach ($variaciones as $variacion) {
            $selected = ($cart_item['variation_id'] == $variacion['variation_id']) ? ' selected="selected "' : " ";
            $output .= '<option ' . $selected . ' value="' . $variacion['variation_id'] . '">' . $variacion['attributes']['attribute_pa_shipping'] . '</option>';
        }
        $output = '<select name="cart_product_variation[' . $cart_item['product_id'] . ']" class="cart_product_variation" data-product_id="' . $cart_item['product_id'] . ' ">' . $output . '</select>';

        if (isset($item_data[0])) {
            $item_data[0] = array(
                'key' => 'shipping',
                'display' => $output
            );
        }

        //print "<pre>";
        //var_dump($all_variaciones);
        //print "<pre>";
        //die;

        return $item_data;
    }

    function filter_available_shipping_variations($variaciones) {

        $d = array();
        $matched_variaciones = array();

        $d['variaciones'] = $variaciones;
        //nos asegura que el metodo was_method exista ytenga una referencia a class-was-method
        WooCommerce_Advanced_Shipping()->was_shipping_method();

        $methods = get_posts(array('posts_per_page' => '-1', 'post_type' => 'was', 'orderby' => 'menu_order', 'order' => 'ASC'));

        foreach ($methods as $method) :

            $condition_groups = get_post_meta($method->ID, '_was_shipping_method_conditions', true);

            /* La condición de shipping_class se usa para crear una relacion uno a uno
             * entre el metodo de envio y la shipping class, y a su vez 
             * la shipping class se usa como intermediaria para conectar el metodo 
             * de envio con una variación del producto en particular.
             * Esta condición la quitamos para poder cargar todos los metodos de envio
             * disponibles para una ciudad.
             */
            foreach ($condition_groups as $index_group => $condition_group) {
                $d[] = $condition_group;
                foreach ($condition_group as $index_condition => $condition) {

                    if ($condition['condition'] == "contains_shipping_class") {
                        unset($condition_groups[$index_group][$index_condition]);
                    }
                }
            }
            $d['condition_groups'] = $condition_groups;


            $package = null;
            // Check if method conditions match to be shown
            $match = WooCommerce_Advanced_Shipping()->was_method->was_match_conditions($condition_groups, $package);
            if (true != $match): continue;
            endif;

            $d['method'] = $method;
            $term_key = 'pattribute_id';
            //Cada package tiene su propia seleccion de de metodo de envio
            //Cargamos el shipping_class relacionado con el metodo de envio
            $shipping_class_id = get_post_meta($method->ID, $term_key, true);

            if (!$shipping_class_id):continue;
            endif;
            $d['shipping_class_id'] = $shipping_class_id;
            //Cargamos el atributo del producto, que relaciona al producto y un  metodo de envio: EJ: inmediato bogotá
            $atributo = get_term($shipping_class_id, $this->product_attribute_shipping_name);
            $varacion_relacionada_nombre = $atributo->slug;
            //$atributo->term_id
            foreach ($variaciones as $variacion) {
                if ($variacion['attributes']['attribute_pa_shipping'] == $varacion_relacionada_nombre) {
                    $matched_variaciones[] = $variacion;
                }
            }
            $d['atributo'] = $atributo;

        endforeach;
        //ad($d);

        return $matched_variaciones;
    }
    
    
	
    function checkout_update_available_shippment_per_city($post_data) {
		
		

        $post_data_parsed = new stdClass();
        $d = explode('&', $post_data);

        foreach ($d as $data) {
            $data = urldecode($data);
            if (false !== strpos($data, '=')) {
                list( $name, $value ) = explode('=', $data);
                $post_data_parsed->$name = $value;
            }
        }

        $old_city = WC()->customer->get_city();
        $new_city = $post_data_parsed->billing_city;

        //if ($old_city == $new_city): return;
        //endif;

        WC()->customer->set_shipping_city($new_city);

        $posicion = -1;
        foreach (WC()->cart->cart_contents as $product) {
            $posicion++;
            //Conseguir variaciones activas
            if (!isset($product['variation_id'])):continue;
            endif;

            $producto = new WC_Product_Variable($product['product_id']);
            $all_variaciones = $producto->get_available_variations();
            $variaciones = $this->filter_available_shipping_variations($all_variaciones);
            $primera_variacion = reset($variaciones);

            $variacion_id = $primera_variacion['variation_id'];
            foreach ($variaciones as $variacion) {
                if ($variacion['variation_id'] == $product['variation_id']) {
                    $variacion_id = $product['variation_id'];
                }
            }
            //actualizar el producto en el carrito
            $this->change_product_variation_in_cart($product['product_id'], $variacion_id, false, $posicion);
        }
    }

    function change_product_variation_in_cart($producto_id, $variacion_id, $forzar_actualizacion = true, $posicion = null) {

        $llave = null;
        $i = -1;

        foreach (WC()->cart->cart_contents as $key => &$producto_carro) {
            $i++;
            if ($producto_id != $producto_carro['product_id']):continue;
            endif;
            //Esta condicion se agrego por si el producto existe más de una vez en el carrito
            if ($posicion !== null && (intval($posicion) != $i)):continue;
            endif;

            //Actualizamos la variacion que tiene el producto en el carrito
            $nueva = new WC_Product_Variation($variacion_id);
            $attributos = $nueva->get_variation_attributes();

            $producto_carro["variation_id"] = $variacion_id;
            $producto_carro['variation']["attribute_pa_shipping"] = $attributos["attribute_pa_shipping"];

            $producto_carro['data'] = $nueva;
            $llave = $key;
        }

        //Algunas veces el metodo de arriba no persiste, lo hacemos más a las malas   
        if ($forzar_actualizacion && $llave) {
            WC()->cart->remove_cart_item($llave);
            //WC()->cart->add_to_cart( $producto_id, 1, $variation_id, $attributos); 
            WC()->cart->restore_cart_item($llave);
        }

        return $llave;
    }

    public function mensaje_atributo_shipping_no_existe() {
        $this->print_admin_messages("el atributo shipping de los productos no existe aún, creelo porfavor");
    }

    private function print_admin_messages($mensaje, $type = 'error') {

        $class = 'notice notice-' . $type . ' is-dismissible';
        printf('<div class="%1$s"><p>%2$s</p></div>', $class, $mensaje);
    }

}

/**
 * The main function responsible for returning the WooCommerce_Variation_Shipping object.
 * Use this function like you would a global variable, except without needing to declare the global
 * Example: <?php WooCommerce_Variation_Shipping()->method_name(); ?>
 * @return object WooCommerce_Variation_Shipping class object.
 */
if (!function_exists('WooCommerce_Variation_Shipping')) :

    function WooCommerce_Variation_Shipping() {
        return WooCommerce_Variation_Shipping::instance();
    }

endif;

WooCommerce_Variation_Shipping();
