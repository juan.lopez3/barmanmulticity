<?php 

Class Woo_connect_product_variations_with_shipping_methods {
	
	public $file = __FILE__;
    private $shipping_taxonomy_name;
    private $shipping_method_post_type;
    private $product_attribute_shipping_name;

    
    public function __construct($shipping_taxonomy_name, $shipping_method_post_type, $product_attribute_shipping_name){
		$this->shipping_taxonomy_name = $shipping_taxonomy_name;
		$this->shipping_method_post_type = $shipping_method_post_type;
		$this->product_attribute_shipping_name = $product_attribute_shipping_name;
	}
    
    
    public function sync_shippingclass_and_productatribute($post_id, $post, $update) {
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) :return;
        endif;
        // verify this came from the our screen and with proper authorization,
        // because save_post can be triggered at other times
        if (!$_POST): return;
        endif;

        $shipping_method_name = get_the_title($post_id);
        //Sin nombre no podemos seguir
        if (!$shipping_method_name): return;
        endif;

        $resultado1 = $this->sync_shipping_class($post_id, $shipping_method_name);
        $resultado2 = $this->sync_product_attribute($post_id, $shipping_method_name);
    }

    private function sync_shipping_class($post_id, $name) {
        $taxonomy = $this->shipping_taxonomy_name;
        $term_key = 'shipping_class_id';
        $resultado = $this->sync_taxonomyterm_with_shippment($post_id, $name, $taxonomy, $term_key);

        return $resultado;
    }

    private function sync_product_attribute($post_id, $name) {

        $taxonomy = $this->product_attribute_shipping_name;
        $term_key = 'pattribute_id';
        $resultado = $this->sync_taxonomyterm_with_shippment($post_id, $name, $taxonomy, $term_key);
        return $resultado;
    }

    private function sync_taxonomyterm_with_shippment($post_id, $name, $taxonomy, $term_key) {

        $shipping_class_id = get_post_meta($post_id, $term_key, true);
        $resultado = $this->sync_term($name, $taxonomy, $shipping_class_id);

        //por si no existe el termino referido por $shipping_class_id
        $shipping_class_id = null;
        if (gettype($resultado) == 'object' && get_class($resultado) == 'WP_Error') {
            $resultado = $this->sync_term($name, $taxonomy, $shipping_class_id);
        }

        $resultado_meta = null;
        if (isset($resultado['term_id'])) {
            $resultado_meta = update_post_meta($post_id, $term_key, $resultado['term_id']);
        } else {
            return $resultado;
        }

        return $resultado_meta;
    }

    private function sync_term($name, $taxonomy, $shipping_class_id) {

        $slug = preg_replace("/[^A-Za-z0-9 ]/", '-', $name);

        if ($shipping_class_id) {
            $resultado = wp_update_term($shipping_class_id, $taxonomy, array(
                'name' => $name,
                'slug' => $slug
            ));
        } else {
            $resultado = wp_insert_term(
                    $name, $taxonomy, array(
                'description' => 'shipping method sync',
                'slug' => $slug
                    )
            );
        }
        return $resultado;
    }

    public function sync_trash_shipping_class($post_id) {
        $post_type = get_post_type($post_id);

        if ($post_type != $this->shipping_method_post_type): return;
        endif;

        $shipping_class_id = get_post_meta($post_id, 'shipping_class_id', true);
        if (!$shipping_class_id): return;
        endif;

        return wp_delete_term($shipping_class_id, $this->shipping_taxonomy_name);
    }	
}



/*
 * 
 *     function checkout_review_variation_update($post_data) {


        return;
        $d = array();
        $post_data_parsed = new stdClass();

        $products_post = array();
        //cart_product_variation[4453] = "4960"

        $post_data = explode('&', $post_data);

        foreach ($post_data as $data) {
            $data = urldecode($data);
            if (false !== strpos($data, '=')) {
                list( $name, $value ) = explode('=', $data);

                $compuesto = array();
                preg_match("#(.*?)\[(.*?)\]#", $name, $compuesto);

                if (empty($compuesto)) {
                    $post_data_parsed->$name = $value;
                } else {
                    $propiedad = $compuesto[1];
                    $llave = $compuesto[2];
                    $post_data_parsed->{$propiedad}[$llave] = $value;
                }
            }
        }
        $d["parsed"] = $post_data_parsed;

        foreach (WC()->cart->cart_contents as $product) {
            $variacion_vieja_id = $post_data_parsed->cart_product_variation[$product['product_id']];
            if ($product['variation_id'] == $variacion_vieja_id):continue;
            endif;

            $_POST['post_data'] = "";
            $this->change_product_variation_in_cart($product['product_id'], $product['variation_id'], false);
            $d["acambiarlo"] = $_POST['post_data'];
        }


        ad($d);
    }
 */
