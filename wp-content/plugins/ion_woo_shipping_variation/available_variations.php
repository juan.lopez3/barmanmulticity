<?php

function filter_available_shipping_variations_by_coupons($variaciones) {

    $coupons_used = WC()->cart->get_applied_coupons();

    if (empty($coupons_used)): return $variaciones;endif;

    $coupons = array();
    foreach ($coupons_used as $key => $value) {
        $args = array(
            's' => $value,
            'post_type' => 'shop_coupon',
            'post_status' => 'publish',
        );
        $coupons[$value] = get_posts($args);
    }

    $promocions = array();
    foreach ($coupons as $coupon) {
        $promocions[$coupon[0]->post_title] = array(
            'promocion_cupon_field' => get_post_meta($coupon[0]->ID, 'promocion_cupon_field', true),
        );
    }

    $correos = array();
    foreach ($promocions as $promocion) {
        if ($promocion["promocion_cupon_field"] != null) {
            $correosPromocionString = get_post_meta($promocion["promocion_cupon_field"], 'correo', true);
            $correosPromocionString = str_replace(' ', '', $correosPromocionString);

            $correosPromocion = explode(",", $correosPromocionString);
            $correos = array_merge($correos, $correosPromocion);
        }
    }

    $esEcolicores = false;

    foreach ($correos as $correo) {
        if (strpos($correo, 'ecolicores') !== FALSE) {
            $esEcolicores = true;
        }
    }


    if ($esEcolicores):
        foreach ($variaciones as $key => $variacion) {
            $variacion['attributes']['attribute_pa_shipping'];
            if (strpos($variacion['attributes']['attribute_pa_shipping'], 'inmediato')) {
                unset($variaciones[$key]);
            }
        }
    endif;
    return $variaciones;
}
