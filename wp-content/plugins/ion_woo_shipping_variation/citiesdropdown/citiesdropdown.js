(function ($, window) {
    $(document).on('ready', function () {

        /* Para sincronizar la ciudad del checkout con el dropdown de ciudad */
        $(document).on("change", "#billing_city", function () {
            $ciudad = $('#billing_city').val();
            set_nombre_ciudad_dropdown($ciudad);
        });
        
        /*
         * Dropdown
         --------------------------------------------------------- */

        var dropdown = $('.ui.selection.dropdown');
        if (dropdown) {

            var ajaxurl = dropdown.data('ajaxurl');
            var ciudadItem = dropdown.find('.item');
            //Mostramos u ocultamos el dropdown 
            dropdown.on('click', function () {
                $(this).toggleClass('active');
                $(this).find('.menu').toggleClass('visible');
            });
            //Seleccionamos una nueva ciudad
            ciudadItem.on('click', function () {
                var value = $(this).data('value');
                var reload = $(this).data('reload');
                var icon = dropdown.find('.marker');

                dropdown.addClass('loading');
                icon.hide();

                set_city_ajax(value, ajaxurl, reload).then(function (result) {
                    console.log('result:', result);
                    set_nombre_ciudad_dropdown(value);
                    dropdown.removeClass('loading');
                    icon.show();
                });
            });

            function set_nombre_ciudad_dropdown(val) {
                dropdown.find('span.nombre-seleccion').html(val);
            }

        }//end if dropdown


    });

    function set_city_ajax(val, ajaxurl, reload) {
        var deferred = $.Deferred();
        var promise = deferred.promise();

        $.post(ajaxurl, {
            action: 'nueva_ciudad',
            ciudad: val
        }, function (res) {
            deferred.resolve(res);
            if (reload)
                location.reload();
        });

        return promise;
    }
})(jQuery, window);
