# Copyright (C) 2013 
# This file is distributed under the same license as the  package.
msgid ""
msgstr ""
"Project-Id-Version:  \n"
"Report-Msgid-Bugs-To: http://wordpress.org/tag/woocommerce-shipping-local-pickup-plus\n"
"POT-Creation-Date: 2013-12-29 09:34:18+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2013-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"

#: classes/class-wc-shipping-local-pickup-plus.php:65
msgid "Local Pickup Plus"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:67
msgid "Local pickup is a simple method which allows the customer to pick up their order themselves at a specified pickup location."
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:137
msgid "from"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:143
msgid "save %s"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:224
msgid "Enable"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:226
msgid "Enable local pickup plus"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:231
msgid "Title"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:233
msgid "This controls the title which the user sees during checkout."
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:234
msgid "Local Pickup"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:238
#: classes/class-wc-shipping-local-pickup-plus.php:333
msgid "Cost"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:240
msgid "Default cost excluding tax. Enter an amount, e.g. 2.50, or leave empty for no default cost.  The default cost can be overriden by setting a cost per pickup location below."
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:245
msgid "Discount"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:247
msgid "Discount for choosing Local Pickup as the shipping option.  Enter an amount, e.g. 2.50 to discount the order total (post-tax) when this shipping method is used."
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:252
msgid "Categories"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:254
msgid "Optional, these categories of products may only be locally picked up."
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:260
msgid "Categories Only"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:262
msgid "Allow local pickup only for the product categories listed above, all other product categories must be shipped via another method"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:267
msgid "Pickup Location Tax"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:269
msgid "When this shipping method is chosen, apply the tax rate based on the pickup location than for the customer's given address."
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:274
msgid "Hide Shipping Address"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:276
msgid "Hide the shipping address when local pickup plus is selected at checkout."
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:321
msgid "Pickup Locations"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:332
msgid "Phone"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:333
msgid "Cost for this pickup location, enter an amount, eg. 0 or 2.50, or leave empty to use the default cost configured above."
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:338
msgid "+ Add Pickup Location"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:339
msgid "Delete Pickup Location"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:369
#: classes/class-wc-shipping-local-pickup-plus.php:374
#: classes/class-wc-shipping-local-pickup-plus.php:375
#: classes/class-wc-shipping-local-pickup-plus.php:412
#: classes/class-wc-shipping-local-pickup-plus.php:417
#: classes/class-wc-shipping-local-pickup-plus.php:418
msgid "(Optional)"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:426
msgid "Delete the selected pickup locations?"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:575
msgid "Your Pickup Location"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:575
msgid "Choose Pickup Location"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:588
msgid "Choose One"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:649
msgid "Please select a local pickup location"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:662
msgid "Please refresh the page and select a different local pickup location"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:721
msgid "Some of your cart products are not eligible for local pickup, please remove %s, or select a different shipping method to continue"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:727
msgid "Some of your cart products are only eligible for local pickup, please remove %s, or change the shipping method to %s to continue"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:767
#: classes/class-wc-shipping-local-pickup-plus.php:769
msgid "Free"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:816
#: classes/class-wc-shipping-local-pickup-plus.php:837
#: classes/class-wc-shipping-local-pickup-plus.php:869
#: classes/class-wc-shipping-local-pickup-plus.php:876
msgid "Pickup Location"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:869
msgid "Edit"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:918
msgid "Pickup location changed to %s"
msgstr ""

#: classes/class-wc-shipping-local-pickup-plus.php:1088
msgid "Address 2"
msgstr ""

#: shipping-local-pickup-plus.php:544
msgid "WooCommerce Local Pickup Plus"
msgstr ""