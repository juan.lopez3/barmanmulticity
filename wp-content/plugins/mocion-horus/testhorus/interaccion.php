<html>
    <head>
        <meta charset="UTF-8">
        <title>Requests PHP</title>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    </head>
    <body ng-app='body' ng-controller='ctrl'>
        <form method="POST" action="request.php" style="width: 60%; margin: auto;">
            <input type="hidden" name='action' value='interaccion'>
            <div class="form-group">
                <input type="input" class="form-control" name="id" placeholder="id">
            </div>

            <button type='submit' class="btn btn-success btn-block">Disparar interaccion</button>
        </form>
    </body>
</html>