<html>
    <head>
        <meta charset="UTF-8">
        <title>Requests PHP</title>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script>
        <script>
            angular.module('body', []).controller('ctrl', function($scope) {
                $scope.data = { firstName: "text", lastName: "text", email: "email", hash: 'password', birthday: "date", identifyNumber: "number", gender: 'text' }
            });
        </script>
    </head>
    <body ng-app='body' ng-controller='ctrl'>
        <form method="POST" action="request.php" style="width: 60%; margin: auto;">
            <input type="hidden" name='action' value='register'>
            <div class="form-group" ng-repeat="(key, item) in data">
                <input type="{{item}}" class="form-control" name="{{key}}" placeholder="{{key}}">
            </div>
            <button type='submit' class="btn btn-success btn-block">Register</button>
        </form>
    </body>
</html>