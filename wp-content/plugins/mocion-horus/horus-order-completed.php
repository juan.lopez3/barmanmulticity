<?php

add_action("woocommerce_order_status_processing", "horus_processing_order");

function horus_processing_order($order_id) {

    error_log("Payment has been received for order $order_id", 0);

    $order = new WC_Order($order_id);
    $myuser_id = (int) $order->user_id;
    $user_info = get_userdata($myuser_id);
    $items = $order->get_items();
    foreach ($items as $item) {
        $product = procesar_interaccion_compra_producto($item);
        error_log("Producto " . $item['product_id'] . "  " . json_encode($product), 0);
    }
    return $order_id;
}

function procesar_interaccion_compra_producto($product_id, $order_id) {
    return;
    //$interaccion_id = "56c1cc0a769ce480473a7ad6";"56cdda5f6fe2b09954110971";
    $interaccion_id = HorusConf::get_var('interaccion_compra_producto_id');

    $product = get_product_data($product_id, $order_id);
    $parametros = default_parametros();
    $parametros = array_merge($parametros, $product);

    $parametros_conexion = configurar_parametros_conexion();

    $response = Requests::post($parametros_conexion["url"] . "/interactions/" . $interaccion_id, $parametros_conexion["headers"], $parametros);
    $json = json_decode($response->body);

    error_log("Horus Response: " . $response->body, 0);

    return $product;
}

function get_product_data($item, $order_id) {
    $product = new WC_Product($item['product_id']);

    $data = [
        'SKU' => $product->get_title(),
        'nombre' => $product->get_title(),
        'Marca' => get_marca($product),
        'BrandProduct_Nber' => get_marca($product),
        'Brand' => get_marca($product),
        'Monto' => $product->price,
        'PurchaseAmount' => $product->price,
        'valor' => $product->price,
        'Total_amount' => $product->price,
        'mililitros' => get_mililitros($product),
        'LocalAttribute_Litres' => get_mililitros($product),
        'medida' => get_mililitros($product),
        'Cantidad' => $item['qty'],
        'QuantityProduct' => $item['qty'],
        'Quantity' => $item['qty'],
        'OrderNumber' => $order_id,
        'ItemNumber' => $item['product_id'],
        'categoria' => get_categoria($product),
        'LocalAttribute_Category' => get_categoria($product),
        'Descuento' => 'Ninguno',
        'LocalAttribute_DiscountAmount' => 'Ninguno',
        'LocalAttribute_Special_Offer' => 'no',
        'PurchaseCode' => 'Ninguno',
        'promocion_codigo' => 'Ninguno',
        'LocalAttribute_Special_Offer_Code' => 'Ninguno',
        'promocion' => 'Ninguna',
        'LocalAttribute_Special_Offer_Name' => 'Ninguna',
        'ciudad' => 'Bogotá',
        'City' => 'Bogotá',
        'distribuidor' => 'teletrade',
        'LocalAttribute_Supplier' => 'teletrade',
        'tipo_envio' => 'Inmediato',
        'LocalAttribute_Order_Type' => 'Inmediato'
    ];
    return $data;
}
