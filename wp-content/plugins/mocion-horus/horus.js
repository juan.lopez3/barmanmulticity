jQuery(document).ready(function ($) {


    console.log();


    $("[data-interaccion-id]").on("click", function (e) {
        console.log("click");

        var interacciondata = {};
        var indice = null;


        jQuery.each($(this).data(), function (index, value) {
            indice = index.replace("interaccion", "");
            if (index.indexOf("interaccion") !== -1 && indice.length > 0) {
                interacciondata[indice] = value;
            }
        });

        notify_interaction(interacciondata);
       
    })


    // We can also pass the url value separately from ajaxurl for front end AJAX implementations

});


function notify_interaction(interacciondata) {
    var params = {
        'action': 'notify_horus_interaction',
        'interacciondata': interacciondata
    };


    jQuery.post(horus.ajax_url, params, function (response) {
        console.log('Got this from the server: ' + response);
    });

}
