<?php

/**
 *  Códigos prueba ACCIONES HORUS 
 *  Token d6d28f0d-2646-063c-5522-93eefee68df0
  Compra producto barman 56cdda5f6fe2b09954110971
  Producto agregado al carrito 57c898c5c1f7c73a51a9cef7
  Ingreso al checkout 57c8993ec1f7c73a51a9cef8
 */
add_action('wp_enqueue_scripts', 'ajax_hours_enqueue_scripts');

function ajax_hours_enqueue_scripts() {
    wp_enqueue_script('horus', plugins_url('/horus.js', __FILE__), array('jquery'), true);
// in JavaScript, object properties are accessed as ajax_object.ajax_url, ajax_object.we_value
    wp_localize_script('horus', 'horus', array('ajax_url' => admin_url('admin-ajax.php')));
}

add_action('wp_ajax_notify_horus_interaction', 'notify_horus_interaction_callback');
add_action('wp_ajax_nopriv_notify_horus_interaction', 'notify_horus_interaction_callback');

/** La interaccion  "Producto agregado al carrito" necesita el parametro agregado_desde, El cual indica desde que parte del portal se agrego el producto
 *  
 * @global type $wpdb
 */
function notify_horus_interaction_callback() {

    return;

    global $wpdb;

    //Estas interacciones tienen sentido solo para usuarios LOGUEADOS
    $token = get_horus_token();
    error_log("token:: " . $token);

    if (!$token) {
        error_log("Can't report to horus user doesn't have token");
        echo "Can't report to horus user doesn't have token";
        wp_die();
    }

    $interacciondata = json_decode(json_encode($_POST['interacciondata']));

    //recibir los parametros enviados por post sobre la interaccion de horus
    if (!isset($interacciondata->Id)) {
        echo "interaccionId not provided";
        error_log("interaccionId not provided");
        wp_die();
    }

    $interaccion_id = $interacciondata->Id;
    unset($interacciondata->Id);

    //Si viene el id de un producto cargamos informacion basica del producto y lo agregamos a la interaccion
    if (isset($interacciondata->Productid)) {
        $interacciondata = add_product_data($interacciondata);
    }

    error_log(json_encode($interacciondata));
    //Agregamos los parametros en mayuscula tambien por un error en HORUS
    foreach ($interacciondata as $key => $value) {
        $llaveMayuscula = ucfirst($key);
        $interacciondata->{$llaveMayuscula} = $value;
    }

    //invocar la interaccion de horus
    $parametros_conexion = configurar_parametros_conexion();

    $parametros = default_parametros();
    $parametros['test'] = json_encode($interacciondata);

    $parametros = array_merge($parametros, (array) $interacciondata);

    $url = $parametros_conexion["url"] . "/interactions/" . $interaccion_id;
    //echo json_encode($parametros_conexion);
    //echo "URL:$url ".
    error_log("conexion" . json_encode($parametros_conexion["headers"]));
    $mensaje = "";
    try {
        $response = Requests::post($url, $parametros_conexion["headers"], $parametros);
        $mensaje = $response->body;
    } catch (Exception $e) {
        $mensaje = "error: " . $e->getMessage();
    }

    error_log("HORUS: " . $mensaje);
    echo $mensaje;
    wp_die();
}

function default_parametros() {
    $parametros = [
        "test" => "test",
        "firstName" => "Consumer 2 name",
        "email" => "c6@email.com",
        "birthday" => "1989-06-20",
        "gender" => "M",
        "A" => "Ingeniero"
    ];

    return $parametros;
}

function configurar_parametros_conexion() {
    return;
    Requests::register_autoloader();
    $parametros_conexion = HorusConf::get_parameters_default();
    $parametros_conexion["headers"]['Cache-Control'] = 'no-cache';

    $token = get_horus_token();
    if ($token) {
        $parametros_conexion["headers"]['X-CUSTOMER-DIGITAL-EXTENSION-TOKEN'] = $token;
    }
    return $parametros_conexion;
}

function add_product_data($interacciondata) {
    $product = new WC_Product($interacciondata->Productid);
    unset($interacciondata->Productid);

    //marca
    $interacciondata->marca = get_marca($product);
    $interacciondata->familia = get_marca($product);
    $interacciondata->BrandProduct_Nber = get_marca($product);

    //nombre-sku
    $interacciondata->producto = $product->get_title();
    $interacciondata->nombre = $product->get_title();
    $interacciondata->SKU = $product->get_title();

    //medida
    $interacciondata->mililitros = get_mililitros($product);
    $interacciondata->medida = get_mililitros($product);
    $interacciondata->LocalAttribute_optin_Litres = get_mililitros($product);

    //precio
    $interacciondata->PurchaseAmount = $product->price;
    $interacciondata->precio = $product->price;
    $interacciondata->ValorProducto = $product->price;
    $interacciondata->valor = $product->price;

    $interacciondata->city = "Bogotá";
    $interacciondata->ciudad = "Bogotá";

    $interacciondata->edicion_especial = "no";
    $interacciondata->LocalAttribute_Special_Offer = "no";


    if (isset($interacciondata->Agregado_desde))
        $interacciondata->LocalAttribute_Added_From = $interacciondata->Agregado_desde;


    return $interacciondata;
}

function get_mililitros($product) {
    return $mililitros = get_post_meta($product->id, 'mililitros', true);
}

function get_categoria($product) {
    //Cargar la categoria relacionada
    $terms = get_the_terms($product->id, 'product_cat');
    $terminos = array();
    foreach ($terms as $term) {
        $terminos[] = $term->name;
    }
    $terminos_string = implode(",", $terminos);
    return $terminos_string;
}

function get_marca($product) {
    $marca = wp_get_post_terms($product->id, 'marca');
    return (is_array($marca) && isset($marca[0])) ? $marca[0] : "Marca";
}
