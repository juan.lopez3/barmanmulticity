��    L      |  e   �      p     q     u     �     �     �     �  	   �  	   �     �  
   �     �     �          %     8     U     a  T   i  <   �  {   �     w     |     �     �     �  
   �     �     �     �     �     �     �     �     	     
	     	     /	     L	     [	     c	     w	     ~	     �	      �	     �	     �	     �	     �	     �	  #   
     '
     5
     F
     X
     `
     d
     p
     |
     �
      �
  
   �
  )   �
     �
     �
     
       &     G   E  @   �     �     �     �  	   �     �       �       �     �     �               $     1     >     E     L     _     x     �     �     �     �     �  6   �  <     O   L     �     �     �     �     �     �     �     �     �          '     7     ;     B     O     e     �     �     �     �     �     �     �     �               $     4     K      [     |     �     �     �  	   �     �     �     �               1  '   D     l       	   �     �  0   �  =   �  3        Q  	   X     b     r     �     �        E   '          =   >   +   L      .                                 /   $   -       B   1              7              A          0   (   "       9       J   :      &           G       ?          )   3       @      	                  H   %   
      I              F         ;   D   K   2   #   !      ,       5          8             <   4   *   6         C              Add Add Global Tab Add New Tab Add Tab Additional Information Click to toggle Close all Configure Content Custom Tab Custom field deleted. Custom field updated. Default Tab Layout Delete Permanently Delete this item permanently Description Dismiss Displays the product attributes and properties configured in the Product Data panel. Displays the product content set in the main content editor. Displays the product review form and any reviews.  Use %d in the Title to substitute the number of reviews for the product. Docs Draft Edit Edit Global Tab Content Edit Tab Expand all Global Heading M j, Y @ G:i Move this item to the Trash Move to Trash N/A Name New Tab No Tabs found No Tabs found in trash Override default tab layout: Parent Product Product Product Description Remove Remove this product tab? Restore Restore this item from the Trash Reviews (%d) Save Changes Save Tab Save/update the tab Search Tabs Search results for &#8220;%s&#8221; Show all Tabs Show global tabs Show product tabs Support Tab Tab Actions Tab Manager Tab Type Tab draft updated. Tab restored to revision from %s Tab saved. Tab scheduled for: <strong>%1$s</strong>. Tab submitted. Tab updated. Tabs Tabs layout %s The tab title, this appears in the tab The title/content for this tab will be provided by a third party plugin This is where you can add new tabs that you can add to products. Title Trash View Tab View Tabs WooCommerce Tab Manager Write a Review Project-Id-Version: 
POT-Creation-Date: 2014-07-25 01:14+0800
PO-Revision-Date: 2014-07-25 02:21+0800
Last-Translator: Vincent <vincent@webmart.tw>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.5
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_x
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: /Users/Mac/Desktop/Woocommerce.Tab.Manager.v1.1.2/woocommerce-tab-manager
 加入 添加全局選項卡 新增選項卡 添加選項卡 其他資訊 點選切換 全部關閉 配置 內容 自定義選項卡 自定義欄位已刪除 自定義欄位已更新 預設選項卡布局 永久刪除 永久刪除此項目 描述 關閉 顯示商品資料面板配置的商品特性和屬性 顯示在主要內容編輯器中設置​​的商品內容 顯示商品留言表格及任何評論。在標題使用 %d 來代替評論數 說明文件 草稿 編輯 編輯全局選項卡內容 編輯選項卡 全部展開 全局 名稱 M j, Y @ G:i 將此項目移至回收桶 移至回收桶 N/A 名稱 新選項卡 沒有找到選項卡 回收桶沒有找到選項卡 覆蓋預設選項卡布局 上層商品 商品 商品描述 移除 移除這個商品選項卡? 還原 將此項目從回收桶還原 評論 (%d) 儲存變更 儲存選項卡 儲存/更新選項卡 搜尋選項卡 &#8220;%s&#8221; 的搜尋結果 顯示所有選項卡 顯示全局選項卡 顯示商品選項卡 支援 選項卡 選項卡動作 選項卡管理器 選項卡類型 選項卡草稿已更新 選項卡還原從 %s 選項卡已儲存 選項卡排程 : <strong>%1$s</strong> 選項卡已提交 選項卡已更新 選項卡 選項卡佈局%s 選項卡的標題，這會顯示在選項卡上 這個選項卡標題/內容將透過第三方外掛來提供 在這裡，您可以為商品添加新的選項卡 標題 回收桶 檢視選項卡 檢視選項卡 WooCommerce 選項卡管理器 發表評論 