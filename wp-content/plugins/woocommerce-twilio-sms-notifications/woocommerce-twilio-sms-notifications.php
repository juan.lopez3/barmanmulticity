<?php
/**
 * Plugin Name: WooCommerce Twilio SMS Notifications
 * Plugin URI: http://www.woothemes.com/products/twilio-sms-notifications/
 * Description: Send SMS order notifications to admins and customers for your WooCommerce store. Powered by Twilio :)
 * Author: SkyVerge
 * Author URI: http://www.skyverge.com
 * Version: 1.2
 * Text Domain: woocommerce-twilio-sms-notifications
 * Domain Path: /i18n/languages/
 *
 * Copyright: (c) 2013-2014 SkyVerge, Inc. (info@skyverge.com)
 *
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 *
 * @package   WC-Twilio-SMS-Notifications
 * @author    SkyVerge
 * @category  Integration
 * @copyright Copyright (c) 2013-2014, SkyVerge, Inc.
 * @license   http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Required functions
if ( ! function_exists( 'woothemes_queue_update' ) ) {
	require_once( 'woo-includes/woo-functions.php' );
}

// Plugin updates
woothemes_queue_update( plugin_basename( __FILE__ ), '2b17098ebabfc218a552515202cf973a', '132190' );

// WC active check
if ( ! is_woocommerce_active() ) {
	return;
}

// Required library class
if ( ! class_exists( 'SV_WC_Framework_Bootstrap' ) ) {
	require_once( 'lib/skyverge/woocommerce/class-sv-wc-framework-bootstrap.php' );
}

SV_WC_Framework_Bootstrap::instance()->register_plugin( '2.0', __( 'WooCommerce Twilio SMS Notifications', 'woocommerce-twilio-sms-notifications' ), __FILE__, 'init_woocommerce_twilio_sms_notifications' );

function init_woocommerce_twilio_sms_notifications() {

/**
 * # WooCommerce Twilio SMS Notifications Plugin
 *
 * ## Plugin Overview
 *
 * This plugin sends SMS order notifications to admins and customers by hooking into WooCommerce order status changes.
 * Admins can customize the message templates, as well as what order status changes should trigger SMS sends.
 *
 * ## Admin Considerations
 *
 * + 'SMS' tab added to WooCommerce > Settings
 *
 * ## Frontend Considerations
 *
 * + Opt-in checkbox added to checkout page which determines whether SMS order updates will be sent to the customer. The
 * admin can override this on a global basis.
 *
 * ## Database
 *
 * ### Global Settings
 *
 * + `wc_twilio_sms_checkout_optin_checkbox_label` - the label for the optin checkbox on the checkout page
 *
 * + `wc_twilio_sms_checkout_optin_checkbox_default` - the default status for the optin checkbox, either checked or unchecked
 *
 * + `wc_twilio_sms_shorten_urls` - true to shorten URLs inside SMS messages with goo.gl, false otherwise
 *
 * + `wc_twilio_sms_enable_admin_sms` - true to send SMS admin new order notifications, false otherwise
 *
 * + `wc_twilio_sms_admin_sms_recipients` - mobile phone numbers to send SMS admin new order notifications to
 *
 * + `wc_twilio_sms_admin_sms_template` the message template to use for SMS admin new order notifications
 *
 * + `wc_twilio_sms_send_sms_<order status>` - true to send an SMS update for each order status, false otherwise
 *
 * + `wc_twilio_sms_default_sms_template` - the message template to use for SMS order notifications if an order status-specific template does not exist
 *
 * + `wc_twilio_sms_<order status>_sms_template - the message template to use for the particular order status
 *
 * + `wc_twilio_sms_account_sid` - the account SID for the Twilio API
 *
 * + `wc_twilio_sms_auth_token` - the auth token for the Twilio API
 *
 * + `wc_twilio_sms_from_number` - the number that SMS will be sent from, must exist in the Twilio account used
 *
 * + `wc_twilio_sms_log_errors` - true to log errors to the WC error log, false otherwise
 *
 * ### Options table
 *
 * + `wc_twilio_sms_version` - the current plugin version, set on install/upgrade
 *
 * ### Order meta
 *
 * + `_wc_twilio_sms_optin` - set to true if the customer has opted-in to SMS order updates for this given order, false otherwise
 *
 */
class WC_Twilio_SMS extends SV_WC_Plugin {


	/** version number */
	const VERSION = '1.2';

	/** plugin id */
	const PLUGIN_ID = 'twilio_sms';

	/** plugin text domain */
	const TEXT_DOMAIN = 'woocommerce-twilio-sms-notifications';

	/** @var \WC_Twilio_SMS_Admin instance */
	public $admin;

	/** @var \WC_Twilio_SMS_AJAX instance */
	public $ajax;

	/** @var \WC_Twilio_SMS_API instance */
	private $api;


	/**
	 * Setup main plugin class
	 *
	 * @since 1.0
	 * @return \WC_Twilio_SMS
	 */
	public function __construct() {

		parent::__construct(
		  self::PLUGIN_ID,
		  self::VERSION,
		  self::TEXT_DOMAIN
		);

		// Load classes
		$this->includes();

		// Add opt-in checkbox to checkout
		add_action( 'woocommerce_after_checkout_billing_form', array( $this, 'add_opt_in_checkbox' ) );

		// Process opt-in checkbox after order is processed
		add_action( 'woocommerce_checkout_update_order_meta', array( $this, 'process_opt_in_checkbox' ) );

		// Add order status hooks
		$this->add_order_status_hooks();
	}


	/**
	 * Loads required classes
	 *
	 * @since 1.0
	 */
	private function includes() {

		// notification class manages sending the SMS notifications
		require_once( 'includes/class-wc-twilio-sms-notification.php' );

		// load admin classes
		if ( is_admin() ) {
			$this->admin_includes();
		}
	}


	/**
	 * Loads admin classes
	 *
	 * @since 1.0
	 */
	private function admin_includes() {

		// admin
		require_once( 'includes/admin/class-wc-twilio-sms-admin.php' );
		$this->admin = new WC_Twilio_SMS_Admin();

		// AJAX
		require_once( 'includes/class-wc-twilio-sms-ajax.php' );
		$this->ajax = new WC_Twilio_SMS_AJAX();
	}

	/**
	 * Add hooks for the opt-in checkbox and customer / admin order status changes
	 *
	 * @since 1.1
	 */
	private function add_order_status_hooks() {

		// Customer order status change hooks
		foreach( array( 'pending', 'failed', 'on-hold', 'processing', 'completed', 'refunded', 'cancelled' ) as $status ) {

			add_action( 'woocommerce_order_status_' . $status, array( $this, 'send_customer_notification' ) );
		}

		// Admin new order hooks
		foreach( array( 'pending_to_on-hold', 'pending_to_processing', 'pending_to_completed', 'failed_to_on-hold', 'failed_to_processing', 'failed_to_completed' ) as $status ) {

			add_action( 'woocommerce_order_status_' . $status, array( $this, 'send_admin_new_order_notification' ) );
		}
	}


	/**
	 * Send customer an SMS when their order status changes
	 *
	 * @since 1.1
	 */
	public function send_customer_notification( $order_id ) {

		$notification = new WC_Twilio_SMS_Notification( $order_id );

		$notification->send_automated_customer_notification();
	}


	/**
	 * Send admins an SMS when a new order is received
	 *
	 * @since 1.1
	 */
	public function send_admin_new_order_notification( $order_id ) {

		$notification = new WC_Twilio_SMS_Notification( $order_id );

		$notification->send_admin_notification();
	}


	/**
	 * Returns the Twilio SMS API object
	 *
	 * @since  1.1
	 * @return \WC_Twilio_SMS_API the API object
	 */
	public function get_api() {

		if ( is_object( $this->api ) ) {
			return $this->api;
		}

		// Load API
		require_once( 'includes/class-wc-twilio-sms-api.php' );

		$account_sid = get_option( 'wc_twilio_sms_account_sid', '' );
		$auth_token  = get_option( 'wc_twilio_sms_auth_token', '' );
		$from_number = get_option( 'wc_twilio_sms_from_number', '' );

		return $this->api = new WC_Twilio_SMS_API( $account_sid, $auth_token, $from_number );
	}


	/**
	 * Adds checkbox to checkout page for customer to opt-in to SMS notifications
	 *
	 * @since 1.0
	 */
	public function add_opt_in_checkbox() {

		// use previous value or default value when loading checkout page
		if( ! empty( $_POST[ 'wc_twilio_sms_optin' ] ) ) {
			$value = woocommerce_clean( $_POST[ 'wc_twilio_sms_optin' ] );
		} else {
			$value = ( 'checked' == get_option( 'wc_twilio_sms_checkout_optin_checkbox_default', 'unchecked' ) ) ? 1 : 0;
		}

		// output checkbox
		woocommerce_form_field( 'wc_twilio_sms_optin', array(
			'type'  => 'checkbox',
			'class' => array( 'form-row-wide' ),
			'label' => get_option( 'wc_twilio_sms_checkout_optin_checkbox_label' )
		), $value );
	}


	/**
	 * Save opt-in as order meta
	 *
	 * @since 1.0
	 * @param int $order_id order ID for order being processed
	 */
	public function process_opt_in_checkbox( $order_id ) {

		if ( ! empty( $_POST[ 'wc_twilio_sms_optin' ] ) ) {
			update_post_meta( $order_id, '_wc_twilio_sms_optin', 1 );
		}
	}


	/**
	 * Load plugin text domain.
	 *
	 * @since 1.0
	 * @see SV_WC_Plugin::load_translation()
	 */
	public function load_translation() {

		load_plugin_textdomain( 'woocommerce-twilio-sms-notifications', false, dirname( plugin_basename( $this->get_file() ) ) . '/i18n/languages' );
	}


	/** Helper methods ******************************************************/

	/**
	 * Returns the plugin name, localized
	 *
	 * @since 1.2
	 * @see SV_WC_Plugin::get_plugin_name()
	 * @return string the plugin name
	 */
	public function get_plugin_name() {

		return __( 'WooCommerce Twilio SMS Notifications', self::TEXT_DOMAIN );
	}


	/**
	 * Returns __FILE__
	 *
	 * @since 1.2
	 * @see SV_WC_Plugin::get_file()
	 * @return string the full path and filename of the plugin file
	 */
	protected function get_file() {

		return __FILE__;
	}


	/**
	 * Gets the URL to the settings page
	 *
	 * @since 1.2
	 * @see SV_WC_Plugin::is_plugin_settings()
	 * @param string $_ unused
	 * @return string URL to the settings page
	 */
	public function get_settings_url( $_ = '' ) {

		if ( SV_WC_Plugin_Compatibility::is_wc_version_gte_2_1() ) {

			return admin_url( 'admin.php?page=wc-settings&tab=twilio_sms' );

		} else {

			return admin_url( 'admin.php?page=woocommerce_settings&tab=twilio_sms' );
		}
	}

	/**
	 * Returns true if on the plugin settings page
	 *
	 * @since 1.1-1
	 * @see SV_WC_Plugin::is_plugin_settings()
	 * @return boolean true if on the settings page
	 */
	public function is_plugin_settings() {

		if ( SV_WC_Plugin_Compatibility::is_wc_version_gte_2_1() ) {

			return isset( $_GET['page'] ) && 'wc-settings' == $_GET['page'] && isset( $_GET['tab'] ) && 'twilio_sms' == $_GET['tab'];

		} else {

			return isset( $_GET['page'] ) && 'woocommerce_settings' == $_GET['page'] && isset( $_GET['tab'] ) && 'twilio_sms' == $_GET['tab'];
		}
	}

	/**
	 * Log messages to WooCommerce error log if logging is enabled
	 *
	 * /wp-content/woocommerce/logs/twilio-sms.txt
	 *
	 * @since 1.1
	 * @param string $content message to log
	 * @param string $_ unused
	 */
	public function log( $content, $_ = null ) {

		if ( 'yes' == get_option( 'wc_twilio_sms_log_errors ') ) {

			parent::log( $content );
		}
	}


	/** Lifecycle methods ******************************************************/


	/**
	 * Run every time.  Used since the activation hook is not executed when updating a plugin
	 *
	 * @since 1.0
	 */
	protected function install() {

		require_once( 'includes/admin/class-wc-twilio-sms-admin.php' );

		// install default settings
		foreach ( WC_Twilio_SMS_Admin::get_settings() as $setting ) {

			if ( isset( $setting['default'] ) ) {
				add_option( $setting['id'], $setting['default'] );
			}
		}
	}


	/**
	 * Perform any version-related changes.
	 *
	 * @since 1.0
	 * @param int $installed_version the currently installed version of the plugin
	 */
	protected function upgrade( $installed_version ) {

		// upgrade to 1.1.2 version
		if ( version_compare( $installed_version, '1.1.2' ) < 0 ) {

			delete_option( 'wc_twilio_sms_is_installed' );
		}
	}


} // end \WC_Twilio_SMS class


/**
 * The WC_Twilio_SMS global object
 * @name $wc_twilio_sms
 * @global WC_Twilio_SMS $GLOBALS['wc_twilio_sms']
 */
$GLOBALS['wc_twilio_sms'] = new WC_Twilio_SMS();

} // init_woocommerce_twilio_sms_notifications()
