=== WooCommerce Twilio SMS Notifications ===
Author: skyverge
Tags: woocommerce
Requires at least: 3.5
Tested up to: 3.8
Requires WooCommerce at least: 2.0
Tested WooCommerce up to: 2.1

Send SMS order notifications to admins and customers for your WooCommerce store. Powered by Twilio :)

See http://docs.woothemes.com/document/twilio-sms-notifications/ for full documentation.

== Installation ==

1. Upload the entire 'woocommerce-twilio-sms-notifications' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
