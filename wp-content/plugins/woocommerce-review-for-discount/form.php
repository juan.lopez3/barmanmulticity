<div class="wrap woocommerce">
    <div id="icon-edit-comments" class="icon32"><br></div>
    <h2 class="nav-tab-wrapper woo-nav-tab-wrapper">
        <a href="admin.php?page=wc-review-discount&amp;tab=discounts" class="nav-tab <?php echo ($tab == 'discounts') ? 'nav-tab-active' : ''; ?>"><?php _e('Discounts', 'wc_review_discount'); ?></a>
        <a href="admin.php?page=wc-review-discount&amp;tab=new" class="nav-tab <?php echo ($tab == 'new') ? 'nav-tab-active' : ''; ?>"><?php _e('New Discount', 'wc_review_discount'); ?></a>
        <a href="admin.php?page=wc-review-discount&amp;tab=email" class="nav-tab <?php echo ($tab == 'email') ? 'nav-tab-active' : ''; ?>"><?php _e('Email Settings', 'wc_review_discount'); ?></a>
    </h2>

    <style type="text/css">
    .chosen-container-multi .chosen-choices li.search-field input[type=text] {height: auto;}
    </style>
    <form action="admin-post.php" method="post">

        <?php if ( $discount['id'] == 0 ): ?>
        <h3><?php _e('Create a New Discount', 'wc_review_discount'); ?></h3>
        <p><?php _e("Create a new discount for a product review. The settings for a discount are similar to a standard <a href=\"edit.php?post_type=shop_coupon\">coupon</a>, but are limited to a single use and are only enabled for successful reviews of your products.", 'wc_review_discount'); ?></p>
        <?php else: ?>
        <h3><?php _e('Edit Discount', 'wc_review_discount'); ?></h3>
        <p><?php _e('The settings for a discount are similar to a standard <a href="edit.php?post_type=shop_coupon">coupon</a>, but are limited to a single use and are only enabled for successful reviews of your products. You can edit the discount below.', 'wc_review_discount'); ?></p>
        <?php endif; ?>

        <table class="form-table">
            <tr>
                <th><label for="type"><?php _e('Discount type', 'wc_review_discount'); ?></label></th>
                <td>
                    <select id="type" name="type">
                    <?php

                    $types = (function_exists('wc_get_coupon_types')) ? wc_get_coupon_types() : $woocommerce->get_coupon_discount_types();

                    foreach ($types as $key => $type) {
                        $selected = ($discount['type'] == $key) ? 'selected' : '';
                        echo '<option value="'. $key .'" '. $selected .'>'. $type .'</option>';
                    }
                    ?>
                    </select>
                </td>
            </tr>
            <tr>
                <th><label for="amount"><?php _e('Amount / Percentage', 'wc_review_discount'); ?></label></th>
                <td>
                    <input type="text" name="amount" id="amount" class="short" value="<?php echo esc_attr($discount['amount']); ?>" placeholder="0.0" />
                    <span class="description"><?php _e('e.g. 5.99 (do not include the percent symbol)', 'wc_review_discount'); ?></span>
                </td>
            </tr>
            <tr>
                <th><label for="individual"><?php _e('Individual use', 'wc_review_discount'); ?></label></th>
                <td>
                    <input type="checkbox" class="checkbox" name="individual_use" id="individual" value="yes" <?php if ($discount['individual'] != 0) echo 'checked'; ?> />
                    <span class="description"><?php _e('Check this box if the coupon cannot be used in conjunction with other coupons', 'wc_review_discount'); ?></span>
                </td>
            </tr>
            <tr>
                <th><label for="before_tax"><?php _e('Apply before tax', 'wc_review_discount'); ?></label></th>
                <td>
                    <input type="checkbox" class="checkbox" name="before_tax" id="before_tax" value="yes" <?php if ($discount['before_tax'] != 0) echo 'checked'; ?> />
                    <span class="description"><?php _e('Check this box if the coupon should be applied before calculating cart tax', 'wc_review_discount'); ?></span>
                </td>
            </tr>
            <tr>
                <th><label for="free_shipping"><?php _e('Enable free shipping', 'wc_review_discount'); ?></label></th>
                <td>
                    <input type="checkbox" class="checkbox" name="free_shipping" id="free_shipping" value="yes" <?php if ($discount['free_shipping'] != 0) echo 'checked'; ?> />
                    <span class="description"><?php _e('Check this box if the coupon enables free shipping (see <a href="admin.php?page=woocommerce&tab=shipping_methods&subtab=shipping-free_shipping">Free Shipping</a>)', 'wc_review_discount'); ?></span>
                </td>
            </tr>
            <tr>
                <th><label for="sending_mode"><?php _e('When do we send the discount code', 'wc_review_discount'); ?></label></th>
                <td>
                    <select name="sending_mode" id="sending_mode">
                        <option value="immediately" <?php if ($discount['send_mode'] == 'immediately') echo 'selected'; ?>><?php _e('Immediately after posting the review', 'wc_review_discount'); ?></option>
                        <option value="approved" <?php if ($discount['send_mode'] == 'approved') echo 'selected'; ?>><?php _e('Only after review has been approved', 'wc_review_discount'); ?></option>
                    </select>
                </td>
            </tr>

            <?php if (function_exists('woocommerce_customer_bought_product')): ?>
            <tr>
                <th><label for="verified"><?php _e('Only send to verified owners', 'wc_review_discount'); ?></label></th>
                <td>
                    <select name="send_to_verified" id="verified">
                        <option value="0" <?php if ($discount['verified'] == 0) echo 'selected'; ?>><?php _e('No', 'wc_review_discount'); ?></option>
                        <option value="1" <?php if ($discount['verified'] == 1) echo 'selected'; ?>><?php _e('Yes', 'wc_review_discount'); ?></option>
                    </select>
                </td>
            </tr>
            <?php endif; ?>

            <tr>
                <th><label for="expiry"><?php _e('Expiry', 'wc_review_discount'); ?></label></th>
                <td>
                    <select name="expiry_value">
                        <option value="" <?php if ($discount['expiry_value'] == 0) echo 'selected'; ?>><?php _e('Does not expire', 'wc_review_discount'); ?></option>
                        <?php for ($x = 1; $x <= 30; $x++): ?>
                        <option value="<?php echo $x; ?>" <?php if ($discount['expiry_value'] == $x) echo 'selected'; ?>><?php echo $x; ?></option>
                        <?php endfor; ?>
                    </select>
                    <select name="expiry_type">
                        <option value="" <?php if ($discount['expiry_type'] == '') echo 'selected'; ?>>-</option>
                        <option value="days" <?php if ($discount['expiry_type'] == 'days') echo 'selected'; ?>><?php _e('days', 'wc_review_discount'); ?></option>
                        <option value="weeks" <?php if ($discount['expiry_type'] == 'weeks') echo 'selected'; ?>><?php _e('weeks', 'wc_review_discount'); ?></option>
                        <option value="months" <?php if ($discount['expiry_type'] == 'months') echo 'selected'; ?>><?php _e('months', 'wc_review_discount'); ?></option>
                    </select>
                    <span class="description"><?php _e('after the discount has been sent to the user', 'wc_review_discount'); ?></span>
                </td>
            </tr>
            <tr>
                <th><label for="unique_email"><?php _e('One coupon per email', 'wc_review_discount'); ?></label></th>
                <td>
                    <input type="checkbox" class="checkbox" value="1" name="unique_email" id="unique_email" <?php if ($discount['unique_email'] != 0) echo 'checked'; ?> />
                    <span class="description"><?php _e('Limit the sending of coupons to one per email address.', 'wc_review_discount'); ?></span>
                </td>
            </tr>
            <tr>
                <th><label for="all_products"><?php _e('Apply to all products', 'wc_review_discount'); ?></label></th>
                <td><input type="checkbox" class="checkbox" name="all_products" id="all_products" value="yes" <?php if ($discount['all_products'] == 'yes') echo 'checked'; ?> /></td>
            </tr>
        </table>

        <div class="hide-if-all-products">
            <strong><?php _e('Select the products/categories that a user can submit a review for, and be rewarded with a discount code. This allows you to limit the reward of discounts to drive reviews of specific products/categories, or to have different discounts for reviews of different products/categories.', 'wc_review_discount'); ?></strong>

            <table class="form-table">
                <tr>
                    <th><label for="product_ids"><?php _e('Products', 'wc_review_discount'); ?></label></th>
                    <td>
                        <select id="product_ids" name="product_ids[]" class="ajax_chosen_select_products_and_variations" multiple="multiple" data-placeholder="<?php _e('Search for a product...', 'wc_review_discount'); ?>" style="width: 500px;">
                            <?php
                            if ( is_array($discount['products']) ) {

                                foreach ( $discount['products'] as $product_id ) {
                                    echo '<option value="'. $product_id .'" selected>'. get_the_title($product_id) .' #'. $product_id .'</option>';
                                }

                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th><label for="product_cats"><?php _e('Product categories', 'wc_review_discount'); ?></label></th>
                    <td>
                        <select id="product_cats" name="product_cats[]" class="chzn-select" multiple="multiple" data-placeholder="<?php _e('Search for a category...', 'wc_review_discount'); ?>" style="width:500px;">
                            <?php
                            foreach ($cats as $cat):
                                $selected = (is_array($discount['categories']) && in_array($cat->term_id, $discount['categories'])) ? 'selected' : '';
                            ?>
                            <option value="<?php echo $cat->term_id; ?>"><?php echo $cat->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th><label for="limit"><?php _e('Limit coupon validity', 'wc_review_discount'); ?></label></th>
                    <td>
                        <input type="checkbox" class="checkbox" value="1" name="limit" id="limit" <?php if ($discount['limit'] != 0) echo 'checked'; ?> />
                        <span class="description"><?php _e('Checking this box will also limit the usage of the coupon to the products/categories defined above.', 'wc_review_discount'); ?></span>
                    </td>
                </tr>
            </table>
        </div>
        <p class="submit">
            <?php if ($discount['id'] == 0): ?>
            <input type="hidden" name="action" value="sfn_rd_new" />
            <input type="submit" name="save" value="<?php _e('Create Discount', 'wc_review_discount'); ?>" class="button-primary" />
            <?php else: ?>
            <input type="hidden" name="id" value="<?php echo $discount['id']; ?>" />
            <input type="hidden" name="action" value="sfn_rd_edit" />
            <input type="submit" name="save" value="<?php _e('Update Discount', 'wc_review_discount'); ?>" class="button-primary" />
            <?php endif; ?>
        </p>
    </form>
    <script type="text/javascript">
    jQuery(".chzn-select").chosen();

    jQuery("select.ajax_chosen_select_products_and_variations").ajaxChosen({
        method:     'GET',
        url:        ajaxurl,
        dataType:   'json',
        afterTypeDelay: 100,
        data:       {
            action:         'woocommerce_json_search_products_and_variations',
            security:       '<?php echo wp_create_nonce("search-products"); ?>'
        }
    }, function (data) {
        var terms = {};

        jQuery.each(data, function (i, val) {
            terms[i] = val;
        });

        return terms;
    });

    jQuery("#all_products").change(function() {
        if (jQuery(this).is(":checked")) {
            jQuery(".hide-if-all-products").hide();
        } else {
            jQuery(".hide-if-all-products").show();
        }
    }).change();
    </script>
</div>