<?php

/**
 * retorna reporte con las ordenes en las que se usaron cupones
 * 
 * @global type $wpdb
 */
function export_orders_with_coupons() {

    global $wpdb;
    ?>

    <br>
    <h2 class="nav-tab-wrapper woo-nav-tab-wrapper">
        <a href="admin.php?page=export_orders_with_coupons_page" class="nav-tab nav-tab-active"> <?php _e('Pedidos con cupones', 'woo-export-order'); ?> </a>
        <a href="admin.php?page=export_coupons_page"             class="nav-tab">                <?php _e('Cupones', 'woo-export-order'); ?> </a>
        <a href="admin.php?page=export_orders_page"              class="nav-tab">                <?php _e('Ventas', 'woo-export-order'); ?> </a>
    </h2>

    <form id="posts-filter" action="?page=export_orders_with_coupons" method="get">

        <input type="hidden" name="page" class="post_page" value="<?php echo!empty($_GET['page']) ? esc_attr($_GET['page']) : 'all'; ?>" />
        <?php if (!empty($_REQUEST['show_sticky'])) { ?>
            <input type="hidden" name="show_sticky" value="1" />
            <?php
        }

        $select = new WP_List_Table;
        $select->months_dropdown("shop_order");

        submit_button(__('Filter'), 'button', 'filter_action', false, array('id' => 'post-query-submit'));

        $args = array(
            'post_type' => 'shop_order',
            'post_status' => 'publish',
            'posts_per_page' => '-1',
        );

        if (!empty($_GET["m"])) {
            $args['m'] = $_GET["m"];
        }

        $loop = new WP_Query($args);

        $var = $today_checkin_var = $today_checkout_var = $booking_time = "";

        // recorremos el array de post que trae la consulta 
        while ($loop->have_posts()) : $loop->the_post();
            ?>

            <?php
            $order_id = $loop->post->ID;

            $order = new WC_Order($order_id);

            $coupons = $order->get_used_coupons();

            //if ($coupons && $order->post_status != 'trash') {

            $total_descuento = $order->get_total_discount();
            $total = $order->get_total();

            $order_items = $order->get_items();

            $my_order_meta = get_post_custom($order_id);


            $cupones = array();
            foreach ($coupons as $key => $item) {
                // $var .= "<td><a href='" . $url_editar . "'>" . $nombrecupon . "</a></td>";
                $cupones[] = "<p>" . $item . "</p> ";
            }
            $separado_cupones = implode("", $cupones);

            $productos = array();
            foreach ($order_items as $key => $item) {
                $productos[] = "<p>" . $item["name"] . "</p> ";
            }
            $separado_productos = implode("", $productos);

            $url_editar_orden = "<a href='" . get_edit_post_link($order_id) . "'># ".$order_id."</a>";

            $var .= "<tr>";
            $var .= "<td>" . $order->completed_date . "</td>";
            $var .= "<td>" . $separado_cupones . "</td>";
            $var .= "<td>" . $url_editar_orden . "</td>";
            $var .= "<td>$ " . number_format($total, 0, null, '.') . "</td>";
            $var .= "<td>" . $separado_productos . "</td>";
            $var .= "<td>$ " . number_format($total_descuento, 0, null, '.') . "</td>";
            $var .= "<td>" . $my_order_meta["_billing_first_name"][0] . " " . $my_order_meta["_billing_last_name"][0] . "</td>";
            $var .= "</tr>";
        //}
        endwhile;

        $swf_path = plugins_url() . "/mocion-wc-export-orders/TableTools/media/swf/copy_csv_xls.swf";
        ?>

        <script>

            jQuery(document).ready(function() {
                var oTable = jQuery('.datatable').dataTable({
                    "bJQueryUI": true,
                    "sScrollX": "",
                    "bSortClasses": false,
                    "aaSorting": [[0, 'desc']],
                    "bAutoWidth": true,
                    "bInfo": true,
                    "sScrollY": "100%",
                    "sScrollX": "100%",
                            "bScrollCollapse": true,
                    "sPaginationType": "full_numbers",
                    "bRetrieve": true,
                    "oLanguage": {
                        "sSearch": "Buscar:",
                        "sInfo": "Ver _START_ a _END_ de _TOTAL_ Ordenes",
                        "sInfoEmpty": "Ver 0 a 0 de 0 entries",
                        "sZeroRecords": "No Ordenes Completed yet",
                        "sInfoFiltered": "(filtered from _MAX_total entries)",
                        "sEmptyTable": "No Ordenes available in table",
                        "sLengthMenu": "Numéro de ordenes para ver: _MENU_",
                        "oPaginate": {
                            "sFirst": "Primera",
                            "sPrevious": "Anterior",
                            "sNext": "Siguiente",
                            "sLast": "Ultima"
                        }
                    },
                    "sDom": 'T<"clear"><"H"lfr>t<"F"ip>',
                    "oTableTools": {
                        "sSwfPath": "<?php echo plugins_url(); ?>/mocion-wc-export-orders/TableTools/media/swf/copy_csv_xls_pdf.swf"
                    }

                });
            });


        </script>

        <div>
            <table id="order_history" class="display datatable">
                <thead>
                    <tr>
                        <th><?php _e('Fecha', 'woo-export-order'); ?></th>
                        <th><?php _e('Cupon', 'woo-export-order'); ?></th>
                        <th><?php _e('Pedido', 'woo-export-order'); ?></th>
                        <th><?php _e('Precio de Venta', 'woo-export-order'); ?></th>
                        <th><?php _e('Productos', 'woo-export-order'); ?></th>
                        <th><?php _e('Descuento del cupón', 'woo-export-order'); ?></th>
                        <th><?php _e('Usuario', 'woo-export-order'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php echo $var; ?>
                </tbody>
            </table>
        </div>
    </form>

    <?php
}
?>
