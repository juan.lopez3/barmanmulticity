<?php

/**
 * retorna reporte de los cupones que se ha creado con el numero de veces que se 
 * a aplicado en pedidos
 * @global type $wpdb
 */
function export_coupons() {
    global $wpdb;
    ?>

    <br>
    <h2 class="nav-tab-wrapper woo-nav-tab-wrapper">
        <a href="admin.php?page=export_orders_with_coupons_page" class="nav-tab">                <?php _e('Pedidos con cupones', 'woo-export-order'); ?> </a>
        <a href="admin.php?page=export_coupons_page"        class="nav-tab nav-tab-active"> <?php _e('Cupones', 'woo-export-order'); ?> </a>
        <a href="admin.php?page=export_orders_page"         class="nav-tab">                <?php _e('Ventas', 'woo-export-order'); ?> </a>
    </h2>


    <form id="posts-filter" action="?page=custom_report_coupon/admin-page.php" method="get">

        <input type="hidden" name="page" class="post_page" value="<?php echo!empty($_GET['page']) ? esc_attr($_GET['page']) : 'all'; ?>" />

        <?php if (!empty($_REQUEST['show_sticky'])) { ?>
            <input type="hidden" name="show_sticky" value="1" />
            <?php
        }

        $select = new WP_List_Table;
        $select->months_dropdown("shop_coupon");

        submit_button(__('Filter'), 'button', 'filter_action', false, array('id' => 'post-query-submit'));
        ?>

        <?php
        $args = array(
            'post_type' => 'shop_coupon',
            'post_status' => 'publish',
        );

        if (!empty($_GET["m"])) {
            $args['m'] = $_GET["m"];
        }


        $coupons = get_posts($args);

        $var = $today_checkin_var = $today_checkout_var = $booking_time = "";

        foreach ($coupons as $id_key => $coupon) {
            $promocion_id = get_post_meta($coupon->ID, 'promocion_cupon_field', true);
            $promocion = get_post($promocion_id);

            $tipo = get_post_meta($coupon->ID, 'discount_type', true);

            if ($tipo == "percent")
                $tipo = "Descuento % Carrito";
            else if ($tipo == "fixed_cart")
                $tipo = "Descuento en el carrito";
            else if ($tipo == "fixed_product")
                $tipo = "Descuento de Producto";
            else if ($tipo == "percent_product")
                $tipo = "% de descuento en el producto";

            $importe = get_post_meta($coupon->ID, 'coupon_amount', true);

            $product_ids = get_post_meta($coupon->ID, 'product_ids', true);

            $product_ids = explode(",", $product_ids);

            $productos = array();
            foreach ($product_ids as $key => $item) {
                $productos[] = "<p>" . get_the_title($item) . "</p> ";
            }
            $separado = implode("", $productos);

            $nombrepromocion = isset($promocion->post_title) ? $promocion->post_title : null;
            $nombrecupon = isset($coupon->post_title) ? $coupon->post_title : null;

            $url_editar = get_edit_post_link($coupon->ID);

            $var .= "<tr>";
            $var .= "<td>" . $nombrepromocion . "</td>";
            $var .= "<td><a href='" . $url_editar . "'>" . $nombrecupon . "</a></td>";
            $var .= "<td>" . $tipo . "</td>";
            $var .= "<td>" . $importe . "</td>";
            $var .= "<td>" . $separado . "</td>";
            $var .= "</tr>";
        }

        $swf_path = plugins_url() . "/mocion-wc-export-orders/TableTools/media/swf/copy_csv_xls.swf";
        ?>

        <script>

            jQuery(document).ready(function() {
                var oTable = jQuery('.datatable').dataTable({
                    "bJQueryUI": true,
                    "sScrollX": "",
                    "bSortClasses": false,
                    "aaSorting": [[0, 'desc']],
                    "bAutoWidth": true,
                    "bInfo": true,
                    "sScrollY": "100%",
                    "sScrollX": "100%",
                            "bScrollCollapse": true,
                    "sPaginationType": "full_numbers",
                    "bRetrieve": true,
                    "oLanguage": {
                        "sSearch": "Buscar:",
                        "sInfo": "Ver _START_ a _END_ de _TOTAL_ Cupones",
                        "sInfoEmpty": "Ver 0 a 0 of 0 entries",
                        "sZeroRecords": "No Cupones Completed yet",
                        "sInfoFiltered": "(filtered from _MAX_total entries)",
                        "sEmptyTable": "No Cupones available in table",
                        "sLengthMenu": "Numéro de cupones para ver: _MENU_",
                        "oPaginate": {
                            "sFirst": "Primera",
                            "sPrevious": "Anterior",
                            "sNext": "Siguiente",
                            "sLast": "Ultima"
                        }
                    },
                    "sDom": 'T<"clear"><"H"lfr>t<"F"ip>',
                    "oTableTools": {
                        "sSwfPath": "<?php echo plugins_url(); ?>/mocion-wc-export-orders/TableTools/media/swf/copy_csv_xls_pdf.swf"
                    }

                });
            });

        </script>

        <div>
            <table id="order_history" class="display datatable">
                <thead>
                    <tr>
                        <th><?php _e('Promoción', 'woo-export-order'); ?></th>
                        <th><?php _e('Cupon', 'woo-export-order'); ?></th>
                        <th><?php _e('Tipo de cupón', 'woo-export-order'); ?></th>
                        <th><?php _e('Importe del cupón', 'woo-export-order'); ?></th>
                        <th><?php _e('IDs de producto', 'woo-export-order'); ?></th>
                        <!--<th><?php _e('Uso / Limite', 'woo-export-order'); ?></th>-->

                    </tr>
                </thead>
                <tbody>
                    <?php echo $var; ?>
                </tbody>
            </table>
        </div>
    </form>

    <?php
}
?>