<?php

$plugin_dir = ABSPATH . 'wp-content/plugins/';


require_once $plugin_dir . 'magic-fields-2/mf_front_end.php';

/**
 * retorna reporte con las ordenes en las que se usaron cupones
 *
 * @global type $wpdb
 */
function export_orders() {

	global $wpdb;
	?>

    <br>
    <h2 class="nav-tab-wrapper woo-nav-tab-wrapper">
        <a href="admin.php?page=export_orders_with_coupons_page"
           class="nav-tab">                <?php _e('Pedidos con cupones', 'woo-export-order'); ?> </a>
        <a href="admin.php?page=export_coupons_page"
           class="nav-tab">                <?php _e('Cupones', 'woo-export-order'); ?> </a>
        <a href="admin.php?page=export_orders_page"
           class="nav-tab nav-tab-active"> <?php _e('Ventas', 'woo-export-order'); ?> </a>
    </h2>

    <form id="posts-filter" action="?page=export_orders" method="get">

        <input type="hidden" name="page" class="post_page"
               value="<?php echo !empty($_GET['page']) ? esc_attr($_GET['page']) : 'all'; ?>"/>
		<?php if (!empty($_REQUEST['show_sticky'])) { ?>
            <input type="hidden" name="show_sticky" value="1"/>
			<?php
		}

		$select = new WP_List_Table;
		$select->months_dropdown("shop_order");

		submit_button(__('Filter'), 'button', 'filter_action', false, array('id' => 'post-query-submit'));

		$args = array(
			'post_type' => 'shop_order',
			'post_status' => 'publish',
			'posts_per_page' => '-1',
			'orderby' => 'completed_date',
			'order' => 'DESC',
		);

		if (!empty($_GET["m"])) {
			$args['m'] = $_GET["m"];
		}

		$loop = new WP_Query($args);

		$var = $today_checkin_var = $today_checkout_var = $booking_time = "";

		// recorremos el array de post que trae la consulta
		while ($loop->have_posts()) : $loop->the_post();
			?>

			<?php
			$order_id = $loop->post->ID;


			$order = new WC_Order($order_id);

			// if ($order->post_status == 'wc-on-hold') {

			$order_items = $order->get_items();
			$order_meta = get_post_custom($order_id);
			$user = $order->get_user();

			$cumpleanios = get_the_author_meta('birthday', $user->ID);


			$nombre = isset($order_meta["_billing_first_name"][0]) ? $order_meta["_billing_first_name"][0] : null;
			$apellido = isset($order_meta["_billing_last_name"][0]) ? $order_meta["_billing_last_name"][0] : null;
			$cedula = isset($order_meta["_billing_cedula"][0]) ? $order_meta["_billing_cedula"][0] : null;
			$direccion = isset($order_meta["_billing_address_1"][0]) ? $order_meta["_billing_address_1"][0] : null;
			$departamento = "";
			$departamento .= "<p>" . isset($order_meta["_billing_city"][0]) ? $order_meta["_billing_city"][0] : null . "</p> ";
			$departamento .= "<p>" . isset($order_meta["_billing_state"][0]) ? $order_meta["_billing_state"][0] : null . "</p> ";
			$email = isset($order_meta["_billing_email"][0]) ? $order_meta["_billing_email"][0] : null;
			$telefono = isset($order_meta["_billing_phone"][0]) ? $order_meta["_billing_phone"][0] : null;
			$celular = isset($order_meta["billing_phone_2"][0]) ? $order_meta["billing_phone_2"][0] : null;

			$uso_cupon = "no";
			$coupons_list = [];
			$cupones = "";
			$promocion_cupon = "";
			$nombreDistribuidor = "Orden vieja";


			$order = wc_get_order($order_id);
			$shipping_items = $order->get_items('shipping');
			$used = (int)reset($shipping_items)['method_id'];
			$distId = get('distribuidor', 1, 1, $used);
			$nombreDistribuidor = get_the_title($distId);


			if ($order->get_used_coupons()) {
				$uso_cupon = "si";
				$coupons = array();
				foreach ($order->get_used_coupons() as $value) {
					$args = array(
						's' => $value,
						'post_type' => 'shop_coupon',
						'post_status' => 'publish',
					);
					$coupons[$value] = get_posts($args);

				}

				$promociones = [];
				foreach ($coupons as $coupon) {
					$promociones[] = get_post_meta($coupon[0]->ID, 'promocion_cupon_field', true);

					$coupons_list[] = $coupon[0]->post_title;
				}


				$nombres_promociones = array();
				foreach ($promociones as $promocion) {
					$prom = get_post($promocion);
					$nombres_promociones[] = $prom->post_title;
				}

				$promocion_cupon = implode(" - ", $nombres_promociones);

				$cupones = implode(" - ", $coupons_list);
			}


			foreach ($order_items as $key => $item) {

				$categoria = wp_get_post_terms($item["product_id"], 'product_cat');
				$categoria = $categoria[0];

				$marca = wp_get_post_terms($item["product_id"], 'marca');
				$marca = $marca[0];

				$uso_promocion = "no";

				$producto_tipo_promocion = get_post_meta($item["product_id"], 'producto_tipo_promocion');

				if ($producto_tipo_promocion) {
					if ($producto_tipo_promocion[0] == "1") {
						$uso_promocion = "si";
					}
				}

				$var .= "<tr>";
				$var .= "<td>" . $order->order_date . "</td>";
				$var .= "<td>" . $nombre . "</td>";
				$var .= "<td>" . $apellido . "</td>";
				$var .= "<td>" . $email . "</td>";
				$var .= "<td>" . $cedula . "</td>";
				$var .= "<td>" . $direccion . "</td>";
				$var .= "<td>" . $telefono . " - " . $celular . "</td>";
				$var .= "<td>" . $cumpleanios . "</td>";
				$var .= "<td>" . $item["name"] . "</td>";
				$var .= "<td>" . $categoria->name . "</td>";
				$var .= "<td>" . $marca->name . "</td>";
				$var .= "<td>" . $uso_promocion . "</td>";
				$var .= "<td>" . $uso_cupon . "</td>";
				$var .= "<td>" . $item["qty"] . "</td>";
				$var .= "<td>$ " . number_format($item["line_total"], 0, null, '.') . "</td>";
				$var .= "<td>" . str_replace("-", " ", $item["pa_shipping"]) . "</td>";
				$var .= "<td>" . $cupones . "</td>";
				$var .= "<td>" . $promocion_cupon . "</td>";
				$var .= "<td>" . $nombreDistribuidor . "</td>";
				$var .= "</tr>";
			}

		endwhile;

		$swf_path = plugins_url() . "/mocion-wc-export-orders/TableTools/media/swf/copy_csv_xls.swf";
		?>

        <script>

            jQuery(document).ready(function () {
                var oTable = jQuery('.datatable').dataTable({
                    "bJQueryUI": true,
                    "sScrollX": "",
                    "bSortClasses": false,
                    "aaSorting": [[0, 'desc']],
                    "bAutoWidth": true,
                    "bInfo": true,
                    "sScrollY": "100%",
                    "sScrollX": "100%",
                    "bScrollCollapse": true,
                    "sPaginationType": "full_numbers",
                    "bRetrieve": true,
                    "oLanguage": {
                        "sSearch": "Buscar:",
                        "sInfo": "Ver _START_ a _END_ de _TOTAL_ Ordenes",
                        "sInfoEmpty": "Ver 0 a 0 de 0 entries",
                        "sZeroRecords": "No Ordenes Completed yet",
                        "sInfoFiltered": "(filtered from _MAX_total entries)",
                        "sEmptyTable": "No Ordenes available in table",
                        "sLengthMenu": "Numéro de ordenes para ver: _MENU_",
                        "oPaginate": {
                            "sFirst": "Primera",
                            "sPrevious": "Anterior",
                            "sNext": "Siguiente",
                            "sLast": "Ultima"
                        }
                    },
                    "sDom": 'T<"clear"><"H"lfr>t<"F"ip>',
                    "oTableTools": {
                        "sSwfPath": "<?php echo plugins_url(); ?>/mocion-wc-export-orders/TableTools/media/swf/copy_csv_xls_pdf.swf"
                    }

                });
            });


        </script>

        <div>
            <table id="order_history" class="display datatable">
                <thead>
                <tr>
                    <th><?php _e('Fecha', 'woo-export-order'); ?></th>
                    <th><?php _e('Nombre', 'woo-export-order'); ?></th>
                    <th><?php _e('Apellido', 'woo-export-order'); ?></th>
                    <th><?php _e('Email', 'woo-export-order'); ?></th>
                    <th><?php _e('Cédula', 'woo-export-order'); ?></th>
                    <th><?php _e('Dirección', 'woo-export-order'); ?></th>
                    <th><?php _e('Teléfono', 'woo-export-order'); ?></th>
                    <th><?php _e('Fecha de nacimiento', 'woo-export-order'); ?></th>
                    <th><?php _e('Producto Comprado', 'woo-export-order'); ?></th>
                    <th><?php _e('Categoria', 'woo-export-order'); ?></th>
                    <th><?php _e('Marca', 'woo-export-order'); ?></th>
                    <th><?php _e('Promoción', 'woo-export-order'); ?></th>
                    <th><?php _e('Código', 'woo-export-order'); ?></th>
                    <th><?php _e('Cantidad', 'woo-export-order'); ?></th>
                    <th><?php _e('Precio Producto', 'woo-export-order'); ?></th>
                    <th><?php _e('Tipo pedido', 'woo-export-order'); ?></th>
                    <th><?php _e('Código usado', 'woo-export-order'); ?></th>
                    <th><?php _e('Promoción del código usado', 'woo-export-order'); ?></th>
                    <th><?php _e('Distribuidor', 'woo-export-order'); ?></th>
                </tr>
                </thead>
                <tbody>
				<?php echo $var; ?>
                </tbody>
            </table>
        </div>
    </form>

	<?php
}

?>
