��    J      l  e   �      P     Q     k     ~     �     �     �     �     �     �     �            )     )   I  	   s  9   }  
   �  L   �          +  	   ;     E     \     p     �     �     �     �  
   �     �     �  A   �  �   &	     �	     �	     	
     
     /
     <
     B
  
   T
     _
     f
     l
     p
     t
     �
     �
     �
     �
     �
     �
     �
  c   �
  2   U     �     �     �  	   �     �     �     �     �  
          /   !  /   Q  .   �  /   �  /   �  .        ?     V  �  k     S     h     �     �     �     �     �     �     �               "     /  %   L     r  -   {  	   �  <   �     �               %     ;     T     s     �     �  !   �     �     �     �  @   �  ~        �     �     �     �     �     �                    #     *     1     8     N     ^     z  	   ~     �     �     �  `   �  *     	   =     G     Z     g     n     u     �     �  	   �     �  *   �  )   �  &     (   E  )   n  &   �     �     �           7          (             ,      J           ;   G                  6                  @   A       '                  $   C              F   4   ?       0              	      .                
   E         3       D   =   "   )   9               I       1   #   >   -      B   *             &   :   %      H   5   <                       8   2            +   !   /    "%s" is a required field. (max file size %s) Actions Add Global Add-on Add one? Add-on deleted successfully Add-on saved successfully Add-ons Add/Edit Global Add-on All Products Applied to... Applies to... Are you sure you want delete this option? Are you sure you want remove this add-on? Category: Check this to exclude this product from all Global Addons Checkboxes Choose categories which should show these addons (or apply to all products). Choose some options&hellip; Click to toggle Close all Custom Input Multipler Custom input (text) Custom input (textarea) Custom price input Delete Edit Error: Global Add-on not found Expand all Export File upload Give this global add-on a reference/name to make it recognisable. Give this global addon a priority - this will deternmine the order in which multiple groups of addons get displayed on the frontend. Per-product add-ons will always have priority 10. Global Add-on Group Global Add-on Reference Global Add-ons Global Addon Exclusion Grand total: Group Group Description Group Name Import Label Max Min New Addon Group New&nbsp;Option No global add-ons exists yet. None Number of Fields Option Label Option Price Options total: Paste exported form data here and then save to import fields. The imported fields will be appended. Please enter a value greater than 0 for "%s - %s". Priority Product category notifications Radio buttons Reference Remove Required fields? Save Global Add-on Select an option... Select box Select options The maximum allowed amount for "%s - %s" is %s. The maximum allowed length for "%s - %s" is %s. The maximum allowed value for "%s - %s" is %s. The minimum allowed amount for "%s - %s" is %s. The minimum allowed length for "%s - %s" is %s. The minimum allowed value for "%s - %s" is %s. This must be a number! characters remaining Project-Id-Version: Woocommerce.Product.Addons_v2.4.3
POT-Creation-Date: 2014-06-21 01:47+0800
PO-Revision-Date: 2014-06-21 02:10+0800
Last-Translator: Vincent <vincent@webmart.tw>
Language-Team: webmart.tw <service@webmart.tw>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.5
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_x
X-Poedit-Basepath: .
Language: zh_TW
X-Poedit-SearchPath-0: /woocommerce-product-addons
 "%s" 為必填欄位 (最大檔案大小 %s) 動作 添加全局附加產品 添加一個 ? 附加產品已刪除 附加產品已儲存 附加產品 添加/編輯全局附加產品 所有商品 套用至... 套用至... 你確定要刪除此選項? 你確定要刪除這個附加產品? 分類 : 勾選要在全局附加產品排除的商品 複選框 選擇顯示這些產品的類別 (或應用到全部商品) 選擇一些選項&hellip; 點選切換 關閉全部 自定義輸入倍數 自定義輸入 (文字) 自定義輸入 (文字區域) 輸入自定義價格 刪除 編輯 錯誤 : 沒有發現全局附加 展開全部 匯出 檔案上傳 給這個全局附加配件一個參照/名稱，使其可識別 給這個全局附加產品一個優先權 - 這將決定多組附加功能顯示在前台的順序。10 將始終優先處理 全局附加產品群組 全局附加產品參照 附加配件 排除全局附加 全部合計 : 群組 群組描述 群組名稱 匯入 標籤 最大 最小 新附加產品群組 新&nbsp;選項 還沒有全局附加產品 無 欄位數 選項標籤 選項價格 選項總計 : 在這裡貼上匯出表單數據，然後儲存到匯入欄位。匯入的欄位將被追加。 請為"%s - %s"輸入一個大於 0 的值 優先級 商品分類通知 單選按鈕 參照 移除 必填欄位? 儲存全局附加產品 選擇一個選項… 選擇框 選擇選項 允許的最大長度為 "%s - %s" 是 %s. 允許的最大數額為 "%s - %s" 是 %s 允許的最大值為 "%s - %s" 是 %s 允許的最小長度為"%s - %s" 是 %s 允許的最小數額為 "%s - %s" 是 %s 允許的最小值為 "%s - %s" 是 %s 必須是數字！ 剩餘字符 