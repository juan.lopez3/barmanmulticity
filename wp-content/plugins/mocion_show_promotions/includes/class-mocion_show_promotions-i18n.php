<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       mocionsoft.com
 * @since      1.0.0
 *
 * @package    Mocion_show_promotions
 * @subpackage Mocion_show_promotions/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Mocion_show_promotions
 * @subpackage Mocion_show_promotions/includes
 * @author     wittolfr <juan.lopez@mocionsoft.com>
 */
class Mocion_show_promotions_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'mocion_show_promotions',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
