<?php

/**
 * Fired during plugin activation
 *
 * @link       mocionsoft.com
 * @since      1.0.0
 *
 * @package    Mocion_show_promotions
 * @subpackage Mocion_show_promotions/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Mocion_show_promotions
 * @subpackage Mocion_show_promotions/includes
 * @author     wittolfr <juan.lopez@mocionsoft.com>
 */
class Mocion_show_promotions_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
