<?php

/**
 * Fired during plugin deactivation
 *
 * @link       mocionsoft.com
 * @since      1.0.0
 *
 * @package    Mocion_show_promotions
 * @subpackage Mocion_show_promotions/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Mocion_show_promotions
 * @subpackage Mocion_show_promotions/includes
 * @author     wittolfr <juan.lopez@mocionsoft.com>
 */
class Mocion_show_promotions_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
