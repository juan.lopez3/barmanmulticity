<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       mocionsoft.com
 * @since      1.0.0
 *
 * @package    Mocion_show_promotions
 * @subpackage Mocion_show_promotions/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
