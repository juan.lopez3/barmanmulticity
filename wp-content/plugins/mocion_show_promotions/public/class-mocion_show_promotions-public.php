<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       mocionsoft.com
 * @since      1.0.0
 *
 * @package    Mocion_show_promotions
 * @subpackage Mocion_show_promotions/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Mocion_show_promotions
 * @subpackage Mocion_show_promotions/public
 * @author     wittolfr <juan.lopez@mocionsoft.com>
 */
class Mocion_show_promotions_Public {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }
    
    /**
     * 
     * @global type $product
     * @param string $content
     * @return string
     */
    public function add_promotion_description($content) {
        global $product;
        if (!$product){
            return $content;
        }
        $descripciones = [];
         $active_rules_sets = get_if_product_active_rule_price($product->id);

        foreach ($active_rules_sets as $set) {
            $descripciones[$set['description']] = "<div='promo_titulo'>".$set['description']."</div>"."<div class='promo_descripcion'>".$set['full_description']."</div>";
        }

        if ($descripciones) {
            $content = $content . "<div class='promo_descripciones'>" . implode(" ", $descripciones) . "</div>";
        }

        return $content;
    }

    /*
    public function alter_short_product_description_header($d) {
        return "<h3>description</h3>" . $d;
    }

    public function alter_short_product_description($d) {
        return "<h3>description</h3>" . $d;
    }

    public function show_product_promotion_in_product_page_before() {
        echo "<h2>beforeeeeeeeeeeeeeeeee</h2>";
    }

    public function show_product_promotion_in_product_page() {
        echo "<h2>xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx</h2>";
    }*/

    /**
     * Returns warranty data about a cart item
     *
     * @param array $other_data
     * @param array $cart_item
     * @return array $other_data
     */
    public function show_product_promotion_in_cart($other_data, $cart_item) {
        $_product = $cart_item['data'];

        $warranty = get_post_meta($_product->id, '_warranty', true);

        $WCDPD = $GLOBALS['RP_WCDPD'];

        // $adjustment =  $WCDPD->pricing->get($cart_item_key);
        /*
          $name = "Promotion";
          $value = "Uno muy bueno";
          $discount_data = array(
          'name'      => $name,
          'value'     => $value,
          'display'   => ''
          ); */
        array_unshift($other_data, $discount_data);

        return $other_data;
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Mocion_show_promotions_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Mocion_show_promotions_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/mocion_show_promotions-public.css', array(), $this->version, 'all');
    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Mocion_show_promotions_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Mocion_show_promotions_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/mocion_show_promotions-public.js', array('jquery'), $this->version, false);
    }

}
