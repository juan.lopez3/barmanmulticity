<?php
	global $wp;
	$current_url = home_url(add_query_arg(array(),$wp->request));
	$register_url = home_url().'/registro?uri='.$current_url;//get_option( 'cl_register_url', '');
	$restore_url  = home_url() . '/restaurar-contrasena';
?>
<div class="cleanlogin-container">		

	<form class="cleanlogin-form" method="post" action="#">

		<input type="hidden" name="redirect_to" value="<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; ?>" />
			
		<fieldset>

			<div class="cleanlogin-field">
			<label for="log">Correo Electr&oacute;nico</label>
				<input class="cleanlogin-field-email" type="text" name="log" placeholder="<?php echo __( 'Correo electronico', 'cleanlogin' ); ?>">
			</div>
			
			<div class="cleanlogin-field">
			<label for="pwd">Contraseña</label> 
				<input class="cleanlogin-field-password" type="password" name="pwd" placeholder="<?php echo __( 'Password', 'cleanlogin' ); ?>">
			</div>
		</fieldset>
		
		<fieldset>
			<div class="cleanlogin-field cleanlogin-field-remember">
				<input type="checkbox" name="rememberme" value="forever">
				<label for="rememberme"><?php echo __( 'Remember?', 'cleanlogin' ); ?></label>
			</div>
			<input class="cleanlogin-field" id="login_button" type="submit" value="<?php echo __( 'Log in', 'cleanlogin' ); ?>" name="submit">
			<input type="hidden" name="action" value="login">
			
			
		</fieldset>
		
		<div class="cleanlogin-form-bottom">
			
			<?php if ( $restore_url != '' )
				echo "<a href='$restore_url' class='cleanlogin-form-pwd-link'>". __( 'Lost password?', 'cleanlogin' ) ."</a>";
			?>

			<?php if ( $register_url != '' )
				echo "<a href='$register_url' id='register_button' class='cleanlogin-form-register-link'>". __( 'Register', 'cleanlogin' ) ."</a>";
			?>
						
		</div>
		
	</form>

</div>
