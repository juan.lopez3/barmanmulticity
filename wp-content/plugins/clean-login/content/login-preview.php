<?php
	global $current_user;
	get_currentuserinfo();
	$edit_url = get_option( 'cl_edit_url', '');
	$show_user_information = get_option( 'cl_hideuser' ) == 'on' ? false : true;

?>

<div class="cleanlogin-container" >
	<div class="cleanlogin-preview">
		<div class="cleanlogin-preview-top">
		<h4>
			<?php
				if ( $show_user_information ) : echo $current_user->user_login; endif;
			 ?>
			<small><?php echo $current_user->user_firstname . ' ' . $current_user->user_lastname; ?></small>
		</h4>
			<a href="<?php echo add_query_arg( 'action', 'logout' ); ?>" class="cleanlogin-preview-logout-link"><?php echo __( 'Log out', 'cleanlogin' ); ?></a>	
			<?php 
				//echo "<a href='$edit_url' class='cleanlogin-preview-edit-link'>". __( 'Edit my profile', 'cleanlogin' ) ."</a>";
			?>
		</div>

		<?php //echo do_shortcode('[wp-favorite-posts]'); ?>
	</div>		
</div>