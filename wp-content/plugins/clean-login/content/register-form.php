<div class="cleanlogin-container cleanlogin-full-width">
	<form class="cleanlogin-form" method="post" action="#">

		<fieldset>
			<div class="mensajeantes">
				Regístrate para estar al tanto de los beneficios, promociones y eventos que Barman Club tiene para ti.
			</div>	
                    
                        <div id="register_nombre" class="cleanlogin-field">
				<label for="firstName"><?php echo __( 'Nombre', 'cleanlogin' ); ?>*</label>
				<input type="text" name="firstName" value="<?php if(isset($current_user->user_email)) {echo $current_user->user_email;} ?>" maxlength="50">
				<span class="error"></span>
			</div>
                    
                        <div id="register_apellido" class="cleanlogin-field">
				<label for="lastName"><?php echo __( 'Apellido', 'cleanlogin' ); ?>*</label>
				<input type="text" name="lastName" value="<?php if(isset($current_user->user_email)) {echo $current_user->user_email;} ?>" maxlength="50">
				<span class="error"></span>
			</div>

                        <div id="register_genero" class="cleanlogin-field">
				<label for="gender"><?php echo __( 'Genero', 'cleanlogin' ); ?>*</label>
				<!--<input type="text" name="email" value="<?php if(isset($current_user->user_email)) {echo $current_user->user_email;} ?>" maxlength="50">-->
				<input type="radio" name="gender" value="M" checked> Hombre<br>
                                <input type="radio" name="gender" value="F"> Mujer<br>
                                <span class="error"></span>
			</div>
                    
			<div id="register_birth" class="cleanlogin-field">
				<label for="birthday"><?php echo __( 'Fecha de nacimiento', 'cleanlogin' ); ?>*</label>
				<input class="date" type="text" name="birthday" value="<?php if(isset($user_id)) {echo get_user_meta( $user_id, 'birthday', true);} ?>" onkeydown="return false;">
				<span class="error"></span>
			</div>
                    
                    	<div id="register_email" class="cleanlogin-field">
				<label for="email"><?php echo __( 'E-mail', 'cleanlogin' ); ?>*</label>
				<input type="text" name="email" value="<?php if(isset($current_user->user_email)) {echo $current_user->user_email;} ?>" maxlength="50">
				<span class="error"></span>
			</div>

			<div id="register_password1" class="cleanlogin-field">
                                <label for="pass1"><?php echo __( 'Contraseña', 'cleanlogin' ); ?>*</label>
				<input class="cleanlogin-field-password" type="password" name="pass1" value="" autocomplete="off" placeholder="<?php echo __( 'Contraseña' ) ?>" maxlength="30">
				<span class="error"></span>
			</div>
			
			<div id="register_password2" class="cleanlogin-field">
                                <label for="pass2"><?php echo __( 'Confirme la contraseña', 'cleanlogin' ); ?>*</label>
				<input class="cleanlogin-field-password" type="password" name="pass2" value="" autocomplete="off" placeholder="<?php echo __( 'Confirme la contraseña' ) ?>" maxlength="30">
				<span class="error"></span>
			</div>

			<div id="register_terminos" class="cleanlogin-field">
				<input class="input-checkbox" type="checkbox" name="terms" checked>
				<label for="terms" class="checkbox">Autorizo voluntariamente a PERNOD RICARD COLOMBIA para tratar mis datos personales de acuerdo con lo dispuesto en la Ley 1581 de 2012, el Decreto Único 1074 de 2015 y la política de tratamiento de datos personales de Pernod Ricard Colombia. La Información será utilizada para contactarme e informarme sobre actividades de publicidad, mercadeo, comerciales, fidelización, encuestas y eventos.
				</label>
			</div>

			<div id="register_derechos" class="cleanlogin-field">
				<input class="input-checkbox" type="checkbox" name="derechos" checked>
				<label for="derechos" class="checkbox">Declaro que he leído y acepto la política de tratamiento de datos personales de Pernod Ricard Colombia y he sido informado de los derechos a conocer, actualizar, rectificar y suprimir datos personales, así como revocar la autorización a través del correo electrónico datoscol@pernod-ricard.com.
					<a href="http://www.drinks.me/politicaPernodRicard" target="_blank">
						Política tratamiento de datos: www.drinks.me/politicaPernodRicard
					</a>
				</label>
			</div>

			<?php /*check if captcha is checked */ if ( get_option( 'cl_antispam' ) == 'on' ) : ?>
				<div class="cleanlogin-field">
					<img src="<?php echo plugins_url( 'captcha', __FILE__ ); ?>"/>
					<input class="cleanlogin-field-spam" type="text" name="captcha" value="" autocomplete="off" placeholder="<?php echo __( 'Escribe el texto de arriba' ) ?>" >
				</div>
			<?php endif; ?>


		</fieldset>

		<div>	
			<input type="submit" value="<?php echo __( 'SUSCRIBIRSE' ) ?>" name="submit" onclick="return validate_register(this);">
			<input type="hidden" name="action" value="register">	
		</div>

	</form>
</div>

<?php //wp_enqueue_script( 'register-form', get_template_directory_uri() . '/assets/js/util.js'); 
	wp_enqueue_script( 'register-form-utils', get_bloginfo('template_directory') . '/assets/js/util.js'); 
    wp_enqueue_script( 'register-form-custom', plugin_dir_url(__FILE__).'custom-clean-login.js');  
?>
