<?php
/**
* Plugin Name: Custom report coupon
* Plugin URI: http://filter_promocion_page.com/
* Description: Filtro de promociones para la agina de administracion de cupones
* Version: 1.0 or whatever version of the plugin (pretty self explanatory)
* Author: Plugin Author's Name
* Author URI: Author's website
* License: A "Slug" license name e.g. GPL12
*/

include 'admin-page.php';

add_action( 'admin_menu', 'custom_report_coupon_menu' );

function custom_report_coupon_menu() {
	add_management_page( 'Reporte', 'Reporte', 'manage_options', 'custom_report_coupon/admin-page.php', 'custom_report_coupon_page', 'dashicons-admin-page'  );

}