<?php

function custom_report_coupon_page() { ?>

    <div class="wrap">

        <h2>Reporte - Uso de Cupones</h2>

        <a class="cpac-edit add-new-h2" href="<?php echo get_site_url(); ?>/wp-admin/admin.php?page=export_orders_page" style="
           margin: 5px 5px 0 0;
           float: left;
           ">EXPORTAR</a>

        <a class="cpac-edit add-new-h2" href="<?php echo get_site_url(); ?>/wp-admin/edit.php?post_type=shop_coupon" style="
           margin: 5px 5px 0 0;
           float: left;
           ">VOLVER</a>

        <form id="posts-filter" action="?page=custom_report_coupon/admin-page.php" method="get">

            <input type="hidden" name="page" class="post_page" value="<?php echo!empty($_GET['page']) ? esc_attr($_GET['page']) : 'all'; ?>" />
            <?php if (!empty($_REQUEST['show_sticky'])) { ?>
                <input type="hidden" name="show_sticky" value="1" />
                <?php
            }

            $select = new WP_List_Table;
            $select->months_dropdown("shop_order");

            submit_button(__('Filter'), 'button', 'filter_action', false, array('id' => 'post-query-submit'));
            ?>

            <table class="wp-list-table widefat fixed posts">
                <thead><tr>
                        <th scope="col" id="coupon_code" class="manage-column column-coupon_code" style="">Fecha</th>
                        <th scope="col" id="coupon_code" class="manage-column column-coupon_code" style="">Código</th>
                        <th scope="col" id="column-meta" class="manage-column column-column-meta" style="">Pedido</th>
                        <th scope="col" id="coupon_code" class="manage-column column-coupon_code" style="">Uso</th>
                    </tr></thead>

                <tfoot> <tr>				
                        <th scope="col" id="coupon_code" class="manage-column column-coupon_code" style="">Fecha</th>
                        <th scope="col" id="coupon_code" class="manage-column column-coupon_code" style="">Código</th>
                        <th scope="col" id="column-meta" class="manage-column column-column-meta" style="">Pedido</th>
                        <th scope="col" id="coupon_code" class="manage-column column-coupon_code" style="">Uso</th>
                    </tr> </tfoot>

                <tbody id="the-list">

                    <?php
                    $args = array(
                        'post_type' => 'shop_order',
                        'post_status' => 'publish',
                        'posts_per_page' => '-1',
                    );

                    if (!empty($_GET["m"])) {
                        $args['m'] = $_GET["m"];
                    }

                    $loop = new WP_Query($args);

                    // recorremos el array de post que trae la consulta 
                    while ($loop->have_posts()) : $loop->the_post();
                        ?>

                        <?php
                        $order_id = $loop->post->ID;

                        $order = wc_get_order($order_id);

                        $coupons = $order->get_used_coupons();

                        // si si se usaron cupones en la orden
                        // se muestra en el reporte 
                        if (!empty($coupons)) :

                            $nombre = $order->billing_first_name . ' ' . $order->billing_last_name;

                            $fecha = $loop->post->post_date;

                            foreach ($coupons as $key => $coupon) :
                                ?>

                                <tr id="post-<?php echo $order_id; ?>" class="post-<?php echo $order_id; ?> type-shop_order status-publish hentry alternate iedit author-other level-0">

                                    <td>
                <?php echo $fecha; ?>
                                    </td>

                                    <td class="coupon_code column-coupon_code">									
                                        <div class="code tips">
                                            <a href="<?php echo $loop->post->guid; ?>&amp;action=edit">
                                                <span><?php echo $coupon; ?></span>
                                            </a>
                                        </div>									
                                    </td>

                                    <td class="column-meta column-column-meta">
                                        Pedido # <?php echo $order_id; ?>
                                    </td>

                                    <td>
                <?php echo $nombre; ?>
                                    </td>

                                </tr>

            <?php endforeach; ?>


                        <?php endif; ?>

    <?php endwhile; ?>

                </tbody>
            </table>

        </form>

    </div>
    <?php
}
