<?php
/**
 * Plugin Name: mocion_p2p_allow_only_national_products
 * Description: Oculta los productos que no están disponibles para todas las ciudades
 * Version: 1.0
 * Author: MocionG
 */

/**
 * @todo
 * 1  Conseguir todas los metodos de envío
 * 2  conseguir los productos que si se deben mostrar validando por los metodos de envío
 * 3  {array} productos validos
 * 4  Conseguir todos los productos menos los de la consulta anterior
 * 5  {array} productos que no son nacionales
 * 6  retornar array de excluidos
 */


add_filter('p2p_connectable_args', 'allow_only_national_products', 10, 3);
function allow_only_national_products($extra_qv, $obj, $item) {

	$productIdsValid = [];
	$metodos = [];
	$attribute = "shipping";

	/**
	 * @step 1
	 */
	WooCommerce_Advanced_Shipping()->was_shipping_method();
	$methods = get_posts(array('posts_per_page' => '-1', 'post_type' => 'was', 'orderby' => 'menu_order', 'order' => 'ASC'));
	foreach ($methods as $m) {
		$metodos[] = array(
			'taxonomy' => 'pa_' . $attribute,
			'terms' => [$m->post_title],
			'field' => 'slug',
			'operator' => 'IN'
		);
	}
	$args = array(
		'post_type' => 'product',
		'posts_per_page' => -1,
		'tax_query' => array(
			'relation' => "AND",
			$metodos
		)
	);

	/**
	 * @step 2
	 */
	$productos = get_posts($args);

	foreach ($productos as $producto) {

		/**
		 * @step 3
		 */
		$productIdsValid[] = $producto->ID;
	}

	/**
	 * @step 4
	 */
	$argsHide = array(
		'post_type' => 'product',
		'posts_per_page' => -1,
		'post__not_in' => $productIdsValid,
	);

	/**
	 * @step 5
	 */
	$productos = get_posts($argsHide);

	foreach ($productos as $p) {
		/**
		 * @step 6
		 */
		$extra_qv["p2p:exclude"][] = $p->ID;
	}

	/**
	 * @step 7
	 */
	return $extra_qv;
}
