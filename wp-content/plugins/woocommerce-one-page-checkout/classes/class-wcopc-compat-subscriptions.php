<?php
/**
 * @package		WooCommerce One Page Checkout
 * @subpackage	Subscriptions Extension Compatibility
 * @category	Template Class
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Class to hold subscriptions compat functionality until merge with the main extension for dev purposes
 */
class WCOPC_Compat_Subscriptions {

	public function __construct() {

		if ( class_exists( 'WC_Subscriptions_Cart' ) ) {
			add_action( 'woocommerce_before_checkout_form', __CLASS__ . '::set_checkout_registration', 10 );
		}
	}

	/**
	 * Override subsbcriptions setting of enable_guest_checkout to false and set to true - we will hide it using JS
	 *
	 * @param  array 
	 * @return void
	 */
	public static function set_checkout_registration( $checkout = '' ) {

		if ( ! is_user_logged_in() && PP_One_Page_Checkout::is_any_form_of_opc_page() ) {
			$checkout->enable_guest_checkout = true;
		}
	}


}
new WCOPC_Compat_Subscriptions();