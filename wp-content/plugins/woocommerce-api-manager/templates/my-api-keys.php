<?php
/**
 * My API Keys
 * @since 1.3.6
 */
?>

<h2><?php _e( 'My API Keys', 'woocommerce-api-manager' ); ?></h2>

<?php
if ( ! empty( $user_id ) ) :

	$user_orders = WCAM()->helpers->get_users_data( $user_id );

	if ( ! empty( $user_orders ) ) :

	?>

		<table class="shop_table my_account_api_manager my_account_orders">

			<thead>
				<tr>
					<th class="api-manager-software-title"><span class="nobr"><?php _e( 'Product', 'woocommerce-api-manager' ); ?></span></th>
					<th class="api-manager-key"><span class="nobr"><?php _e( 'API License Key', 'woocommerce-api-manager' ); ?></span></th>
					<th class="api-manager-email"><span class="nobr"><?php _e( 'API License Email', 'woocommerce-api-manager' ); ?></span></th>
					<th class="api-manager-activation"><span class="nobr"><?php _e( 'Activations', 'woocommerce-api-manager' ); ?></span></th>
				<?php if ( WCAM()->helpers->is_plugin_active( 'woocommerce-subscriptions/woocommerce-subscriptions.php' ) ) : ?>
					<th class="api-manager-subscription"><span class="nobr"><?php _e( 'Subscription', 'woocommerce-api-manager' ); ?></span></th>
				<?php endif; ?>
				</tr>
			</thead>
			<tbody>
			<?php

				// Sort products alphbetically by Software Title
				$user_orders = WCAM()->array->array_uasort_by_key( $user_orders, 'software_title' );

				foreach ( $user_orders as $order_key => $data ) :

				/**
				 * Prepare the Subscription information
				 */

				// Finds the post ID (integer) for a product even if it is a variable product
				if ( $data['is_variable_product'] == 'no' ) {

					$post_id 	= $data['parent_product_id'];

				} else {

					$post_id 	= $data['variable_product_id'];

				}

				// Finds order ID that matches the license key. Order ID is the post_id in the post meta table
				$order_id 	= $data['order_id'];

				// Finds the product ID, which can only be the parent ID for a product
				$product_id = $data['parent_product_id'];

				// Get the subscription status i.e. active
				if ( WCAM()->helpers->is_plugin_active( 'woocommerce-subscriptions/woocommerce-subscriptions.php' ) ) {

					$status = WCAM()->helpers->get_subscription_status( $user_id, $post_id, $order_id, $product_id );

				}

				// End Subscription information prep

				// Software Title
				if ( $data['is_variable_product'] == 'no' ) {

					$software_title = $data['_api_software_title_parent'];

				} else if ( $data['is_variable_product'] == 'yes' ) {

					$software_title = $data['_api_software_title_var'];

				} else {

					$software_title = $data['software_title'];

				}

				// Check if this is an order_key, or the new longer api_key
				$order_data_key = ( ! empty( $data['api_key'] ) ) ? $data['api_key'] : $data['order_key'];

				// Get the order
				$customer_order = get_post( $data['order_id'] );

				if ( is_object( $customer_order ) ) {

					// WooCommerce 2.2 compatibility
					if ( function_exists( 'wc_get_order' ) ) {
						$order = wc_get_order( $order_id );
					} else {
						$order = new WC_Order();
					}

					$order->populate( $customer_order );

				}

			?>

				<tr class="order">
					<td class="api-manager-product">
						<a href="<?php echo esc_url(  $order->get_view_order_url() ); ?>"><?php echo esc_attr( $software_title ) . ' ' . $order->get_order_number(); ?></a>
					</td>
					<td class="api-manager-license-key">
						<?php
						if ( ! empty( $status ) && $status != 'active' && WCAM()->helpers->is_plugin_active( 'woocommerce-subscriptions/woocommerce-subscriptions.php' ) ) echo '<del>';
						if ( $data['_api_update_permission'] == 'no' ) echo '<del>';
						echo $order_data_key;
						if ( $data['_api_update_permission'] == 'no' ) echo '</del>';
						if ( ! empty( $status ) && $status != 'active' && WCAM()->helpers->is_plugin_active( 'woocommerce-subscriptions/woocommerce-subscriptions.php' ) ) echo '</del>';
						?>
					</td>
					<td class="api-manager-license-email">
						<?php echo esc_attr( $data['license_email'] ); ?>
					</td>
					<td class="api-manager-activations">
						<?php
						// Get activation info
						$a_info = WCAM()->helpers->get_users_activation_data( $user_id, $data['order_key'] );

						$active_activations = 0;

						if ( ! empty( $a_info ) ) :

					    	foreach ( $a_info as $key => $activations ) :

					    		if ( $activations['activation_active'] == 1 && $activations['order_key'] == $order_data_key ) :

									$active_activations++;

								endif; // end if activations

							endforeach; // end a_info

						endif; // not empty a_info

						$order_data = WCAM()->helpers->get_order_info_by_email_with_order_key( $data['license_email'], $order_data_key );

						if ( ! empty( $order_data ) ) :

							// Activations limit or unlimited
							if ( $order_data['is_variable_product'] == 'no' && $order_data['_api_activations_parent'] != '' ) :

								$activations_limit = absint( $order_data['_api_activations_parent'] );

							elseif ( $order_data['is_variable_product'] == 'no' && $order_data['_api_activations_parent'] == '' ) :

								$activations_limit = 'unlimited';

							elseif ( $order_data['is_variable_product'] ==  'yes' && $order_data['_api_activations'] != '' ) :

								$activations_limit = absint( $order_data['_api_activations'] );

							elseif ( $order_data['is_variable_product'] == 'yes' && $order_data['_api_activations'] == '' ) :

								$activations_limit = 'unlimited';

							endif;

							$num_activations = ( $active_activations  > 0 ) ? $active_activations : 0;

							$my_acccount_activations = $num_activations . __( ' out of ', 'woocommerce-api-manager' ) . $activations_limit;

							if ( $data['_api_update_permission'] == 'no' ) :

								$my_acccount_activations = __( 'Disabled', 'woocommerce-api-manager' );

							endif; // if No API Access Permission

							if ( ! empty( $status ) && $status != 'active' && WCAM()->helpers->is_plugin_active( 'woocommerce-subscriptions/woocommerce-subscriptions.php' ) ) :

								$my_acccount_activations = __( 'Disabled', 'woocommerce-api-manager' );

							endif; // if subscription is not active

							echo esc_attr( $my_acccount_activations );

							endif; // not empty order_data

						?>
					</td>
				<?php if ( WCAM()->helpers->is_plugin_active( 'woocommerce-subscriptions/woocommerce-subscriptions.php' ) ) : ?>
					<td class="api-manager-subscriptions">
						<?php
							if ( ! empty( $status ) ) :

								echo esc_attr( $status );

							else :

								 _e( 'No', 'woocommerce-api-manager' );

							endif;
						?>

					</td>
				<?php endif; ?>
				</tr>

				<?php $current_info = WCAM()->helpers->get_users_activation_data( $user_id, $order_data['order_key'] ); ?>
				<?php
					foreach ( $current_info as $key => $activation_info ) :
						if ( $activation_info['order_key'] == $order_data_key ) :
				?>
				<tr class="api-manager-domains">
					<td colspan="1" style="border-right: 0;"><a href="<?php echo esc_url( WCAM()->helpers->nonce_url( array( 'domain' => $activation_info['activation_domain'], 'instance' => $activation_info['instance'], 'order_key' => $data['order_key'], 'user_id' => $user_id, 'api_key' => $order_data_key  ) ) ); ?>" style="float: right;" class="button <?php echo sanitize_html_class( 'delete' ) ?>"><?php echo apply_filters( 'api_manager_my_account_delete', __( 'Delete', 'woocommerce-api-manager' ) ); ?></a></td>
					<td <?php echo ( WCAM()->helpers->is_plugin_active( 'woocommerce-subscriptions/woocommerce-subscriptions.php' ) ? 'colspan="4"' : 'colspan="3"' ); ?> class="api-manager-activations" style="text-align:left; vertical-align: middle; border-left: 0; padding-left: 0;">
						<a href="<?php echo esc_url( $activation_info['activation_domain'] ); ?>" target="_blank"><?php echo WCAM()->helpers->remove_url_prefix( $activation_info['activation_domain'] ); ?></a><?php echo ' activated on ' . date_i18n( __( 'M j\, Y \a\t h:i a', 'woocommerce-api-manager' ), strtotime( $activation_info['activation_time'] ) ); ?>
						<?php if ( ! empty( $status ) && $status != 'active' && WCAM()->helpers->is_plugin_active( 'woocommerce-subscriptions/woocommerce-subscriptions.php' ) ) : ?>
							<span style="background-color:#faebeb; border:1px solid #dc7070; color:#212121; padding:2px">
							<?php _e( 'Domain activated with invalid API License Key.', 'woocommerce-api-manager' ); ?>
							</span>
						<?php endif; ?>
					</td>
				</tr>

			<?php endif; // end if order_key ?>

			<?php endforeach; // end current_info ?>

			<?php endforeach; // end user_orders ?>

			</tbody>

		</table>

	<?php else : ?>

	<p><?php _e( 'You have no API keys.', 'woocommerce-api-manager' ); ?></p>

	<?php endif; // end if user_orders ?>

<?php endif; // end if user_id
