<?php // API Manager ?>

<?php if ( count( $keys ) > 0 && ! empty( $keys ) ) : ?>

	<?php // if ( get_option( 'woocommerce_api_manager_remove_email_link' ) == 'yes' || get_option( 'woocommerce_api_manager_remove_all_download_links' ) == 'yes' ) : ?>

		<h2><?php _e( 'File Downloads', 'woocommerce-api-manager' ); ?></h2>

			<p><a href="<?php echo esc_url( get_permalink( wc_get_page_id( 'myaccount' ) ) ); ?>"><?php _e( 'Download Files', 'woocommerce-api-manager' ); ?></a></p>

	<?php // endif; ?>

	<h2><?php ( WCAM()->helpers->get_billing_country( $order_id ) == 'GB' ) ? _e( 'API Licence Key:', 'woocommerce-api-manager' ) : _e( 'API License Key:', 'woocommerce-api-manager' ); ?></h2>

	<?php foreach ( $keys as $key ) :

		if ( WCAM()->helpers->get_uniqid_prefix( $key['api_key'], '_am_' ) == $order_key || $key['api_key'] == $order_key ) :

			// Determine the Software Title from the customer order data
			$software_title = ( empty( $key['_api_software_title_var'] ) ) ? $key['_api_software_title_parent'] : $key['_api_software_title_var'];

			if ( empty( $software_title ) ) :

				$software_title = $key['software_title'];

			endif;

			// Check if this is an order_key, or the new longer api_key
			$order_data_key = ( ! empty( $key['api_key'] ) ) ? $key['api_key'] : $key['order_key'];

	?>

			<h4><?php echo $software_title; ?></h4>

			<ul>
				<li><?php ( WCAM()->helpers->get_billing_country( $key['order_id'] ) == 'GB' ) ? _e( 'API Licence Key:', 'woocommerce-api-manager' ) : _e( 'API License Key:', 'woocommerce-api-manager' ); ?> <strong><?php echo $order_data_key; ?></strong></li>
				<li><?php ( WCAM()->helpers->get_billing_country( $key['order_id'] ) == 'GB' ) ? _e( 'API Licence Email:', 'woocommerce-api-manager' ) : _e( 'API License Email:', 'woocommerce-api-manager' ); ?> <strong><?php echo $key['license_email']; ?></strong></li>
			</ul>

		<?php endif; ?>

	<?php endforeach; ?>

<?php endif; ?>
