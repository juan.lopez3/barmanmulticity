<?php
/**
 * API Downloads
 * @since 1.3.6
 */
?>

<h2><?php _e( 'My API Downloads', 'woocommerce-api-manager' ); ?></h2>

<?php

if ( ! empty( $user_id ) ) :

	$user_orders = WCAM()->helpers->get_users_data( $user_id );

	if ( ! empty( $user_orders ) ) :

		$dropbox_app_key = get_option( 'woocommerce_api_manager_dropbox_dropins_saver' );
	?>

		<table class="shop_table my_account_api_manager my_account_orders">

			<thead>
				<tr>
					<th class="api-manager-software-product"><span class="nobr"><?php _e( 'Product', 'woocommerce-api-manager' ); ?></span></th>
					<th class="api-manager-version"><span class="nobr"><?php _e( 'Version', 'woocommerce-api-manager' ); ?></span></th>
					<th class="api-manager-version-date"><span class="nobr"><?php _e( 'Version Date', 'woocommerce-api-manager' ); ?></span></th>
					<th class="api-manager-documentation"><span class="nobr"><?php _e( 'Documentation', 'woocommerce-api-manager' ); ?></span></th>
					<th class="api-manager-download"><span class="nobr"><?php _e( 'Download', 'woocommerce-api-manager' ); ?></span></th>
				</tr>
			</thead>
			<tbody>
			<?php

				// Sort products alphbetically by Software Title, and remove duplicates
				$user_orders = WCAM()->array->array_remove_duplicate_by_key( $user_orders, 'software_title', 'uasort' );

				foreach ( $user_orders as $order_key => $data ) :

				/**
				 * Prepare the Subscription information
				 */

				// Finds the post ID (integer) for a product even if it is a variable product
				if ( $data['is_variable_product'] == 'no' ) {

					$post_id 	= $data['parent_product_id'];

				} else {

					$post_id 	= $data['variable_product_id'];

				}

				// Finds order ID that matches the license key. Order ID is the post_id in the post meta table
				$order_id 	= $data['order_id'];

				// Finds the product ID, which can only be the parent ID for a product
				$product_id = $data['parent_product_id'];

				// Get the subscription status i.e. active
				if ( WCAM()->helpers->is_plugin_active( 'woocommerce-subscriptions/woocommerce-subscriptions.php' ) ) {

					$status = WCAM()->helpers->get_subscription_status( $user_id, $post_id, $order_id, $product_id );

				}

				// End Subscription information prep

				// Software Title
				if ( $data['is_variable_product'] == 'no' ) {

					$software_title = $data['_api_software_title_parent'];

				} else if ( $data['is_variable_product'] == 'yes' ) {

					$software_title = $data['_api_software_title_var'];

				} else {

					$software_title = $data['software_title'];

				}

				$download_id = WCAM()->helpers->get_download_id( $post_id );

				$downloadable_data = WCAM()->helpers->get_downloadable_data( $data['order_key'], $data['license_email'], $post_id, $download_id );

				if ( is_object( $downloadable_data ) ) {
					$downloads_remaining 	= $downloadable_data->downloads_remaining;
					$download_count 		= $downloadable_data->download_count;
					$access_expires 		= $downloadable_data->access_expires;
				}

				$download_count_set = WCAM()->helpers->get_download_count( $order_id, $data['order_key'] );

				$download = WCAM()->helpers->get_download_url( $post_id );

				// Check if there is download permission
				if ( ! empty( $status ) && $status != 'active' && WCAM()->helpers->is_plugin_active( 'woocommerce-subscriptions/woocommerce-subscriptions.php' ) ) {
					$no_download = true;
				} else if ( $data['_api_update_permission'] != 'yes' || $downloads_remaining == '0' || empty( $downloadable_data ) || $download_count_set === false || $access_expires > 0 && strtotime( $access_expires ) < current_time( 'timestamp' ) ) {
					$no_download = true;
				} else {
					$no_download = false;
				}

			if ( $no_download === false ) :
			?>

				<tr class="order">
					<td class="api-manager-downloads-product">
						<?php
						$download_product = get_permalink( $product_id );
						if ( ! empty( $download_product ) ) :
						?>
							<a href="<?php echo esc_url( $download_product ) ?>" target="_blank"><?php echo esc_attr( $software_title ) ?></a>
						<?php
						endif;
						?>
					</td>
					<td class="api-manager-version">
						<?php
						$download_version = get_post_meta( $post_id, '_api_new_version', true );
						if ( ! empty( $download_version ) ) :
							echo esc_attr( $download_version );
						endif;
						?>
					</td>
					<td class="api-manager-version-date">
						<?php
						$version_date = get_post_meta( $post_id, '_api_last_updated', true );
						if ( ! empty( $version_date ) ) :
							echo esc_attr( date_i18n( $version_date ) );
						else :
							esc_html_e( 'No Date', 'woocommerce-api-manager' );
						endif;
						?>
					</td>
					<td class="api-manager-changelog">
						<?php
						$changelog = get_post_meta( $post_id, '_api_changelog', true );
						if ( ! empty( $changelog ) ) :
						?>
							<a href="<?php echo esc_url( get_permalink( absint( $changelog ) ) ); ?>" target="_blank"><?php esc_html_e( 'Changelog', 'woocommerce-api-manager' ); ?></a>
						<?php
						endif;
						?>
						<br><hr>
						<?php
						$documentation = get_post_meta( $post_id, '_api_product_documentation', true );
						if ( ! empty( $documentation ) ) :
						?>
							<a href="<?php echo esc_url( get_permalink( absint( $documentation ) ) ); ?>" target="_blank"><?php esc_html_e( 'Documentation', 'woocommerce-api-manager' ); ?></a>
						<?php
						endif;
						?>
					</td>
					<td class="api-manager-download">
						<?php
						if ( ! empty( $download ) && WCAM()->helpers->find_amazon_s3_in_url( $download ) === true && $no_download === false ) :
						?>
						<a href="<?php echo esc_url( WCAM()->helpers->format_secure_s3_url( $download ) ); ?>" target="_blank"><?php esc_html_e( 'Download', 'woocommerce-api-manager' ); ?></a>
						<?php
						elseif ( ! empty( $download ) && WCAM()->helpers->find_amazon_s3_in_url( $download ) === false && $no_download === false ) : ?>
							<a href="<?php echo esc_url( WCAM()->helpers->create_url( $data['order_key'], $data['license_email'], $post_id, $download_id, $user_id ) ); ?>" target="_blank"><?php esc_html_e( 'Download', 'woocommerce-api-manager' ); ?></a>
						<?php
						else :
							esc_html_e( 'Disabled', 'woocommerce-api-manager' );
						endif;
						?>
						<br><hr>
						<?php
						if ( ! empty( $dropbox_app_key ) && ! empty( $download ) && WCAM()->helpers->find_amazon_s3_in_url( $download ) === true && $no_download === false ) :
						?>
						<a href="<?php echo esc_url( WCAM()->helpers->format_secure_s3_url( $download ) ); ?>" class="dropbox-saver nobr"></a>
						<?php
						elseif ( ! empty( $dropbox_app_key ) && ! empty( $download ) && WCAM()->helpers->find_amazon_s3_in_url( $download ) === false && $no_download === false ) : ?>
							<a href="<?php echo esc_url( WCAM()->helpers->create_url( $data['order_key'], $data['license_email'], $post_id, $download_id, $user_id ) ); ?>" class="dropbox-saver nobr"></a>
						<?php
						elseif ( empty( $dropbox_app_key ) ) :
							echo '&nbsp;';
						else :
							esc_html_e( 'Disabled', 'woocommerce-api-manager' );
						endif;
						?>
					</td>
				</tr>
			<?php
				endif; // end if download disabled
				endforeach; // end user_orders

			?>

			</tbody>

		</table>

	<?php else : ?>

	<p><?php _e( 'You have no downloads.', 'woocommerce-api-manager' ); ?></p>

	<?php endif; // end if user_orders ?>

<?php endif; // end if user_id

/**
 * Javascript
 */
if ( get_option( 'woocommerce_api_manager_remove_all_download_links' ) == 'yes' ) :
ob_start();
?>
		jQuery('h2:contains("Available downloads")').css('display', 'none');
		jQuery('.digital-downloads').css('display', 'none');
<?php
	$javascript = ob_get_clean();
	WCAM()->wc_print_js( $javascript );
endif; // end if remove

if ( ! empty( $dropbox_app_key ) ) :
?>

	<script type="text/javascript" src="https://www.dropbox.com/static/api/2/dropins.js" id="dropboxjs" data-app-key="<?php esc_attr_e( $dropbox_app_key ) ?>"></script>

<?php endif; ?>
