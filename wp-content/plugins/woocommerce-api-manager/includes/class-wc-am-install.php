<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * WooCommerce API Manager Install Class
 *
 * @package API Manager/Install
 * @author Todd Lahman LLC
 * @copyright   Copyright (c) Todd Lahman LLC
 * @since 1.3.4
 *
 */
class WC_Api_Manager_Install {

	/**
	 * @var The single instance of the class
	 */
	protected static $_instance = null;

	/**
	 * @static
	 * @return class instance
	 */
	public static function instance() {

		if ( is_null( self::$_instance ) && ! is_object( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * Cloning is forbidden.
	 *
	 */
	private function __clone() {}

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 */
	private function __wakeup() {}

	public function __construct() {

		// Installation
		register_activation_hook( WAM_PLUGIN_FILE, array( $this, 'install' ) );

		add_action( 'admin_init', array( $this, 'check_version' ), 5 );

	}

	/**
	 * check_version function.
	 *
	 * @access public
	 * @return void
	 */
	public function check_version() {
		if ( ! defined( 'IFRAME_REQUEST' ) && ( get_option( WCAM()->wc_api_manager_version_name ) != WCAM()->version ) ) {
			$this->install();

			do_action( 'woocommerce_api_manager_updated' );
		}
	}

	/**
	 * Handles tasks when plugin is activated
	 */
	public function install() {
		global $wpdb;

		if ( WooCommerce_API_Manager::$wc_am_self_upgrade ) {

			$global_options = array(
				'api_key' 			=> '',
				'activation_email' 	=> '',
						);

			update_option( 'wc_api_manager', $global_options );

			// Generate a unique installation $instance id
			$instance = WCAM()->helpers->generate_key( 12, false );

			$single_options = array(
				'wc_api_manager_product_id' 			=> 'WooCommerce Upgrade API Manager',
				'wc_api_manager_instance' 				=> $instance,
				'wc_api_manager_deactivate_checkbox' 	=> 'on',
				'wc_api_manager_activated' 				=> 'Deactivated',
				);

			foreach ( $single_options as $key => $value ) {
				update_option( $key, $value );
			}

		}

		// Create the lost API key page
		$lost_api_key_page_id = get_option( 'woocommerce_lost_license_page_id' );

		// Creates the lost API key page with the right shortcode in it
		$slug = 'lost-api-key';
		$found = $wpdb->get_var( "SELECT ID FROM " . $wpdb->posts . " WHERE post_name = '$slug' LIMIT 1;" );

		if ( empty( $lost_api_key_page_id ) || ! $found ) {
			$lost_api_key_page = array(
				'post_title' 	=> _x( 'Lost API Key', 'Title of a page', 'woocommerce-api-manager' ),
				'post_content' 	=> '[woocommerce_api_manager_lost_api_key]',
				'post_status' 	=> 'publish',
				'post_type' 	=> 'page',
				'post_name' 	=> $slug,
			);
			$lost_api_key_page_id = (int) wp_insert_post( $lost_api_key_page );
			update_option( 'woocommerce_lost_license_page_id', $lost_api_key_page_id );
		}

		/**
		 * Prepare account data
		 * @since 1.3
		 */
		if ( WCAM()->helpers->check_am_data_exists() === true ) {

			WCAM()->tools->rebuild_api_data();

		}

		$curr_ver = get_option( WCAM()->wc_api_manager_version_name );

		// checks if the current plugin version is lower than the version being installed
		if ( version_compare( WCAM()->version, $curr_ver, '>' ) ) {
			// update the version
			update_option( WCAM()->wc_api_manager_version_name, WCAM()->version );
		}

		/**
		 * @since 1.3.4
		 */
		if ( get_option( 'woocommerce_api_manager_activation_order_note' ) === false ) {
			update_option( 'woocommerce_api_manager_activation_order_note', 'yes' );
		}

		if ( get_option( 'woocommerce_api_manager_url_expire' ) === false ) {
			update_option( 'woocommerce_api_manager_url_expire', 15 );
		}

		if ( get_option( 'woocommerce_api_manager_remove_all_download_links' ) === false ) {
			update_option( 'woocommerce_api_manager_remove_all_download_links', 'yes' );
		}

		// Install secure hash table
		$this->secure_hash_table_install();

	}

	/**
	 * Set up the secure hash database table
	 * @return void
	 */
	private function secure_hash_table_install() {
		global $wpdb;

		$wpdb->hide_errors();

		$collate = '';

	    if ( $wpdb->has_cap( 'collation' ) ) {
			if( ! empty( $wpdb->charset ) ) {
				$collate .= "DEFAULT CHARACTER SET $wpdb->charset";
			}
			if( ! empty($wpdb->collate ) ) {
				$collate .= " COLLATE $wpdb->collate";
			}
	    }

	    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

	    if ( ! $wpdb->get_var( "SHOW TABLES LIKE '{$wpdb->prefix}woocommerce_api_manager_secure_hash';" ) ) {
		    $tables = "
				CREATE TABLE {$wpdb->prefix}woocommerce_api_manager_secure_hash (
				  hash_id bigint(20) NOT NULL auto_increment,
				  hash_user_id bigint(20),
				  hash_name varchar(255) NOT NULL,
				  hash_value varchar(255) NOT NULL,
				  hash_time varchar(255) NOT NULL,
				  PRIMARY KEY  (hash_id),
				  KEY hash_name (hash_name)
				) $collate;
				";
		    dbDelta( $tables );
		}
	}

}

WC_Api_Manager_Install::instance();
