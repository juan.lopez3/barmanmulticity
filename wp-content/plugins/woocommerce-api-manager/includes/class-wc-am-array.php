<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * WooCommerce API Manager Array Formatting Class
 *
 * @package API Manager/Array Formatting
 * @author Todd Lahman LLC
 * @copyright   Copyright (c) Todd Lahman LLC
 * @since 1.3.7
 *
 */

class WC_Api_Manager_Array {

	/**
	 * @var The single instance of the class
	 */
	protected static $_instance = null;

	/**
	 * @static
	 * @return class instance
	 */
	public static function instance() {

		if ( is_null( self::$_instance ) && ! is_object( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * Cloning is forbidden.
	 *
	 */
	private function __clone() {}

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 */
	private function __wakeup() {}

	/**
	 * Variables
	 */
	public $array_compare_by_key;

	/**
	 * Merges arrays recursively with an associative key
	 * To merge arrays that are numerically indexed use the PHP array_merge_recursive() function
	 *
	 * @since 1.1
	 * @param $array1 Array
	 * @param $array2 Array
	 * @return array
	 */
	public function array_merge_recursive_associative( $array1, $array2 ) {

		$merged_arrays = $array1;

		if ( is_array( $array2 ) ) {
			foreach ( $array2 as $key => $val ) {
				if ( is_array( $array2[$key] ) ) {
					$merged_arrays[$key] = ( isset( $merged_arrays[$key] ) && is_array( $merged_arrays[$key] ) ) ? $this->array_merge_recursive_associative( $merged_arrays[$key], $array2[$key] ) : $array2[$key];
				} else {
					$merged_arrays[$key] = $val;
				}
			}
		}

		return $merged_arrays;
	}

	/**
	 * array_merge_recursive does indeed merge arrays, but it converts values with duplicate
	 * keys to arrays rather than overwriting the value in the first array with the duplicate
	 * value in the second array, as array_merge does. I.e., with array_merge_recursive,
	 * this happens (documented behavior):
	 *
	 * array_merge_recursive(array('key' => 'org value'), array('key' => 'new value'));
	 *     => array('key' => array('org value', 'new value'));
	 *
	 * array_merge_recursive_distinct does not change the datatypes of the values in the arrays.
	 * Matching keys' values in the second array overwrite those in the first array, as is the
	 * case with array_merge, i.e.:
	 *
	 * array_merge_recursive_distinct(array('key' => 'org value'), array('key' => 'new value'));
	 *     => array('key' => array('new value'));
	 *
	 * Parameters are passed by reference, though only for performance reasons. They're not
	 * altered by this function.
	 *
	 * @param array $array1
	 * @param array $array2
	 * @return array
	 * @author Daniel <daniel (at) danielsmedegaardbuus (dot) dk>
	 * @author Gabriel Sobrinho <gabriel (dot) sobrinho (at) gmail (dot) com>
	 * @since 1.1.1
	 */
	public function array_merge_recursive_distinct ( array &$array1, array &$array2 ) {
		$merged = $array1;

		foreach ( $array2 as $key => &$value ) {

			if ( is_array( $value ) && isset( $merged [$key] ) && is_array( $merged [$key] ) ) {

				$merged [$key] = array_merge_recursive_distinct ( $merged [$key], $value );

			} else {

				$merged [$key] = $value;

			}

		}

		return $merged;
	}

	/**
	 * Experimental
	 * @since 1.1.1
	 * @param  mixed  $needle
	 * @param  mixed  $haystack
	 * @param  boolean $strict
	 * @return boolean
	 */
	public function in_array_recursive( $needle, $haystack, $strict = false ) {

		foreach ( $haystack as $item ) {

			if ( ( $strict ? $item === $needle : $item == $needle ) || ( is_array( $item ) && $this->in_array_recursive( $needle, $item, $strict ) ) ) {

				return true;

			}

		}

		return false;
	}

	/**
	 * Removes element from array based on key
	 * @since 1.1
	 * @return array New array minus removed elements
	 *
	 * For example:
	 *
	 * $fruit_inventory = array(
	 *	  'apples' => 52,
	 *	  'bananas' => 78,
	 *	  'peaches' => 'out of season',
	 *	  'pears' => 'out of season',
	 *	  'oranges' => 'no longer sold',
	 *	  'carrots' => 15,
	 *	  'beets' => 15,
	 *	);
	 *
	 * $fruit_inventory = array_remove_by_key($fruit_inventory,
     *                              "beets",
     *                              "carrots");
	 */
	public function array_remove_by_key() {

		$args = func_get_args();

		return array_diff_key( $args[0], array_flip( array_slice( $args, 1 ) ) );

	}

	/**
	 * Removes element from array based on value
	 * @since 1.1
	 * @return array New array minus removed elements
	 * For example:
	 *
	 * $fruit_inventory = array(
	 *	  'apples' => 52,
	 *	  'bananas' => 78,
	 *	  'peaches' => 'out of season',
	 *	  'pears' => 'out of season',
	 *	  'oranges' => 'no longer sold',
	 *	  'carrots' => 15,
	 *	  'beets' => 15,
	 *	);
	 *
	 * $fruit_inventory = array_remove_by_value($fruit_inventory,
     *                                 "out of season",
     *                                 "no longer sold");
	 */
	public function array_remove_by_value() {

		$args = func_get_args();

		return array_diff( $args[0], array_slice( $args, 1 ) );

	}

	/**
	 * array_search_multi Finds if a value matched with a needle exists in a multidimensional array
	 * @param  array $array  multidimensional array (for simple array use array_search)
	 * @param  mixed $value  value to search for
	 * @param  mixed $needle needle that needs to match value in array
	 * @return boolean
	 *
	 * @since 1.1.1
	 */
	public function array_search_multi( $array, $value, $needle ) {

		foreach( $array as $index_key => $value_key ) {

			if ( $value_key[$value] === $needle ) {
				return true;
			}

		}

		return false;
	}

	/**
	 * get_array_search_multi Finds a key for a value matched by a needle in a multidimensional array
	 * @param  array $array  multidimensional array (for simple array use array_search)
	 * @param  mixed $value  value to search for
	 * @param  mixed $needle needle that needs to match value in array
	 * @return mixed
	 *
	 * @since 1.1.1
	 */
	public function get_key_array_search_multi( $array, $value, $needle ) {

		foreach( $array as $index_key => $value_key ) {

			if ( $value_key[$value] === $needle ) {
				return $value_key;
			}

		}

		return false;
	}

	/**
	 * Searches multidimensional array, and removes duplicate nested arrays.
	 * @param  array $array The array to search
	 * @param  string $key   The nested array key to search for.
	 * @param  string $uasort [description]
	 * @return array        A new array that has only unique nested arrays.
	 * @uses array_compare_by_key()
	 *
	 * @since 1.3.7
	 */
	public function array_remove_duplicate_by_key( $array, $key, $sort = '' ) {
		$unique_array = array(); // The results will be loaded into this array.
		$unique_index_keys = array(); // The list of keys will be added here.

		if ( ! is_array( $array ) || empty( $array ) || empty( $key ) ) {
			return array();
		}

		foreach ( $array as $nested_array ) { // Iterate through your array.
			if ( ! in_array( $nested_array[$key], $unique_index_keys ) ) { // Check to see if this is a key that's already been used before.
				$unique_index_keys[] = $nested_array[$key]; // If the key hasn't been used before, add it into the list of keys.
				$unique_array[] = $nested_array; // Add the nested array into the unique_array.
			}
		}

		if ( $sort == 'uasort' ) { // Sort an array with a user-defined comparison function and maintain index association
			$this->array_compare_by_key = $key;
			uasort( $unique_array, array( $this, 'array_compare_by_key' ) );
		}

		return $unique_array;
	}

	/**
	 * Sorts an array alphabetically by key
	 * @param  array $array
	 * @param  string $key
	 * @return array
	 * @uses array_compare_by_key()
	 *
	 * @since 1.3.7
	 */
	public function array_uasort_by_key( $array, $key ) {
		$this->array_compare_by_key = $key;
		uasort( $array, array( $this, 'array_compare_by_key' ) );

		return $array;
	}

	/**
	 * Callback method for sorting an array by key
	 * @param  array $a
	 * @param  array $b
	 * @return array
	 *
	 * @since 1.3.7
	 */
	public function array_compare_by_key( $a, $b ) {
		return strcmp( $a[$this->array_compare_by_key], $b[$this->array_compare_by_key] );
	}


} // end class
