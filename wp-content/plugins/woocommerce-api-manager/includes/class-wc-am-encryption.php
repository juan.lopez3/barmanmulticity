<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * WooCommerce API Manager Encryption Class
 *
 * Encrypts a string value.
 *
 * @package API Manager/Encryption
 * @author Todd Lahman LLC
 * @copyright   Copyright (c) Todd Lahman LLC
 * @since 1.3.2
 *
 */
class WC_Api_Manager_Encryption {

	/**
	 * @var The single instance of the class
	 */
	protected static $_instance = null;

	/**
	 * @static
	 * @return class instance
	 */
	public static function instance() {

		if ( is_null( self::$_instance ) && ! is_object( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * Cloning is forbidden.
	 *
	 */
	private function __clone() {}

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 */
	private function __wakeup() {}

	/**
	 *
	 * Encrypt a string
	 * @since 1.3.2
	 * @param string $string value to encrypt
	 * @return string encrypted string
	 */
	public function encrypt( $string ) {

		if ( empty( $string ) ) {
			return $string;
		}

		//only encrypt if needed
		if ( stripos( $string, '$Api_Manager$ENC1$' ) !== FALSE or stripos( $string, '$Api_Manager$RIJNDAEL$' ) !== FALSE ) {
			return $string;
		}

		$nonce_salt = ( defined( 'NONCE_SALT' ) ) ? NONCE_SALT : ']K4eR{$@^@.Cb*P6+i0 jg&qEa8+V H-@N>:WuL/pW^z9nEte j|]{w!i!B~|saD';

		$key = md5( DB_NAME . DB_USER . DB_PASSWORD . $nonce_salt );

		if ( ! function_exists( 'mcrypt_encrypt' ) ) {
			$result = '';
			for ( $i = 0; $i < strlen( $string ); $i ++ ) {
				$char    = substr( $string, $i, 1 );
				$keychar = substr( $key, ( $i % strlen( $key ) ) - 1, 1 );
				$char    = chr( ord( $char ) + ord( $keychar ) );
				$result .= $char;
			}

			return '$Api_Manager$ENC1$' . base64_encode( $result );
		}

		return '$Api_Manager$RIJNDAEL$' . base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_256, md5( $key ), $string, MCRYPT_MODE_CBC, md5( md5( $key ) ) ) );
	}

	/**
	 *
	 * Decrypt a string
	 * @since 1.3.2
	 * @param string $string value to decrypt
	 * @return string decrypted string
	 */
	public function decrypt( $string ) {

		if ( empty( $string ) ) {
			return $string;
		}

		$nonce_salt = ( defined( 'NONCE_SALT' ) ) ? NONCE_SALT : ']K4eR{$@^@.Cb*P6+i0 jg&qEa8+V H-@N>:WuL/pW^z9nEte j|]{w!i!B~|saD';

		$key = md5( DB_NAME . DB_USER . DB_PASSWORD . $nonce_salt );

		if ( stripos( $string, '$Api_Manager$ENC1$' ) !== FALSE ) {
			$string = str_ireplace( '$Api_Manager$ENC1$', '', $string );
			$result = '';
			$string = base64_decode( $string );
			for ( $i = 0; $i < strlen( $string ); $i ++ ) {
				$char    = substr( $string, $i, 1 );
				$keychar = substr( $key, ( $i % strlen( $key ) ) - 1, 1 );
				$char    = chr( ord( $char ) - ord( $keychar ) );
				$result .= $char;
			}

			return $result;
		}

		if ( function_exists( 'mcrypt_encrypt' ) && stripos( $string, '$Api_Manager$RIJNDAEL$' ) !== FALSE) {
			$string = str_ireplace( '$Api_Manager$RIJNDAEL$', '', $string );

			return rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_256, md5( $key ), base64_decode( $string ), MCRYPT_MODE_CBC, md5( md5( $key ) ) ), "\0" );
		}

		return $string;
	}
}
