<?php

/**
 * WooCommerce API Manager Admin Settings Class
 *
 * @package WooCommerce/Update API Manager/Settings Admin
 * @author Todd Lahman LLC
 * @copyright   Copyright (c) Todd Lahman LLC
 * @since 1.3
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class WC_API_Manager_Admin_Settings {

	/**
	 * The WooCommerce settings tab name
	 *
	 * @since 1.3
	 */
	public $tab_name = 'api_manager';

	/**
	 * The prefix for API Manager settings
	 *
	 * @since 1.0
	 */
	public $option_prefix = 'woocommerce_api_manager';

	/**
	 * @var The single instance of the class
	 */
	protected static $_instance = null;

	/**
	 * @static
	 * @return class instance
	 */
	public static function instance() {

		if ( is_null( self::$_instance ) && ! is_object( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * Cloning is forbidden.
	 *
	 */
	private function __clone() {}

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 */
	private function __wakeup() {}

	public function __construct() {

		add_filter( 'woocommerce_settings_tabs_array', array( $this, 'add_api_manager_settings_tab' ), 60 );

		add_action( 'woocommerce_settings_tabs_api_manager', array( $this, 'api_manager_settings_page' ) );

		add_action( 'woocommerce_update_options_' . $this->tab_name, array( $this, 'update_api_manager_settings' ) );

		// Custom Amazon S3 Secret Access Key form field that is encrypted and decrypted
		add_action( 'woocommerce_update_option_wc_am_s3_secret_key', array( $this, 'encrypt_secret_key' ) );
		add_action( 'woocommerce_admin_field_wc_am_s3_secret_key', array( $this, 'secret_key_field' ) );

	}

	/**
	 * Add the API Manager settings tab to the WooCommerce settings tabs array.
	 * @since 1.3
	 *
	 * @param array $settings_tabs Array of WooCommerce setting tabs and their labels, excluding the API Manager tab.
	 * @return array $settings_tabs Array of WooCommerce setting tabs and their labels, including the API Manager tab.
	 */
	public function add_api_manager_settings_tab( $settings_tabs ) {

		$settings_tabs[ $this->tab_name ] = __( 'API Manager', 'woocommerce-api-manager' );

		return $settings_tabs;
	}

	/**
	 * Uses the WooCommerce admin fields API to output settings via the @see woocommerce_admin_fields() function.
	 *
	 * @uses woocommerce_admin_fields()
	 * @uses $this->get_settings()
	 * @since 1.3
	 */
	public function api_manager_settings_page() {

		woocommerce_admin_fields( $this->get_settings() );

		$tools = $this->get_tools();

		if ( ! empty( $_GET['action'] ) && ! empty( $_REQUEST['_wpnonce'] ) && wp_verify_nonce( $_REQUEST['_wpnonce'], 'debug_action' ) ) {

			switch ( $_GET['action'] ) {
				case "rebuild_api_data" :

					WCAM()->tools->rebuild_api_data();

					echo '<div class="updated"><p>' . __( 'API Manager data rebuilt. All customers will need to get their new API Keys to reactivate software.', 'woocommerce-api-manager' ) . '</p></div>';
				break;
				case "rebuild_downloadable_table" :

					WCAM()->tools->rebuild_downloadable_product_permissions();

					echo '<div class="updated"><p>' . __( 'WooCommerce Downloadable Product Permissions table rebuilt.', 'woocommerce-api-manager' ) . '</p></div>';
				break;
				default:
					$action = esc_attr( $_GET['action'] );
					if( isset( $tools[ $action ]['callback'] ) ) {
						$callback = $tools[ $action ]['callback'];
						$return = call_user_func( $callback );
						if( $return === false ) {
							if( is_array( $callback ) ) {
								echo '<div class="error"><p>' . sprintf( __( 'There was an error calling %s::%s', 'woocommerce-api-manager' ), get_class( $callback[0] ), $callback[1] ) . '</p></div>';

							} else {
								echo '<div class="error"><p>' . sprintf( __( 'There was an error calling %s', 'woocommerce-api-manager' ), $callback ) . '</p></div>';
							}
						}
					}
				break;
			}
		}

		include( WCAM()->plugin_path() . '/admin/views/html-settings-admin-tools.php' );

	}

	/**
	 * Get tools
	 *
	 * @return array of tools
	 */
	public function get_tools() {
		return apply_filters( 'woocommerce_api_manager_tools', array(
			'rebuild_api_data'	=> array(
				'name'		=> __('API Manager Data','wc_api_manager'),
				'button'	=> __('Rebuild API Manager Data','wc_api_manager'),
				'desc'		=> __( '<strong class="red">Warning - Backup Database First</strong> This tool will delete and rebuild all API Manager customer order data, and searchable API Keys. All API Manager API Key activations will also be deleted, so <strong>customers will need to reactivate software with the new API Keys that will be generated</strong>. Manually added API Keys will not be rebuilt, and will need to be recreated manually on the order screen. One API Key will be recreated per line item. Only use this tool as a last resort.', 'woocommerce-api-manager' ),
			),
			'rebuild_downloadable_table' => array(
				'name'		=> __( 'Download Permissions', 'woocommerce-api-manager' ),
				'button'	=> __( 'Rebuild Download Permissions', 'woocommerce-api-manager' ),
				'desc'		=> __( '<strong class="red">Warning - Backup Database First</strong> This tool will delete and rebuild the woocommerce_downloadable_product_permissions table. Downloadable product permissions that were added manually will need to be recreated manually on the order screen.  Only use this tool as a last resort.', 'woocommerce-api-manager' ),
			),
		) );
	}

	/**
	 * Get all the settings for the API Manager extension in the format required by the @see woocommerce_admin_fields() function.
	 *
	 * @return array Array of settings in the format required by the @see woocommerce_admin_fields() function.
	 * @since 1.3
	 */
	public function get_settings() {

		return apply_filters( 'woocommerce_api_manager_settings', array(

			array(
				'name'	=> __( 'Order Screen', 'woocommerce-api-manager' ),
				'type'	=> 'title',
				'desc'	=> '',
				'id'	=> $this->option_prefix . '_order_screen'
			),

			array(
				'name'				=> __( 'Activation Order Note', 'woocommerce-api-manager' ),
				'desc'				=> __( 'Record Activation/Deactivation Order Notes', 'woocommerce-api-manager' ),
				'id'				=> $this->option_prefix . '_activation_order_note',
				'default'			=> 'no',
				'type'				=> 'checkbox',
				'desc_tip'			=> __( 'Records an order note when a customer activates/deactivates software with a valid API Key.', 'woocommerce-api-manager' ),
				'checkboxgroup'		=> 'start',
				'show_if_checked'	=> 'option',
			),

			array( 'type'	=> 'sectionend', 'id' => $this->option_prefix . '_order_screen' ),

			array(
				'name'	=> __( 'Amazon S3', 'woocommerce-api-manager' ),
				'type'	=> 'title',
				'desc'	=> '',
				'id'	=> $this->option_prefix . '_amazon_s3'
			),

			array(
				'name'		=> __( 'Access Key ID', 'woocommerce-api-manager' ),
				'desc'		=> __( 'The Amazon Web Services Access Key ID.', 'woocommerce-api-manager' ),
				'tip'		=> '',
				'id'		=> $this->option_prefix . '_amazon_s3_access_key_id',
				'css'		=> 'min-width:250px;',
				'default'	=> '',
				'type'		=> 'text',
				'desc_tip'	=> false,
			),

			array(
				'name'		=> __( 'Secret Access Key', 'woocommerce-api-manager' ),
				'desc'		=> __( 'The Amazon Web Services Secret Access Key is securely encrypted in the database.', 'woocommerce-api-manager' ),
				'tip'		=> '',
				'id'		=> $this->option_prefix . '_amazon_s3_secret_access_key',
				'css'		=> 'min-width:250px;',
				'default'	=> '',
				'type'		=> 'wc_am_s3_secret_key',
				'desc_tip'	=> __( 'The Amazon Web Services Secret Access Key.', 'woocommerce-api-manager' ),
			),

			array( 'type'	=> 'sectionend', 'id' => $this->option_prefix . '_order_screen' ),

			array(
				'name'	=> __( 'Download Links', 'woocommerce-api-manager' ),
				'type'	=> 'title',
				'desc'	=> '',
				'id'	=> $this->option_prefix . '_download_links'
			),

			// array(
			// 	'name'				=> __( 'Remove Email Download Link', 'woocommerce-api-manager' ),
			// 	'desc'				=> __( 'Do not display a download link in emails or in My Account > Order Details.', 'woocommerce-api-manager' ),
			// 	'id'				=> $this->option_prefix . '_remove_email_link',
			// 	'default'			=> 'no',
			// 	'type'				=> 'checkbox',
			// 	'checkboxgroup'		=> 'start',
			// 	'show_if_checked'	=> 'option',
			// 	'desc_tip'			=> __( 'The Email and My Account > Order Details download links expire 24 hours after they are generated.', 'woocommerce-api-manager' ),
			// ),

			array(
				'name'		=> __( 'URL Expire Time', 'woocommerce-api-manager' ),
				'desc'		=> __( 'Expiration time in minutes, for Amazon S3 and local server WooCommerce URLs.', 'woocommerce-api-manager' ),
				'id'		=> $this->option_prefix . '_url_expire',
				'default'	=> 15,
				'type'		=> 'select',
				'options'	=> apply_filters( 'woocommerce_api_manager_url_expire_time', array_combine( range( 5, 60, 5 ), range( 5, 60, 5 ) ) ),
				'desc_tip'	=> __( 'Sets the time limit in minutes before a secure URL will expire. If a download begins before the expiration time limit is reached, the download will continue until complete.', 'woocommerce-api-manager' ),
			),

			array(
				'name'		=> __( 'Save to Dropbox App Key', 'woocommerce-api-manager' ),
				'desc'		=> sprintf( __( 'This creates a Save to Dropbox link in the My Account > My API Downloads section. Create an App Key %shere%s.', 'woocommerce-api-manager' ), '<a href="' . esc_url( 'https://www.dropbox.com/developers/apps/create' ) . '" target="blank">', '</a>' ),
				'tip'		=> '',
				'id'		=> $this->option_prefix . '_dropbox_dropins_saver',
				'css'		=> 'min-width:250px;',
				'default'	=> '',
				'type'		=> 'text',
				'desc_tip'	=> false,
			),

			array(
				'name'				=> __( 'Remove All Download Links', 'woocommerce-api-manager' ),
				'desc'				=> __( 'Do not display any download links in emails, Order Details, or the My Account dashboard (except for the My API Downloads).', 'woocommerce-api-manager' ),
				'id'				=> $this->option_prefix . '_remove_all_download_links',
				'default'			=> 'no',
				'type'				=> 'checkbox',
				'checkboxgroup'		=> 'start',
				'show_if_checked'	=> 'option',
				'desc_tip'			=> __( 'Emails contain a link to the My Account dashboard, where the My API Downloads section has a download link for each purchased product.', 'woocommerce-api-manager' ),
			),

			array( 'type'	=> 'sectionend', 'id' => $this->option_prefix . '_order_screen' ),

		));

	}

	/**
	 * Uses the WooCommerce options API to save settings via the @see woocommerce_update_options() function.
	 *
	 * @uses woocommerce_update_options()
	 * @uses $this->get_settings()
	 * @since 1.3
	 */
	public function update_api_manager_settings() {

		woocommerce_update_options( $this->get_settings() );
	}

	/**
	 * Updates and encrypts the Amazon S3 secret key
	 * @since 1.3.2
	 * @param  string $field unencrypted secret key
	 * @return string        encrypted secret key
	 */
	public function encrypt_secret_key( $field ) {

		if ( isset( $_POST[ $field['id'] ] ) ) {

			update_option( $field['id'], stripslashes( WCAM()->encrypt->encrypt( $_POST[ $field['id'] ] ) ) );

		}

	}

	/**
	 * Displays the Amazon S3 secret key
	 * @since 1.3.2
	 * @param  string $field encrypted secret key
	 * @return string        unencrypted secret key
	 */
	public function secret_key_field( $field ) {

		if ( isset( $field['id'] ) && isset( $field['name'] ) ) :

			$field_val = get_option( $field['id'] );
			?>
				<tr valign="top">
					<th scope="row" class="titledesc">
						<label for="<?php echo wp_kses_post( $field['id'] ); ?>"><?php echo esc_attr( $field['name'] ); ?></label>
					</th>
					<td class="forminp forminp-password">
						<input name="<?php echo esc_attr( $field['id'] ); ?>" id="<?php echo esc_attr( $field['id'] ); ?>" type="password" style="<?php echo esc_attr( isset( $field['css'] ) ? $field['css'] : '' ); ?>" value="<?php echo esc_attr( WCAM()->encrypt->decrypt( $field_val ) ); ?>" class="<?php echo esc_attr( isset( $field['class'] ) ? $field['class'] : '' ); ?>">
						<span class="description"><?php echo esc_attr( $field['desc'] ); ?></span>
					</td>
				</tr>
			<?php

		endif;

	}


} // end of class

WC_API_Manager_Admin_Settings::instance();
