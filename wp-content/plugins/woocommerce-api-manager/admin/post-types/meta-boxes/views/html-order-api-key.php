<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<div class="wc-metaboxes">
	<div class="wc-metabox closed">
		<h3 class="fixed">
			<button type="button" rel="<?php echo $order_data_key . ',' . $software_title . ',' . $data['license_email'] . ',' . $i . ',' . $data['user_id'] . ',' . $data['order_id']; ?>" class="am_delete_key button"><?php _e( 'Delete', 'woocommerce-api-manager' ); ?></button>
			<div class="handlediv" title="<?php _e( 'Click to toggle', 'woocommerce-api-manager' ); ?>"></div>
			<strong><?php printf( __( 'Software : %s | Activations: %s out of %s | API Access: %s | Version: %s | API Key: %s', 'woocommerce-api-manager' ), $software_title, $num_activations, $activations_limit, $data['_api_update_permission'], $data['current_version'], '<span class="am_tooltip" title="' . $order_data_key . '"> &hellip; ' . WCAM()->helpers->get_last_string_characters( $order_data_key, 4 ) . '</span>' ); ?></strong>
			<input type="hidden" name="user_id[<?php echo $i; ?>]" value="<?php echo $data['user_id']; ?>" />
		</h3>
		<table cellpadding="0" cellspacing="0" class="wc-metabox-content">
			<tbody>
				<tr>
					<td>
						<label><?php ( defined( 'WPLANG' == 'en_GB' ) && WPLANG == 'en_GB' ) ? _e( 'API Licence Key:', 'woocommerce-api-manager' ) : _e( 'API License Key:', 'woocommerce-api-manager' ); ?>:</label>
						<input type="text" class="short am_expand_text_box" name="api_key[<?php echo $i; ?>]" value="<?php echo $order_data_key; ?>" readonly />
					</td>
					<td>
						<label><?php _e( 'Activation Limit', 'woocommerce-api-manager' ); ?>:</label>
					<?php
					if ( $data['is_variable_product'] =='no' ) :
					?>
						<input type="text" class="short" name="_api_activations_parent[<?php echo $i; ?>]" value="<?php echo $data['_api_activations_parent'] ?>" placeholder="<?php _e( 'Unlimited', 'woocommerce-api-manager' ); ?>" />
					<?php
					elseif ( $data['is_variable_product'] == 'yes' ) :
					?>
						<input type="text" class="short" name="_api_activations[<?php echo $i; ?>]" value="<?php echo $data['_api_activations'] ?>" placeholder="<?php _e( 'Unlimited', 'woocommerce-api-manager' ); ?>" />
					<?php
					endif;
					?>
					</td>
					<td>
						<label><?php _e( 'API Access Permission', 'woocommerce-api-manager' ); ?>:</label>
						<input type="checkbox" class="am_checkbox" name="_api_update_permission[<?php echo $i; ?>]" value="yes" <?php checked( $data['_api_update_permission'], 'yes' ); ?> />
					</td>
				</tr>
				<tr>
					<td>
						<label><?php _e( 'Software Title', 'woocommerce-api-manager' ); ?>:</label>
					<?php
					if ( $data['is_variable_product'] =='no' ) :
					?>
						<input type="text" class="am_tooltip short am_expand_text_box" title="The Software Title should not be changed, because it must match the Software Title in the API form for the product, otherwise the API Manager will not work for this product on this customer order." name="_api_software_title_parent[<?php echo $i; ?>]" value="<?php echo $data['_api_software_title_parent']; ?>" placeholder="<?php _e( 'Required', 'woocommerce-api-manager' ); ?>" readonly />
					<?php
					elseif ( $data['is_variable_product'] == 'yes' ) :
					?>
						<input type="text" class="am_tooltip short am_expand_text_box" title="The Software Title should not be changed, because it must match the Software Title in the API form for the product, otherwise the API Manager will not work for this product on this customer order." name="_api_software_title_var[<?php echo $i; ?>]" value="<?php echo $data['_api_software_title_var']; ?>" placeholder="<?php _e( 'Required', 'woocommerce-api-manager' ); ?>" readonly />
					<?php
					endif;
					?>
					</td>
					<td>
						<label><?php ( defined( 'WPLANG' == 'en_GB' ) && WPLANG == 'en_GB' ) ? _e( 'API Licence Email:', 'woocommerce-api-manager' ) : _e( 'API License Email:', 'woocommerce-api-manager' ); ?>:</label>
						<input type="text" class="short" name="license_email[<?php echo $i; ?>]" value="<?php echo $data['license_email']; ?>" placeholder="<?php _e( 'Email Required', 'woocommerce-api-manager' ); ?>" />
					</td>
					<td>
						<label><?php _e( 'Software Version', 'woocommerce-api-manager' ); ?>:</label>
						<input type="text" class="short" name="current_version[<?php echo $i; ?>]" value="<?php echo $data['current_version']; ?>" readonly />
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
