<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * WooCommerce API Manager Tools
 *
 * Builds API Manager data for downloadable software products for new installations.
 * Will also rebuild API Manager data for existing installs, but it will not detect
 * new API Keys added manually to orders.
 *
 * @package API Manager/Admin/Tools
 * @author Todd Lahman LLC
 * @copyright   Copyright (c) Todd Lahman LLC
 * @since 1.3
 *
 */

class WC_API_Manager_Tools {

	/************************************************************
	*
	* Build/Rebuild API Manager Data
	*
	************************************************************/

	private $order_id;

	/**
	 * @var The single instance of the class
	 */
	protected static $_instance = null;

	/**
	 * @static
	 * @return class instance
	 */
	public static function instance() {

		if ( is_null( self::$_instance ) && ! is_object( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * Cloning is forbidden.
	 *
	 */
	private function __clone() {}

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 */
	private function __wakeup() {}

	public function rebuild_api_data() {
		global $wpdb;

		//Populates an array containing the ID of every user
		$id = $wpdb->get_col("
			SELECT ID
			FROM {$wpdb->prefix}users
		" );

		// Remove all wc_am_orders data so it can be rebuilt
		if ( ! empty( $id ) ) {

			foreach ( $id as $key => $user_id ) {

				delete_user_meta( $user_id, $wpdb->get_blog_prefix() . 'wc_am_orders' );

			}

			$order_items = $wpdb->get_results("
				SELECT 		order_item_id, order_item_name, order_item_type, order_id
				FROM 		{$wpdb->prefix}woocommerce_order_items
				ORDER BY 	order_item_id
			", ARRAY_A );

			if ( ! empty( $order_items ) ) {

				$loop = 0;

				foreach ( $order_items as $key => $order ) {

					/**
					 * $order contains numerically indexed arrays like this:
					 *
					 * Array
					 * (
					 *   [order_item_id] => 1
					 *   [order_item_name] => My Product
					 *   [order_item_type] => line_item
					 *   [order_id] => 4724
					 * )
					 *
					 */

					if ( count( $order ) > 0 ) {

						// Order ID
						$this->order_id = $order['order_id'];

						// order_id is the post_id
						$order_meta = get_post_custom( $this->order_id );

						if ( ! empty( $order_meta  ) )
							$user_id = $order_meta['_customer_user'][0];

						if ( ! empty( $user_id  ) )
							$user_meta = WCAM()->helpers->get_users_data( $user_id );

						if ( isset( $user_meta ) && ! empty( $user_meta ) ) {

							if ( WCAM()->array->array_search_multi( $user_meta, 'order_id', $this->order_id ) === true )
								continue; // skip duplicate order ids

						}

						$item_meta_product_id = $this->get_item_meta_product_id( $order['order_item_id'] );

						$item_meta_variation_id = $this->get_item_meta_variation_id( $order['order_item_id'] );

						$product_id = ( ! empty( $item_meta_variation_id ) ) ? $item_meta_variation_id : $item_meta_product_id;

						if ( empty( $product_id ) )
							continue; // skip this order if no download permission

						$product_meta = get_post_custom( $product_id );

						if ( empty( $product_meta ) )
							continue; // skip this order if no order meta data exists

						$software_title = ( ! empty( $product_meta['_api_software_title_var'][0] ) ) ? $product_meta['_api_software_title_var'][0] : $product_meta['_api_software_title_parent'][0];

						if ( empty( $software_title ) )
							continue; // skip this order if no order meta data exists

						$is_variable_product = ( ! empty( $product_meta['_api_software_title_var'][0] ) ) ? 'yes' : 'no';

						if ( empty( $is_variable_product ) )
							continue; // skip this order if no order meta data exists

						/**
						 * Delete any existing activations, since the old API Keys will no longer exist
						 */
						delete_user_meta( $user_id, $wpdb->get_blog_prefix() . WCAM()->helpers->user_meta_key_activations . $order_meta['_order_key'][0] );

						/**
						 * Rebuild new API Manager order data arrays
						 */
						$api_key = $order_meta['_order_key'][0] . '_am_' . WCAM()->helpers->generate_key( 12, false );

						$meta_data = WCAM()->helpers->get_user_order_array(
									empty( $api_key ) ? '' : sanitize_text_field( $api_key ),
									empty( $user_id ) ? '' : absint( $user_id ),
									empty( $this->order_id ) ? '' : absint( $this->order_id ),
									empty( $order_meta['_order_key'][0] ) ? '' : sanitize_text_field( $order_meta['_order_key'][0] ),
									empty( $order_meta['_billing_email'][0] ) ? '' : sanitize_email( $order_meta['_billing_email'][0] ),
									empty( $product_meta['_api_software_title_parent'][0] ) ? '' : sanitize_text_field( $product_meta['_api_software_title_parent'][0] ),
									empty( $product_meta['_api_software_title_var'][0] ) ? '' : sanitize_text_field( $product_meta['_api_software_title_var'][0] ),
									empty( $software_title ) ? '' : sanitize_text_field( $software_title ),
									empty( $product_meta['parent_product_id'][0] ) ? '' : absint( $product_meta['parent_product_id'][0] ),
									empty( $product_meta['variable_product_id'][0] ) ? '' : absint( $product_meta['variable_product_id'][0] ),
									empty( $product_meta['_api_new_version'][0] ) ? '' : sanitize_text_field( $product_meta['_api_new_version'][0] ),
									empty( $product_meta['_api_activations'][0] ) ? '' : absint( $product_meta['_api_activations'][0] ),
									empty( $product_meta['_api_activations_parent'][0] ) ? '' : absint( $product_meta['_api_activations_parent'][0] ),
									'yes',
									empty( $is_variable_product ) ? '' : sanitize_text_field( $is_variable_product ),
									empty( $license_type ) ? '' : sanitize_text_field( $license_type ),
									empty( $expires ) ? '' : sanitize_text_field( $expires ),
									empty( $order_meta['_completed_date'][0] ) ? '' : sanitize_text_field( $order_meta['_completed_date'][0] ),
									empty( $_api_was_activated ) ? '' : sanitize_text_field( $_api_was_activated ),
									empty( $api_key ) ? '' : sanitize_text_field( $api_key )
								);


						$this->save_meta_data( $meta_data, $api_key );

						/**
						 * Rebuild searchable API Keys
						 */
						$post_data = WCAM()->helpers->get_postmeta_data( $this->order_id );

						// Adds the API License Key to the post meta order, so it can be found in shop order search
						update_post_meta( $this->order_id, '_api_license_key_' . $loop, $api_key );

						$loop++;

					}

				} // end foreach $order_items

			} // end if $order_items

		} // end if $id

	}

	/**
	 * Saves API Manager order data
	 * @param  array $data    Order data
	 * @param  string $api_key API License Key used to access order data
	 * @return void
	 */
	private function save_meta_data( $data, $api_key ) {
		global $wpdb;

		$current_info = WCAM()->helpers->get_users_data( $data[$api_key]['user_id'] );

		if ( ! empty( $current_info ) ) {

			$new_info = WCAM()->array->array_merge_recursive_associative( $data, $current_info );

			update_user_meta( $data[$api_key]['user_id'], $wpdb->get_blog_prefix() . 'wc_am_orders', $new_info );

		} else {

			update_user_meta( $data[$api_key]['user_id'], $wpdb->get_blog_prefix() . 'wc_am_orders', $data );

		}

	}

	/**
	 * Get parent order item meta.
	 *
	 * @param mixed $item_id
	 * @return void
	 */
	private function get_item_meta_product_id( $order_item_id ) {
		global $wpdb;

		$item_meta = $wpdb->get_var( $wpdb->prepare( "
			SELECT 		meta_value
			FROM 		{$wpdb->prefix}woocommerce_order_itemmeta
			WHERE 		order_item_id = %d
			AND 		meta_key = '_product_id'
		", $order_item_id ) );

		return $item_meta;
	}

	/**
	 * Get variable order item meta.
	 *
	 * @param mixed $item_id
	 * @return void
	 */
	private function get_item_meta_variation_id( $order_item_id ) {
		global $wpdb;

		$item_meta = $wpdb->get_var( $wpdb->prepare( "
			SELECT 		meta_value
			FROM 		{$wpdb->prefix}woocommerce_order_itemmeta
			WHERE 		order_item_id = %d
			AND 		meta_key = '_variation_id'
		", $order_item_id ) );

		return $item_meta;
	}

	/************************************************************
	*
	* Rebuild woocommerce_downloadable_product_permissions table
	*
	************************************************************/

	private $dl_order_id;

	public function rebuild_downloadable_product_permissions() {
		global $wpdb;

		$order_items = $wpdb->get_results("
			SELECT 		order_item_id, order_item_name, order_item_type, order_id
			FROM 		{$wpdb->prefix}woocommerce_order_items
			ORDER BY 	order_item_id
		", ARRAY_A );

		// Emtpy the downloadable_product_permissions table before it is rebuilt
		if ( ! empty( $order_items ) ) {

			$this->empty_downloadable_product_permissions_table();

		}

		foreach ( $order_items as $key => $order ) {

			/**
			 * $order contains numerically indexed arrays like this:
			 *
			 * Array
			 * (
			 *   [order_item_id] => 1
			 *   [order_item_name] => My Product
			 *   [order_item_type] => line_item
			 *   [order_id] => 4724
			 * )
			 *
			 */

			// Build the new Downloadable Product Permissions data
			if ( count( $order ) > 0 ) {

				// Order ID
				$this->dl_order_id = $order['order_id'];

				$item_meta_product_id = $this->get_item_meta_product_id( $order['order_item_id'] );

				$item_meta_variation_id = $this->get_item_meta_variation_id( $order['order_item_id'] );

				$product_id = ( ! empty( $item_meta_variation_id ) ) ? $item_meta_variation_id : $item_meta_product_id;

				if ( ! empty( $product_id ) ) {

					$product_post = get_post_custom( $product_id );

					$product_post_meta = get_post_custom( $this->dl_order_id );

					$post = get_post( $this->dl_order_id );

					if ( ! empty( $post ) )
						$post_date = $post->post_date;

					// An order_key must exist
					if ( ! empty( $product_post_meta['_order_key'][0] ) ) {

						$file_download_paths = get_post_meta( $product_id, '_downloadable_files', true );

						// The order must have a file to download
						if ( ! empty( $file_download_paths ) ) foreach ( $file_download_paths as $download_id => $file_path ) {

							$this->woocommerce_downloadable_file_permission( $download_id, $product_id, $product_post, $product_post_meta, $post_date, $this->dl_order_id );

						}

					}

				}

			}

		}

	}

	/**
	 * Remove Downloadable Product Permission entry before rebuilding it
	 *
	 * @access public
	 * @param  int $order_id
	 * @return void
	 */
	private function empty_downloadable_product_permissions_table() {
		global $wpdb;

		$wpdb->query( "
			TRUNCATE {$wpdb->prefix}woocommerce_downloadable_product_permissions
		" );

	}

	/**
	 * Grant downloadable product access to the file identified by $download_id
	 *
	 * @access public
	 * @param string $download_id file identifier
	 * @param int $product_id product identifier
	 * @param array $product_post
	 * @param array $product_post_meta
	 * @param string $post_date
	 * @return void
	 */
	private function woocommerce_downloadable_file_permission( $download_id, $product_id, $product_post, $product_post_meta, $post_date, $order_id ) {
		global $wpdb;

		$user_email = $product_post_meta['_billing_email'][0];

	    $limit = empty( $product_post['_download_limit'][0] ) ? '' : absint( $product_post['_download_limit'][0] );

	    // Default value is NULL in the table schema
		$expiry = empty( $product_post['_download_expiry'][0] ) ? null : absint( $product_post['_download_expiry'][0] );

		if ( $expiry ) $expiry = date_i18n( "Y-m-d", strtotime( $post_date . ' + ' . $expiry . ' DAY' ) );

	    $data = array(
	    	'download_id'			=> $download_id,
			'product_id' 			=> $product_id,
			'user_id' 				=> $product_post_meta['_customer_user'][0],
			'user_email' 			=> $user_email,
			'order_id' 				=> $order_id,
			'order_key' 			=> $product_post_meta['_order_key'][0],
			'downloads_remaining' 	=> $limit,
			'access_granted'		=> $post_date,
			'download_count'		=> 0
	    );

	    $format = array(
	    	'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%d'
	    );

	    if ( ! is_null( $expiry ) ) {
	        $data['access_expires'] = $expiry;
	        $format[] = '%s';
	    }

		// Downloadable product - give access to the customer
		$result = $wpdb->insert( $wpdb->prefix . 'woocommerce_downloadable_product_permissions',
			$data,
			$format
		);

		return $result ? $wpdb->insert_id : false;

	}



} // End of class
