<?php while (have_posts()) : the_post(); ?>
<nav class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
	<div class="bc-contenedor">
		<?php if (function_exists('bcn_display')) : bcn_display(); endif; ?>
	</div>
</nav>
<?php the_content(); ?>
<?php wp_link_pages(array('before' => '<nav class="pagination">', 'after' => '</nav>')); ?>
<?php endwhile; ?>
