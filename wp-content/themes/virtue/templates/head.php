<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"  xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml" <?php language_attributes(); ?>   > <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"  xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml" <?php language_attributes(); ?>  > <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"  xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml" <?php language_attributes(); ?>  > <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"  xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml"  <?php language_attributes(); ?> > <!--<![endif]-->
  <head>
    <!-- some other thing -->

    <meta charset="utf-8">
    <?php global $virtue; ?>
    <link rel="icon" type="image/png" href="<?php bloginfo('template_directory'); ?>/favicon.png" />
    <title> <?php wp_title( '|', true, 'right' ); ?></title>
    <meta name="viewport" content="width=device-width, maximum-scale=1.0">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    
    <!-- Web Master Tools  -->
    <meta name="google-site-verification" content="VFkIbW4gBJsC7sigZKB8FU7RTb9iekTegtZEARiUV4k" />
    <!-- Web Master Tools  -->

    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <script type="text/javascript">
      document.domain = document.domain;
    </script>
    <script type="text/javascript" src="<?php echo site_url();?>/wp-includes/js/jquery/jquery.js?ver=1.11.1"></script>	
    <?php wp_head(); ?>


    <?php
    // validamos la url de la pagina actual y si contiene order-received
    // significa que es la pagina de agradecimiento por la compra
    // se agrega el script de Transacción de Ecommerce
    $uri = $_SERVER['REQUEST_URI'];
    $t = explode("/", $uri);
    if(in_array("order-received", $t)) :

    foreach ($t as $key => $value) {
      if(is_numeric($value)) {
        $order_id = $value;
      }
    }


    $order = new WC_Order($order_id);
    global $woocommerce;
    $items = $order->get_items();
    $array_items = array();
    foreach ($items as $key => $item) {
      $categoria = get_the_terms( $item["product_id"], 'product_cat' );
      $array_items[] = "{
                'sku': '".$item["product_id"]."',
                'name': '".$item["name"]."',
                'category': '".$categoria[0]->name."',
                'price': ".$item["line_subtotal"].",
                'quantity': ".$item["qty"]."
            }" ;
    }
    $string_items = implode(",", $array_items);
    $total_orden = $order->get_total();
    $shipping = count($items);

    ?>
    <script>
      dataLayer = [{
        'transactionId': <?php echo $order_id; ?>,
        'transactionAffiliation': 'Barman Club',
        'transactionTotal': <?php echo $total_orden; ?> ,
        'transactionTax': 0,
        'transactionShipping': <?php echo $shipping; ?>,
        'transactionProducts': [<?php echo $string_items; ?>]
      }];
    </script>
    <?php endif; ?>

  </head>
