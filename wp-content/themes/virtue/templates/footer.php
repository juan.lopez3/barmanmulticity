
<div><p class="texto_colaboradores"><b>Barman Club cuenta con el soporte de contenido de Publicaciones Semana, <br>
        experiencia de marcas de Pernod Ricard y respaldo de entrega y pedidos Teletrade S.A. <br>
        Tres grandes trabajando y construyendo a los mejores anfitriones</b></p></div>

<footer id="containerfooter" class="footerclass" role="contentinfo">
    <?php if (isset($_GET['authentication']) || isset($_GET['created']) || isset($_GET['sent'])) : ?>
        <div class="mesajes-wrap">
            <div class="overlay-mensajes cerrar-mensajes"></div>
            <div class="mensajes">
                <?php echo do_shortcode('[mensajes]'); ?>
                <button type="button" class="pull-right btn btn-danger cerrar-mensajes">cerrar</button>
            </div>
        </div>
    <?php endif; ?>
    <!-- pop up login / registro -->
    <div class="login-form">
        <?php
        if (!is_user_logged_in()) :
            try {
                echo do_shortcode('[clean-login]');
            } catch (Exception $e) {
                print $e->getMessage();
                die;
            }
        endif;
        ?>
    </div>

    <div class="container">
        <div class="row">
            <div class="container">
                <?php
                global $virtue;
                if (isset($virtue['footer_layout'])) {
                    $footer_layout = $virtue['footer_layout'];
                } else {
                    $footer_layout = 'fourc';
                }
                if ($footer_layout == "fourc") {
                    if (is_active_sidebar('footer_1')) {
                        ?>
                        <div class="col-md-3 col-sm-6 footercol1">
                            <?php dynamic_sidebar('footer_1'); ?>
                        </div>
                    <?php }; ?>
                    <?php if (is_active_sidebar('footer_2')) { ?> 
                        <div class="col-md-3  col-sm-6 footercol2">
                            <?php dynamic_sidebar('footer_2'); ?>
                        </div>
                    <?php }; ?>
                    <?php if (is_active_sidebar('footer_3')) { ?>
                        <div class="col-md-3 col-sm-6 footercol3">
                            <?php dynamic_sidebar('footer_3'); ?>
                        </div>
                    <?php }; ?>
                    <?php if (is_active_sidebar('footer_4')) { ?> 
                        <div class="col-md-3 col-sm-6 footercol4">
                            <?php dynamic_sidebar('footer_4'); ?>
                        </div>
                    <?php }; ?>
                    <?php
                } else if ($footer_layout == "threec") {
                    if (is_active_sidebar('footer_third_1')) {
                        ?>
                        <div class="col-md-4 footercol1">
                            <?php dynamic_sidebar('footer_third_1'); ?>
                        </div>
                    <?php }; ?>
                    <?php if (is_active_sidebar('footer_third_2')) { ?> 
                        <div class="col-md-4 footercol2">
                            <?php dynamic_sidebar('footer_third_2'); ?>
                        </div>
                    <?php }; ?>
                    <?php if (is_active_sidebar('footer_third_3')) { ?>
                        <div class="col-md-4 footercol3">
                            <?php dynamic_sidebar('footer_third_3'); ?>
                        </div>
                    <?php }; ?>
                    <?php
                } else {
                    if (is_active_sidebar('footer_double_1')) {
                        ?>
                        <div class="col-md-10 col-sm-10">
                            <?php dynamic_sidebar('footer_double_1'); ?> 
                        </div>
                    <?php }; ?>
                    <?php if (is_active_sidebar('footer_double_2')) { ?>
                        <div class="col-md-2 col-sm-2">
                            <?php dynamic_sidebar('footer_double_2'); ?> 
                        </div>
                    <?php }; ?>
                <?php } ?>
            </div><!-- /. container -->
        </div><!-- /. row -->

        <div class="footercredits clearfix">
            <div class="container">
                <?php if (has_nav_menu('footer_navigation')) : ?>
                    <div class="footernav clearfix">
                        <?php wp_nav_menu(array('theme_location' => 'footer_navigation', 'menu_class' => 'footermenu clearfix')); ?>
                    </div>
                <?php endif; ?>
                <p>
                    <?php
                    if (isset($virtue['footer_text'])) :
                        $footertext = $virtue['footer_text'];
                    else :
                        $footertext = '[copyright] [the-year] [site-name] [theme-credit]';
                    endif;
                    echo do_shortcode($footertext);
                    ?>
                    <a style="color: #aaa; font-weight: 700;" href="http://www.barmanclub.co/politica_de_tratamiento_de_datos_personales.pdf" target="_blank">Política de protección de datos</a>
                </p>
            </div>
        </div><!-- footercredits -->

    </div>
    <div class="postfooter"></div>
</footer>
<script type="text/javascript" src="<?php echo bloginfo('template_directory'); ?>/assets/js/iframeWidth.js"></script>
<script type="text/javascript" src="<?php echo bloginfo('template_directory'); ?>/assets/js/videoYoutube.js"></script>

<!-- Tag Manager -->
<!--
<noscript>
        <iframe src='//dmp.pernod-ricard.com/JScript/iframe.php?container=TEROCwEHTEhdU0Rc' height='0' width='0' style='display:none;visibility:hidden'>
        </iframe>
</noscript>
<script>(function(d, s,id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = '//dmp.pernod-ricard.com/JScript/pernod-ricard-api.js?container=TEROCwEHTEhdU0Rc'; fjs.parentNode.insertBefore(js, fjs.nextSibling); }(document, 'script','pdrd_'+new Date().getTime()));</script>
-->
<!-- End Tag Manager -->
<?php wp_footer(); ?>
