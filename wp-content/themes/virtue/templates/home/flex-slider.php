<div class="sliderclass">
  <?php  global $virtue; 
        if(isset($virtue['slider_size'])) {$slideheight = $virtue['slider_size'];} else { $slideheight = 400; }
        if(isset($virtue['slider_size_width'])) {$slidewidth = $virtue['slider_size_width'];} else { $slidewidth = 1140; }
        if(isset($virtue['slider_captions'])) { $captions = $virtue['slider_captions']; } else {$captions = '';}
        if(isset($virtue['home_slider'])) {$slides = $virtue['home_slider']; } else {$slides = '';}
?>
<?php $args = array(
    'numberposts' => 4,
    'offset' => 0,
    'category' => 88,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => 'post',
    'post_status' => 'publish',
    'suppress_filters' => false );

    $recent_posts = wp_get_recent_posts( $args, OBJECT ); 
    ?>

  <div id="imageslider" class="container">
    <div class="flexslider loading" style="max-width:<?php echo $slidewidth;?>px; margin-left: auto; margin-right:auto;">
        <ul class="slides">
            <?php 
            $sing=0;
            foreach ($recent_posts as $pot): {

                    $connected = new WP_Query( array(
                      'connected_type' => 'products_to_posts',
                      'connected_items' => $pot,
                      'nopaging' => true,
                    ) );
                    
                    $pr=$connected->posts[0]; 
                    } 
                    ?>
                      <li> 
                        <div class="producto-head">
                          <header class="textos-head">
                            <div class="title-post"><?php echo $pot->post_title; ?></div>
                            <div class="slide-go"><a href="<?php echo $pot->guid; ?>">Continuar</a></div>

                            <div class="grupo-comprar">
                              <div class="title-product">
                                Mejor con <span><?php echo $pr->post_title ?></span>
                                <div class="comprar">
                                  <a href="<?php echo get_permalink($pr->ID); ?>" data-jckqvpid="<?php echo $pr->ID; ?>">Comprar</a>
                                </div>
                              </div>
                            </div>
                          </header>
                          <div class="imagen-producto"><?php echo get_the_post_thumbnail( $pr->ID, 'full' ); ?></div>
                        </div>
                      </li>
            <?php 
            $sing++;
            endforeach; ?>
        </ul>
      </div> <!--Flex Slides-->
  </div><!--Container-->
</div><!--sliderclass-->

      <?php 
          if(isset($virtue['trans_type'])) {$transtype = $virtue['trans_type'];} else { $transtype = 'slide';}
          if(isset($virtue['slider_transtime'])) {$transtime = $virtue['slider_transtime'];} else {$transtime = '300';}
          if(isset($virtue['slider_autoplay'])) {$autoplay = $virtue['slider_autoplay'];} else {$autoplay = 'true';}
          if(isset($virtue['slider_pausetime'])) {$pausetime = $virtue['slider_pausetime'];} else {$pausetime = '7000';}
      ?>
      <script type="text/javascript">
            jQuery(window).load(function () {
                jQuery('.flexslider').flexslider({
                    animation: "<?php echo $transtype ?>",
                    animationSpeed: <?php echo $transtime ?>,
                    slideshow: <?php echo $autoplay ?>,
                    slideshowSpeed: <?php echo $pausetime ?>,
                    smoothHeight: true,
                    controlNav: false,
                    before: function(slider) {
                      slider.removeClass('loading');
                    }  
                  });
                });
      </script>