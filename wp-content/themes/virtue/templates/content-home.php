<?php
$image = get_the_post_thumbnail(get_the_id(), 'home-post');

//traemos las categorias
$categoria = get_the_category();

//consulta el producto relacionado (compra)
//$pot = get_post( get_the_id(), OBJECT );

$url_corta = wp_get_shortlink();
?>


<!-- Contenido columna izquierda -->
<div class="col-sm-8 landing-left-panel">

	<div class="cat-title">
		<!-- Categoria del post -->
		<h3><?php echo $categoria[0]->cat_name; ?></h3>
	</div>

	<section class="contenido">

		<!-- thumbnail del post -->
		<?php if($image) : ?>
		<div class="post-single-img">
			<a class="image" href="<?php the_permalink(); ?>">
				<?php echo $image ?>
			</a>
		</div>
		<?php endif ?>

		<!-- el titulo del post -->
		<h2 class="title">
			<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
		</h2>
		<!-- El resumen del post -->
		<div class="excerpt resumen-post"><?php the_excerpt(); ?></div>

		<!-- footer: social share, imagen producto, producto -->
		<div class="att">

			<div class="sharebe">
				<p>Compartir</p>
				<?php

				$title = str_replace("#", "",  get_the_title());

				?>

				<!-- Comparte especificamente la url del post en las redes, y no la url del home-->
				<!-- Simple Share Buttons Adder (6.1.0) simplesharebuttons.com -->
				<div class="ssba ssba-wrap">
					<div style="text-align:left">
						<a data-site="" class="ssba_twitter_share" href="http://twitter.com/intent/tweet?url=<?php print $url_corta ?>&via=BarmanClub&text=<?php print $title; ?>" target="_blank">
							<img src="<?php echo get_site_url(); ?>/wp-content/plugins/simple-share-buttons-adder/buttons/somacro/twitter.png" title="Twitter" class="ssba ssba-img" alt="Tweet about this on Twitter"></a>
						<a data-site="" class="ssba_facebook_share" href="http://www.facebook.com/sharer.php?u=<?php  the_permalink(); ?>" target="_blank">
							<img src="<?php echo get_site_url(); ?>/wp-content/plugins/simple-share-buttons-adder/buttons/somacro/facebook.png" title="Facebook" class="ssba ssba-img" alt="Share on Facebook"></a>
					</div>
				</div>

			</div>
			<?php

			// Display connected pages
			//imprime solo uno de los productos relacionados
			$contador = 0;
			if (is_array( $post->products)) {

				foreach ( $post->products as $post ) : setup_postdata( $post );
				if ($contador++ >= 1) {
					break;
				}
			?>
			<div class="imagen_product_post">
				<?php  print get_the_post_thumbnail( get_the_id(), 'shop_thumbnail'); ?>
			</div>

			<div class="attached-product">
				<div class="preparalo">
					<p>Mejor con</p>
					<div class="titulo_producto"><?php echo the_title(); ?></div>
				</div>
				<div class="comprar">
					<a href="<?php the_permalink(); ?>"
        data-interaccion-id="<?php echo HorusConf::get_var('interaccion_agregado_al_carrito_id') ?>"  
        data-interaccion-agregado_desde="articulo del home"
        data-interaccion-productid="<?php echo $post->ID ?>"                                             
                                           
                                        >
						Comprar
					</a>
				</div>
			</div>

			<?php

				endforeach;
			}
			wp_reset_postdata();// set $post back to original post ?>

		</div>


	</section>

</div><!-- /Contenido columna izquierda -->

<?php if (isset($entrada_index) && $entrada_index == 2) { ?>
<!-- Favotiros para los usuarios logueados -->
<!-- right content, formulario -->
<div class="col-sm-4 landing-right-panel">
	<?php if ( is_user_logged_in() ) { ?>
	<div class="cat-title"><h2>Favoritos</h2></div>


	<?php
	// muestra post marcados como favoritos por el usuario actual
	$favoritos = wpfp_get_users_favorites();
	if(!empty($favoritos)){
		foreach ($favoritos as $key => $favorito) :

		//$tit = do_shortcode($favorito);
		$post = get_post( $favorito, OBJECT );
		$enlace_post = get_permalink($post->ID);
	?>

	<p><a href="<?php echo $enlace_post; ?>"><?php echo ($post->post_title); ?></a></p>

	<?php  endforeach;
	}

} else { ?>
	<div class="cat-title"><h2>Registro</h2></div>

	<?php } ?>

	<div>  
		<?php echo do_shortcode('[clean-login-register]'); ?>
	</div>
</div>

<?php } else { ?>
<!-- right content, relacionados -->
<!-- Los post relaciones (a la derecha) -->

<div class="col-sm-4 landing-right-panel">
	<div class="cat-title">
		<h2>relacionados</h2>
	</div>

	<?php
	//contenido relacionado real

	$contador = 0;

	if (is_array( $post->relateds) && !empty($post->relateds)) {

		print "<div class='display-posts-listing'>";
		foreach ( $post->relateds as $post ) : setup_postdata( $post );
		if ($contador++ >= 2) {
			break;
		}
	?>

	<div class="listing-item">

		<a href="<?php the_permalink() ?>">
			<?php print get_the_post_thumbnail(get_the_id(), 'minipost'); ?>
		</a>
		<a class="title" href="<?php the_permalink() ?>">
			<?php the_title(); ?>
		</a>
	</div>



	<?php

		endforeach;
		print "</div>";
		wp_reset_postdata(); // soliuset $post back to original post
	}else{
		echo do_shortcode('[display-posts orderby="date"   posts_per_page="2" related="true" include_excerpt="false" image_size="minipost" wrapper="div"]');
	}

	?>
</div>
<?php } ?>

