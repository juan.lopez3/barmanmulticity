<?php global $virtue; ?>
<div id="content" class="container homepagecontent" onclick="location.href='https://barmanclub.co/';">
    <div class="row">

        <div class="main col-sm-12 <?php echo kadence_main_class(); ?>" role="main">
            <div class="homecontent clearfix home-margin">


                <?php
                $the_banner = new WP_Query('post_type=banner_home');

                if ($the_banner->have_posts()) {
                ?>
                <?php
                    while ($the_banner->have_posts()) :
                    $the_banner->the_post();
                ?>
                <a class="banner-top-tienda"
					data-interaccion-id="<?php echo HorusConf::get_var('interaccion_banner_home_id') ?>"
					data-interaccion-agregado_desde="Accion Banner superior home" href="<?php echo get('imagenes_banner_home_enlace'); ?>">
                    <picture>
                        <source srcset="<?php print get_image('imagenes_banner_home_imagen_desktop',1,1,0); ?>" media="(min-width: 1200px)">
                        <source srcset="<?php echo get_image('imagenes_banner_home_imagen_movil',1,1,0); ?>" media="(max-width: 767px)">
                        <img srcset="<?php print get_image('imagenes_banner_home_imagen_desktop',1,1,0); ?>" alt="Barman club">
                    </picture>
                </a>
                <?php endwhile; ?>
                <?php } ?>



                <?php
                $args = array(
                    'posts_per_page' => 5,
                    'orderby' => 'post_date',
                    'category__not_in' => '-26',
                    'cache_results' => false,
                    'no_found_rows' => true,
                    'update_post_term_cache' => false,
                );
                query_posts($args);

                //productos relacionados
                p2p_type('products_to_posts')->each_connected($wp_query, array(), 'products');
                p2p_type('posts_to_posts')->each_connected($wp_query, array(), 'relateds');





                while (have_posts()) : the_post();
                $entrada_index = $wp_query->current_post + 1; //the loop
                ?>
                <div class="row">

                    <?php
                    set_query_var('entrada_index', $entrada_index);
                    //archivo content-home.php
                    get_template_part('templates/content', 'home');
                    ?>


                </div>
                <?php endwhile; ?>
            </div><!-- /home content -->
        </div><!-- /main -->

        <!-- videos -->
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="cat-title">
                        <h2>VIDEO</h2>
                    </div>
                </div>

                <!-- loop de videos a mostar en el home -->
                <?php
                query_posts('cat=26');
                while (have_posts()) : the_post();

                $entrada_index = $wp_query->current_post + 1; //the loop
                ?>
                <div class="col-sm-6">
                    <div class="grid-video">
                        <div class="el-titulo">
                            <a href="<?php the_permalink(); ?>">
                                <?php the_title(); ?>
                            </a>
                        </div>

                        <div class="contenedor_video" id="contenedor_<?php echo get("video_youtube_id"); ?>" data-id-yt="<?php echo get("video_youtube_id"); ?>">
                            <div class="imagen">
                                <?php
                                set_query_var('video_youtube', $entrada_index);
                                get_template_part('templates/yt', 'imagen');
                                ?>
                            </div>
                            <div class="video" id="video_<?php echo get("video_youtube_id"); ?>">

                            </div>

                        </div>


                    </div>
                </div>
                <?php endwhile; ?>
            </div><!-- /row -->
        </div><!-- /container -->

    </div>
</div>

