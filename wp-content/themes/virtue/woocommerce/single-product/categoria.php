<?php
/**
 * Single Product Category
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $post, $product;

?>
<div class="product_category_title">

	<?php echo $product->get_categories(); ?>

</div>
