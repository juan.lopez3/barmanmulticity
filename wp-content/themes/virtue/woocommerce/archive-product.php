<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly
?>

<div id="content" class="container">
    <div class="row">
        <div class="main <?php echo kadence_main_class(); ?>" role="main">

            <?php do_action('woocommerce_archive_description'); ?>

            <?php
            if (have_posts()) :
                /**
                 * Modal: distribucion de productos
                 */
                woocommerce_get_template_part('home-tienda/modal');
                /**
                 * Banner tienda
                 */
                woocommerce_get_template_part('home-tienda/banner');
                /**
                 * Tienda mensajes
                 */
                do_action('woocommerce_before_my_page');
                ?>

                <!-- breadcrumbs -->
                <div class="breadcrumbs">
                    <?php woocommerce_breadcrumb(); ?>
                </div>

                <!-- navegacion -->
                <?php woocommerce_get_template_part('home-tienda/navegacion', 'categorias'); ?>

                <?php woocommerce_product_loop_start(); ?>

                <?php
                //si la pagina es el home de la tienda
                if (is_post_type_archive()) :
                    ?>
                    <div class="col-xs-12 col-sm-12 lo-ultimo">
                        <div>
			<?php woocommerce_product_loop_start(); ?>

				<?php woocommerce_product_subcategories(); ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php wc_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; // end of the loop. ?>

			<?php woocommerce_product_loop_end(); ?>
			



                        </div>                        
                    </div>
        <?php

    endif;//cierra if (is post type archive )
    woocommerce_product_loop_end();
    ?>

                <?php
                /**
                 * woocommerce_after_shop_loop hook
                 *
                 * @hooked woocommerce_pagination - 10
                 */
                // do_action('woocommerce_after_shop_loop');
                ?>

            <?php elseif (!woocommerce_product_subcategories(array('before' => woocommerce_product_loop_start(false), 'after' => woocommerce_product_loop_end(false)))) : ?>

                <?php woocommerce_get_template('loop/no-products-found.php'); ?>

            <?php endif; ?>
        </div>
    </div>
</div>

                            <?php
                            /** ANTIGUA MANERA DE ORDENAR: ahora los ordenamos manualmente con wordpress
                             * 
                             *  Agregamos ordenamiento primero por categoria de producto (whisky, vodka)
                             *  y luego por empresa, la idea es que pernod sea la primera empresa siempre
                             */
                             
                             /*
                            $categorias = get_terms(array('taxonomy' => 'product_cat', 'hide_empty' => 0));
                            $empresas = get_terms(array('taxonomy' => 'empresa', 'hide_empty' => 0, 'orderby' => 'slug', 'order' => 'DESC'));
                           
                            //Por si no tenemos la taxonomia creada
                            if (gettype($empresas) == "object") { //WP_ERROR
                                $empresas = array();
                            }

                            //Agregamos otra empresa para los productos que no tienen empresa
                            $sinempresa = new stdClass();
                            $sinempresa->slug = null;
                            $sinempresa->term_id = null;
                            $empresas[] = $sinempresa;


                            foreach ($categorias as $categoria) {

                                foreach ($empresas as $empresa) {

                                    $args = array_merge($wp_query->query_vars, array(
                                        'posts_per_page' => 1000, // Un limite por si alcaso
                                        "empresa" => $empresa->slug,
                                        'tax_query' => array(
                                            array(
                                                'taxonomy' => 'product_cat',
                                                'field' => 'term_id',
                                                'terms' => $categoria->term_id
                                            ),
                                        )
                                    ));
                                    $my_query = new WP_Query($args);

                                    while ($my_query->have_posts()) : $my_query->the_post();

                                        woocommerce_get_template_part('content', 'product');

                                    endwhile;
                                }//end empresas
                            }//end categorias
                            
                            */
                            ?>


