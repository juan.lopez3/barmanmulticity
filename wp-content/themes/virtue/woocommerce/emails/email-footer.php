<?php
/**
 * Email Footer
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates/Emails
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Load colours
$base = get_option( 'woocommerce_email_base_color' );

$base_lighter_40 = wc_hex_lighter( $base, 40 );

// For gmail compatibility, including CSS styles in head/body are stripped out therefore styles need to be inline. These variables contain rules which are added to the template inline.
$template_footer = "
    border-top:0;
    -webkit-border-radius:6px;
";

$credit = "
    border:0;
    color: $base_lighter_40;
    font-family: Arial;
    font-size:12px;
    line-height:125%;
    text-align:center;
";
?>

<table width="650" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td style="padding:80px 0px 0px 0px;">
        <img src="http://www.barmanclub.co/wp-content/uploads/2015/02/barman_footerTexture.gif" width="650" height="16" alt="" style="display:block;"/>
      </td>
    </tr>
    <tr>
      <td style="padding:40px 38px 40px 38px;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tbody>
            <tr>
              <td width="10%" style="padding:0px 0px 0px 39px;"><img src="http://www.barmanclub.co/wp-content/uploads/2015/02/barman_footerLEFTarrow.gif" width="17" height="17" alt="" style="display:block;"/></td>
              <td width="80%" style="text-align:center; font-family:Arial, Helvetica, sans-serif; color:#4e4e4e; font-size:bold;">Pidelo en línea, págalo en Casa <a href="http://barmanclub.co" target="_blank" style="color:#d10018; text-decoration:none;">barmanclub.co</a></td>
              <td width="10%" style="padding:0px 39px 0px 0px;"><img src="http://www.barmanclub.co/wp-content/uploads/2015/02/barman_footerRIGHTarrow.gif" width="17" height="17" alt="" style="display:block;"/></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td style="background:#595959; padding:16px 38px 12px 38px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr valign="top">
            <td style="font-family:Arial, Helvetica, sans-serif; padding:10px 0px 0px 0px; color:#ffffff; font-size:14px; text-align:left;">Síguenos en:</td>
            <td ><a href="https://www.facebook.com/barmanclubco" target="_blank">
              <img src="<?php echo(get_template_directory_uri()); ?>/assets/img/instagram-blanco.png" />
            </a></td>
            <td ><a href="https://www.facebook.com/barmanclubco" target="_blank">
              <img src="<?php echo(get_template_directory_uri()); ?>/assets/img/twitter-blanco.png" />
            </a></td>
            <td ><a href="https://www.facebook.com/barmanclubco" target="_blank">
              <img src="<?php echo(get_template_directory_uri()); ?>/assets/img/facebook-blanco.png" />
            </a></td>
            <td width="55%"></td>
            <td ><a href="http://barmanclub.co" target="_blank"><img src="http://www.barmanclub.co/wp-content/uploads/2015/02/barman_footerLogo.gif" width="41" height="56" alt="Barman" style="display:block;"/></a></td>
          </tr>
        </tbody>
      </table></td>
    </tr>
    <tr>
      <td style="background:#343434; padding:0px 0px 0px 0px;"><img src="http://www.barmanclub.co/wp-content/uploads/2015/02/spacer.gif" width="1" height="26" alt=""/></td>
    </tr>

</table>

</body>
</html>
