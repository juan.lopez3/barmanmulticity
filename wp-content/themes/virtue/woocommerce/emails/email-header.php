<?php
/**
 * Email Header
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.0.0
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly


// Load colours
$bg = get_option('woocommerce_email_background_color');
$body = get_option('woocommerce_email_body_background_color');
$base = get_option('woocommerce_email_base_color');
$base_text = wc_light_or_dark($base, '#202020', '#ffffff');
$text = get_option('woocommerce_email_text_color');

$bg_darker_10 = wc_hex_darker($bg, 10);
$base_lighter_20 = wc_hex_lighter($base, 20);
$text_lighter_20 = wc_hex_lighter($text, 20);

// For gmail compatibility, including CSS styles in head/body are stripped out therefore styles need to be inline. These variables contain rules which are added to the template inline. !important; is a gmail hack to prevent styles being stripped if it doesn't like something.
$wrapper = "
	background-color: " . esc_attr($bg) . ";
	width:100%;
	-webkit-text-size-adjust:none !important;
	margin:0;
	padding: 0;
";
$template_container = "
	box-shadow:0 0 0 3px rgba(0,0,0,0.025) !important;
	border-radius:6px !important;
	background-color: " . esc_attr($body) . ";
	border: 1px solid $bg_darker_10;
	border-radius:6px !important;
";
$template_header = "
	background-color: " . esc_attr($base) . ";
	color: $base_text;
	border-top-left-radius:6px !important;
	border-top-right-radius:6px !important;
	border-bottom: 0;
	font-family:Arial;
	font-weight:bold;
	line-height:100%;
	vertical-align:middle;
";
$body_content = "
	background-color: " . esc_attr($body) . ";
	border-radius:6px !important;
";
$body_content_inner = "
	color: $text_lighter_20;
	font-family:Arial;
	font-size:14px;
	line-height:150%;
	text-align:left;
";
$header_content_h1 = "
	color: " . esc_attr($base_text) . ";
	margin:0;
	padding: 28px 24px;
	text-shadow: 0 1px 0 $base_lighter_20;
	display:block;
	font-family:Arial;
	font-size:30px;
	font-weight:bold;
	text-align:left;
	line-height: 150%;
";

$titulo = "El pedido se ha completado";
$e = explode("/",$_SERVER['REQUEST_URI']);
if(in_array("lost-password", $e) || in_array("my-account", $e) || in_array("registro", $e)  || in_array("restaurar-contrasena", $e)) {
    $titulo = "Barman Club";
}


?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title><?php echo get_bloginfo('name'); ?></title>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
        <div style="<?php echo $wrapper; ?>">
            <table width="650" border="0" cellspacing="0" cellpadding="0" align="center" style="background:#ffffff;">
                <tbody>
                    <tr>
                        <td style="background:#f3f3f3; border-bottom-color:#e4e4e4; border-bottom-style:solid; border-bottom-width:1px; padding:0px 0px 8px 0px;"><table width="650" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr valign="top">
                                        <td width="538"><img src="http://www.barmanclub.co/wp-content/uploads/2015/02/barman_topBar.gif" width="538" height="50" alt="" style="display:block;"/></td>
                                        <td width="112" rowspan="2"><a href="http://barmanclub.co" target="_blank"><img src="http://www.barmanclub.co/wp-content/uploads/2015/02/barman_topLogo.gif" width="112" height="125" alt="Barman" style="display:block;"/></a></td>
                                    </tr>
                                    <tr>
                                        <td style="font-family:Arial, Helvetica, sans-serif; color:#4e4e4e; font-weight:bold; font-size:27px; padding:0px 0px 0px 38px;"><?php echo $titulo; ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
            </table>
