<?php
/**
 * Customer Reset Password email
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.0.0
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly
?>

<?php do_action( 'woocommerce_email_header', $email_heading );  ?>

<table width="650" border="0" cellspacing="0" cellpadding="0" align="center" style="background:#ffffff;">

    <tbody>
        <tr>
            <td style="font-family:arial, helvetica, sans-serif; padding:55px 38px 60px 38px; color:#959595; font-size:15px;">
                <p><?php _e('Someone requested that the password be reset for the following account:', 'woocommerce'); ?></p>
                <p><?php printf(__('Username: %s', 'woocommerce'), $user_login); ?></p>
                <p><?php _e('If this was a mistake, just ignore this email and nothing will happen.', 'woocommerce'); ?></p>
                <p><?php _e('To reset your password, visit the following address:', 'woocommerce'); ?></p>
                <p>
                    <a href="<?php echo esc_url(add_query_arg(array('key' => $reset_key, 'login' => rawurlencode($user_login)), wc_get_endpoint_url('lost-password', '', get_permalink(wc_get_page_id('myaccount'))))); ?>"><?php _e('Click here to reset your password', 'woocommerce'); ?></a>
                </p>
            </td>

    </tbody>
    <tfoot>
    </tfoot>
</table>

<?php do_action('woocommerce_email_footer'); ?>
