<?php
/**
 * Restore password email
 *
 * @author WooThemes
 * @package WooCommerce/Templates/Emails/HTML
 * @version 2.0.0
 */
?>

<?php echo wc_get_template('emails/email-header.php', array('email_heading' => $email_heading)); ?>

<!--<table cellspacing="0" cellpadding="6" style="width: 100%; border: 1px solid #eee;" border="1" bordercolor="#eee">-->
<table width="650" border="0" cellspacing="0" cellpadding="6" align="center" s>
    <thead>
    </thead>
    <tbody>
        <tr><td style="
                padding: 40px;
                padding-bottom: 0;
                ">Utilice el siguiente link para restablecer su contraseña: 
                <a href=" <?php echo $url_msg; ?> ">Link de activación</a> 
                <!--<br/><br/> <?php //echo $blog_title;   ?> <br/>-->

            </td></tr>
    </tbody>
    <tfoot>
    </tfoot>
</table>

<?php wc_get_template('emails/email-footer.php'); ?>