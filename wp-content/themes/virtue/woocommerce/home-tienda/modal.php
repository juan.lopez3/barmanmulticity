<?php
//verificamos si el modal se cerro
$check_alert = false;
//si la pagina es el home de la tienda
if ( is_post_type_archive() && $check_alert === false ) :
?>
<!-- modal -->
<div class="alert alert-warning alert-dismissible fade in" role="alert">
    <button type="button" class="close" data-dismiss="alert">
        <span aria-hidden="true">&times;</span>
        <span class="sr-only">Cerrar</span>
    </button>
    <strong>
        Por el momento distribución de productos únicamente en Bogotá, Cali y Medellín.
        <!-- <span class="condiciones-entrega">Ver condiciones de entrega</span> -->
    </strong>
</div>
<div class="popup-condiciones-entrega">
    <div class="cerrar-condiciones">x</div>
    <div class="condiciones-entrega-content">
        <?php get_template_part('condiciones'); ?>
    </div>
</div>
<!-- /.modal -->
<?php endif; ?> <!-- /. if home tienda -->
