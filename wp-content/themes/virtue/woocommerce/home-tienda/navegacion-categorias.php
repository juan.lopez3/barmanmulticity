<nav class="row wrap-cat-product filtro-categorias">
    <!-- todas las categorias -->
    <div class="categoria-producto all-products cat-current" data-categoria="todas">
        <div class="nombre-categoria">TODOS</div>
    </div>
    <?php
    //conseguir categoria actual
    global $wp_query;
    $cat_args = array(
        'taxonomy'		=> 'product_cat',
        'parent'      	=> 0,
    );

    $categorias = get_categories( $cat_args );

    foreach ($categorias as $categoria) :
    $nombre_categoria = $categoria->name;
    $slug_categoria = $categoria->slug;
    $id_categoria = $categoria->term_id;
    $imagen_id = get_woocommerce_term_meta( $categoria->term_id, 'thumbnail_id', true );
    $imagen = wp_get_attachment_url( $imagen_id );
    $clases = ' cat-current';
    ?>

    <div class="categoria-producto <?php echo $nombre_categoria; ?>" data-categoria="<?php echo $nombre_categoria; ?>">
        <div class="nombre-categoria"><?php echo $nombre_categoria; ?></div>
    </div>

    <?php endforeach; ?><!-- /. cierra foreach -->
</nav><!-- /. row categorias -->
