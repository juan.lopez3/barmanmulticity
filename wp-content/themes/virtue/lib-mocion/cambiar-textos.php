<?php
add_filter( 'woocommerce_order_button_text', 'woo_custom_order_button_text' );

function woo_custom_order_button_text() {
	return __( 'REALIZAR PEDIDO', 'woocommerce' );
}

/**
 * Change text strings
 *
 * @link http://codex.wordpress.org/Plugin_API/Filter_Reference/gettext
 */
function my_text_strings( $translated_text, $text, $domain ) {

	switch ( $translated_text ) {
		case 'View Cart' :
		$translated_text = __( 'comprar', 'woocommerce' );
		break;
		case 'Customer Details' :
		$translated_text = 'Detalles del cliente';
		break;     
		case 'Cart Subtotal':
		$translated_text = 'Sub total';
		break; 
		case 'Order Total' :
		$translated_text = 'Total';
		break;          
		case 'Product' :
		$translated_text = 'Producto';
		break;    
		case 'Quantity' :
		$translated_text = 'Cantidad';
		break;   
		case 'Coupon has been removed.' :
		$translated_text = 'El cupón ha sido eliminado';
		break;
                case 'Your details' :
		$translated_text = 'Tus datos';
		break;
	}
	return $translated_text;
}

add_filter( 'gettext', 'my_text_strings', 20, 3 );
add_filter('gettext',  'translate_text'); 
add_filter('ngettext', 'translate_text');

function translate_text($translated) { 

$translated = str_ireplace('ORDER NUMBER:', 'Número de orden', $translated); 
	
return $translated; 
}

