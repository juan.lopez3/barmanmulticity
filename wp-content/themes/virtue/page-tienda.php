<?php
/*
Template Name: tienda
*/
?>

<div id="content" class="container">
    <div class="row">
        <div class="main <?php echo kadence_main_class(); ?>" role="main">
            <?php get_template_part('templates/content', 'page'); ?>
        </div><!-- /.main -->
    </div>
</div>
