<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>
  
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WVHLQ5B');</script>
<!-- End Google Tag Manager -->

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WVHLQ5B"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <!--[if lt IE 9]>
  <script src="<?php echo get_template_directory_uri() . '/assets/js/vendor/respond.min.js';?>"></script>
  <![endif]-->
  <div id="wrapper" class="container">
    <?php get_template_part('templates/header'); ?>

    <div class="wrap contentclass" role="document">

    <!-- toma jameson -->
    <!--<div class="toma-page">
      <div class="row">
          <div class="imagen-principal col-sm-6 col-xs-10">
              <img src="<?php echo get_site_url() ?>/wp-content/themes/virtue_barman/Imagenes/jameson/C317A2488E77675.png" alt="imagen principal">
          </div>
          <div class="logo-pequeno">
              <img src="<?php echo get_site_url() ?>/wp-content/themes/virtue_barman/Imagenes/jameson/D4F4B5630EF4419B.png" alt="logo toma">
          </div>
          <div class="logo-toma">
              <img src="<?php echo get_site_url() ?>/wp-content/themes/virtue_barman/Imagenes/jameson/logoJAMESON.svg" alt="logo toma">
          </div>
      </div>
    </div>-->

    <!-- end toma -->


      <?php include kadence_template_path(); ?>
    </div><!-- /.row-->
  </div><!--Wrapper-->
  <?php get_template_part('templates/footer'); ?>
</body>
</html>
