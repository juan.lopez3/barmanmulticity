(function($, window, undefined) {
  $(window).load(function() {

    //desenfoque en overlay de entrada
    if ($("#av-overlay-wrap").hasClass("filtro-edad")) {
      console.log("OVERLAY");
      $("header.banner.headerclass").addClass("overllayy");
      $(".wrap.contentclass").addClass("overllayy");
    }
    $("header .kad-header-right ul#menu-extra-menu li a").each(function() {
      $(this).addClass("skew-forward");
    });
    $(".homecontent .col-md-4 .listing-item, .home .col-md-8 .listing-item").each(function() {
      $(this).addClass("curl-top-left");
    });


    // agregando atributo rel links header del sitio
    // este link se creo por administrador y no viene con la opcion de ese atributo
    $("li.menu-t a").attr({rel: "nofollow"});
    $("li.menu-f a").attr({rel: "nofollow"});

  });


  // AGE VERIFY
  if (getCookie("age-verified") === false) {
    $("div#av-overlay-wrap").show();
  }

  function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ')
        c = c.substring(1);
      if (c.indexOf(name) == 0)
        return true;
    }
    return false;
  }
  
  

  

})(jQuery, window);