<?php
/*
  Template Name: Buena vida
 */
?>
<div class="container">

    <?php
    $args_pag = array(
        'base' => '%_%',
        'format' => '?paged=%#%',
        'total' => 1,
        'current' => 0,
        'show_all' => false,
        'end_size' => 1,
        'mid_size' => 2,
        'prev_next' => true,
        'prev_text' => __('« Anterior'),
        'next_text' => __('Siguiente »'),
        'type' => 'plain',
        'add_args' => false,
        'add_fragment' => '',
        'before_page_number' => '',
        'after_page_number' => ''
    );
    ?>

    <?php
    $page = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array(
        'posts_per_page' => 3,
        'category_name' => 'buena-vida',
        'paged' => $page,
    );
    query_posts($args);

//productos relacionados
    p2p_type('products_to_posts')->each_connected($wp_query, array(), 'products');
    p2p_type('posts_to_posts')->each_connected($wp_query, array(), 'relateds');
    ?>

    <div class="wp-pagenavi">
<?php echo paginate_links($args); ?>
    </div>

    <?php
    while (have_posts()) : the_post();
//        $entrada_index = $wp_query->current_post + 1; //the loop 
        ?>
        <div class="row">
            <?php
//set_query_var('entrada_index', $entrada_index);
//archivo content-home.php
            get_template_part('templates/content', 'home');
            ?>

        </div>
    <?php endwhile; ?>
<?php wp_reset_query();  // Restore global post data  ?>
</div>