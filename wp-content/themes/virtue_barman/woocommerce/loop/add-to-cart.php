<?php
/**
 * Loop Add to Cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/add-to-cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;
global $wp_query;//query_var

$interaccion = "Anadir producto desde el call to action de la miniatura de producto";
if (isset($wp_query->queried_object->name)){
    switch($wp_query->queried_object->name){
        case "product":
            $interaccion = "Anadir producto al carro desde la tienda ";
            $interaccionID = HorusConf::get_var('interaccion_agregado_al_carrito_id');
            break;
        //
        
    }
}elseif(isset($wp_query->queried_object->post_type)){
    switch($wp_query->queried_object->post_type){
        case "product":
            $interaccion = "interna prodcucto productos relacionados";
            $interaccionID = HorusConf::get_var('interaccion_agregado_al_carrito_id');
            break;
        case "post":
            $interaccion = "interna de articulo calltoaction inferior";
            $interaccionID = HorusConf::get_var('interaccion_agregado_al_carrito_id');
            break;                 
    }    
}
//var_dump(array("hola",$wp_query->queried_object));

//$post_slug=$post->post_name;


$text_button_add = isset($popup) ? "Comprar" : $product->add_to_cart_text();

echo apply_filters( 'woocommerce_loop_add_to_cart_link',
	sprintf( '<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s" '
                . 'data-interaccion-id="%s" data-interaccion-agregado_desde="%s" data-interaccion-productid="%s"'
                . '>%s</a>',
		esc_url( $product->add_to_cart_url() ),
		esc_attr( isset( $quantity ) ? $quantity : 1 ),
		esc_attr( $product->id ),
		esc_attr( $product->get_sku() ),
		esc_attr( isset( $class ) ? $class : 'button' ),
                $interaccionID,
                $interaccion,
                esc_attr( $product->id ),
		esc_html( $text_button_add )
	),
$product );
