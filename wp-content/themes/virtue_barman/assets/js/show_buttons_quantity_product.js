 ( function( $ ) {
 	'use strict';
 	function wcQuantityIncrementPrepend() {
 		$( 'div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)' ).addClass( 'buttons_added' ).append( '<input type="button" value="+" class="plus" />' ).prepend( '<input type="button" value="-" class="minus" />' );
 	}
 	wcQuantityIncrementPrepend();

 	// Run on cart update
 	$( document.body ).on( 'updated_wc_div', function( event ) {
 		wcQuantityIncrementPrepend();
	} );
 	$(document).ajaxComplete(function(){
 		wcQuantityIncrementPrepend();
 	});

 } ( jQuery ) );
