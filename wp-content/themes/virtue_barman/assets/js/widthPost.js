(function ($, window){
    $(document).on('ready', function() {
        $('#content .single-article-header>div.col-sm-8').addClass('col-sm-12');
        $('#content .single-article-header>div.col-sm-8').removeClass('col-sm-8');
        $('#content .single-article>div.col-sm-8').addClass('col-sm-12');    
        $('#content .single-article>div.col-sm-8').removeClass('col-sm-8');
        $('#content .related-sidebar.col-sm-4').hide();        
    });
})(jQuery, window);
