<?php
// Exit if accessed directly
if (!defined('ABSPATH'))
    exit;

/**** Separamos en archivos mas manejables ****/
$includes = [
    'includes/barman_loginout_menu.php',
    'includes/barman_cupons.php',
    'includes/shortcode_mensajes.php',
    'includes/pippin_rember_password.php',
    'includes/barman_cambiar_precio_producto_por_entrega.php',
    'includes/shortcodes_display_posts.php',
    'includes/barman_articulos_relacionados.php',
    'includes/user_login.php',
    'includes/customizr.php',
    'includes/mobile_shortcodes.php',
    'includes/busqueda.php',
    'includes/usuario_campos_fecha_y_genero.php',
    'includes/mocion-woocommerce.php',
    'includes/barman_ajustes_front_usando_filtros.php',
    'includes/reubicar_cupon_form_in_checkout.php',
    'lib-mocion/scripts.php',
    'includes/show_buttons_quantity_product.php',
    'includes/barman_seo_pre_render.php',
    'includes/barman_image_size.php',
    'includes/barman_campo_unidades.php',
    'includes/barman_limit_image_dimension_upload.php',
    'includes/barman_size_contenedor_100.php',
    'includes/alter_product_archive_query_limit.php',
    'includes/add_product_image_to_cart.php'
];

foreach ($includes as $file) {
    if (!$filepath = locate_template($file)) {
        trigger_error(sprintf(__('Error locating %s for inclusion', 'barman'), $file), E_USER_ERROR);
    }
    require_once $filepath;
}
unset($file, $filepath);

/** registrar menus **/
function register_my_menus() {
    register_nav_menus(
            array(
                'header_menu' => __('Header Menu'),
                'extra_menu' => __('Extra Menu'),
                'footer_menu' => __('Footer Menu')
            )
    );
}


add_action('init', 'register_my_menus');

/** Agregamos los scripts necesarios **/
function theme_name_scripts() {
    wp_enqueue_script('mainjs', get_template_directory_uri() . '/mainjs.js',    array(), filemtime(dirname(__FILE__)), true);
    // wp_enqueue_script('popupoferta', get_stylesheet_directory_uri() . '/assets/js/popupOferta.js', array(), filemtime(dirname(__FILE__)), true);    
    
    wp_enqueue_script('mocionajaxdebug', get_stylesheet_directory_uri() . '/assets/js/ajaxdebug.js', array(), filemtime(dirname(__FILE__)), true);    
    wp_enqueue_style('estilosbarman', get_stylesheet_directory_uri() . '/assets/css/main.css', array(), filemtime(dirname(__FILE__)));
   
}
//El ultimo numero es la prioridad, es grande para asegurarnos que nuestros estilos cargan de ultimo
add_action('wp_enqueue_scripts', 'theme_name_scripts', 9999999);



/** AJUSTES VARIOS **/
add_theme_support('post-thumbnails');

function custom_excerpt_length($length) {
    return 100;
}
add_filter('excerpt_length', 'custom_excerpt_length', 999);



add_filter('wp_list_categories', 'add_slug_css_list_categories');
function add_slug_css_list_categories($list) {
    $cats = get_categories();
    foreach ($cats as $cat) {
        $find = 'cat-item-' . $cat->term_id . '"';
        $replace = 'category-' . $cat->slug . '"';
        $list = str_replace($find, $replace, $list);
        $find = 'cat-item-' . $cat->term_id . ' ';
        $replace = 'category-' . $cat->slug . ' ';
        $list = str_replace($find, $replace, $list);
    }
    $images = get_option('taxonomy_image_plugin');
    $cat_id = $category->term_taxonomy_id;
    return $list;
}


// Insertar Breadcrumb    
function the_breadcrumb() {
    if (!is_home()) {
        echo '<a href="';
        echo get_option('home');
        echo '">';
        bloginfo('Home');
        echo "</a> / ";
        if (is_category() || is_single()) {
            the_category('title_li=');
            if (is_single()) {
                echo " / ";
                the_title();
            }
        } elseif (is_page()) {
            echo the_title();
        }
    }
}





function search_form_shortcode() {
    return get_search_form();
}

add_shortcode('search_form', 'search_form_shortcode');

//cambiar texto de boton
add_filter('woocommerce_product_add_to_cart_text', 'woo_archive_custom_cart_button_text');    // 2.1 +

function woo_archive_custom_cart_button_text() {
    return __('Añadir al Carrito', 'woocommerce');
}

//member check
add_shortcode('member', 'member_check_shortcode');



add_filter('wpseo_pre_analysis_post_content', 'mysite_opengraph_content');

function mysite_opengraph_content($val) {
    return preg_replace("/<img[^>]+>/i", "", $val);
}

// imagen especial facebook
if (function_exists('add_image_size')) {
    add_image_size('facebook', 600, 315);
}


function my_remove_ssba_from_content($content) {
    global $post;
    if ($post !== '') {
        $post->post_content .= '[ssba_hide]';
    }
    return $content;
}

if (shortcode_exists('ssba')) {
    add_filter('the_content', 'my_remove_ssba_from_content', 9);
    add_filter('the_excerpt', 'my_remove_ssba_from_content', 9);
}


function get_the_search_excerpt() {
    $excerpt = get_the_content();
    $excerpt = preg_replace(" (\[.*?\])", '', $excerpt);
    $excerpt = strip_shortcodes($excerpt);
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, 150);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = trim(preg_replace('/\s+/', ' ', $excerpt));
    $excerpt = $excerpt . '... <a href="' . $permalink . '"> Continuar</a>';
    return $excerpt;
}


/******
  @date 2015-05-15
  Configuramos friendly-URl para la tienda
  ejemplo:
  /tienda/categoria-producto/ron
  debe llevar a product&categoria=ron
 */

add_action( 'init', 'wpse26388_rewrites_init' );
function wpse26388_rewrites_init(){
    add_rewrite_rule(
        /*'tienda/categoria-producto/(.*)',*/
        'tienda/([^/]*$)', 
        'index.php?post_type=product&categoria=$matches[1]',
        'top' );

    add_rewrite_rule(
        'categoria-producto/(.*)',
        'index.php?post_type=product&categoria=$matches[1]',
        'top' );     

    add_rewrite_rule(
        'category/(.*)',
        '$matches[1]',
        'top' );     

}


add_filter( 'query_vars', 'wpse26388_query_vars' );
function wpse26388_query_vars( $query_vars ){
    $query_vars[] = 'categoria';
    return $query_vars;
}

/******
  @date 2015-05-15
  Con esta funcion podemos leer el parametro pasado por la URL
  print get_query_var('categoria');

 */

function get_telefono_email_customer($order) {

    $cupones = $order->get_used_coupons();

    // telefono por defecto teletrade
    $telefono = "4926307";


    if (!empty($cupones)) {
        $coupons = array();
        foreach ($cupones as $key => $value) {
            $args = array(
                's' => $value,
                'post_type' => 'shop_coupon',
                'post_status' => 'publish',
            );
            $coupons[$value] = get_posts($args);
        }

        // consultamos la promocion del cupon
        $promocions = array();
        foreach ($coupons as $coupon) {
            $promocions[$coupon[0]->post_title] = array(
                'promocion_cupon_field' => get_post_meta($coupon[0]->ID, 'promocion_cupon_field', true),
            );
        }
        
        $correos = array();
        foreach ($promocions as $promocion) {
            if ($promocion["promocion_cupon_field"] != null) {
                $titulo = get_post($promocion["promocion_cupon_field"]);
                // si la promocion es CONSULTORES PERNOD id post = 4558
                if($titulo->post_title == "Consultores Pernod") {
                    $telefono = "6741506 – 3176569993";
                } else {
                    $telefono = "6784801 - 6719505  cel. 3176569992";
                }
            }
        }
    }

    // var_dump($order);
    return $telefono;
}


//Fix SSL on Post Thumbnail URLs
        function ssl_post_thumbnail_urls($url, $post_id) {
                //Skip file attachments
                if( !wp_attachment_is_image($post_id) )
                        return $url;

                //Correct protocol for https connections
                list($protocol, $uri) = explode('://', $url, 2);
                if( is_ssl() ) {
                        if( 'http' == $protocol )
                                $protocol = 'https';
                } else {
                        if( 'https' == $protocol )
                                $protocol = 'http';
                }

                return $protocol.'://'.$uri;
        }
        add_filter('wp_get_attachment_url', 'ssl_post_thumbnail_urls', 10, 2);
