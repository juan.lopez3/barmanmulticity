<?php
/**
* Scripts and stylesheets
*
* Enqueue stylesheets in the following order:
* 1. /theme/assets/css/main.css
*
* Enqueue scripts in the following order:
* 1. /theme/assets/lib/semantic/file.js'
*/

function mocion_scripts() {
    $assets = array(
        //css
        'semantic-css'       => '/assets/semantic/semantic.min.css',
        //scripts
        'mocion-scripts'     => '/assets/js/mocion-scripts.js',
        'header-mobile'      => '/assets/js/header-mobile.js',
        'semantic-js'        => '/assets/semantic/semantic.min.js',
        'picture'            => '/assets/libraries/js/picturefill.min.js',
        'buttons_quantity'   => '/assets/js/show_buttons_quantity_product.js'
    );

    /**
    * Enqueue Custom Styles
    */
    wp_enqueue_style('semantic_css', get_stylesheet_directory_uri() . $assets['semantic-css'], false, null);

    /**
    * Enqueue Custom Scripts
    */
    wp_enqueue_script('picture', get_stylesheet_directory_uri() . $assets['picture'], false, array(), null, true);
    wp_enqueue_script('semantic_js', get_stylesheet_directory_uri() . $assets['semantic-js'], array(), null, true);
    wp_enqueue_script('mocion_scripts', get_stylesheet_directory_uri() . $assets['mocion-scripts'], array(), null, true);
    wp_enqueue_script('buttons_quantity', get_stylesheet_directory_uri() . $assets['buttons_quantity'], array(), null, true);

    /**
    * Enqueue Custom Scripts - MOBILE
    */
    if (is_mobile()) :
    wp_enqueue_script('header_mobile', get_stylesheet_directory_uri() . $assets['header-mobile'], array(), null, true);
    endif;

}
add_action('wp_enqueue_scripts', 'mocion_scripts', 100);

add_action( 'init', 'my_add_shortcodes' );

function my_add_shortcodes() {

    add_shortcode( 'my-login-form', 'my_login_form_shortcode' );
}

function my_login_form_shortcode() {

    if ( is_user_logged_in() )
        return '';

    return wp_login_form( array( 'echo' => false ) );
}
