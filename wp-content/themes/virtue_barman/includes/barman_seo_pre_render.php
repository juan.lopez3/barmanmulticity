<?php 

/*
* @method: hook to change the description meta tag of site only for 
*/
function custom_seo_description($site_description) { 

	/*
		Se QUEMA la meta descripción en la página tienda
	*/
	$path=$_SERVER['REQUEST_URI'];
	$path = explode('/', $path);
	if (count($path) < 4 ) {
		foreach ($path as $key => $value) {
			if ($value == 'tienda') {
				return 'Pide Licor a domicilio en Barman Club las 24 horas. Whisky Chivas Regal, Something Special, Jamenson, Vodka Absolut,  Tequila Olmeca y mucho más.';
			}
		}
	}

	$slug = get_store_current_product_category();

	if ($slug && get_post_type() != 'product') {
		$category = get_product_category_by_slug(strtolower($slug));
		return $category['description'];
	}else{
		return $site_description;
	}

}
add_filter( 'wpseo_metadesc', 'custom_seo_description', 10, 1 );

/*
* @method: hook to change the title of site
*/
function custom_seo_title($site_title) {

	/*
		Se QUEMA el título en la página tienda
	*/
	$path=$_SERVER['REQUEST_URI'];
	$path = explode('/', $path);
	if (count($path) < 4 ) {
		foreach ($path as $key => $value) {
			if ($value == 'tienda') {
				return 'Barman Club | Licor a domicilio en Bogotá';
			}
		}
	}


	if(get_post_type() != 'product' && wpse8170_loop() == 'single') {
		return the_title() . " | " . get_bloginfo('name');
	}
	
	$slug = get_store_current_product_category();
	if ($slug && get_post_type() != 'product') {
		$category = get_product_category_by_slug(strtolower($slug));
		return $category['name'] . " | " . get_bloginfo('name');
	}else{			
		return $site_title;
	}
}
add_filter( 'wpseo_title', 'custom_seo_title', 10, 1 );


/*
* @method: Get the category of a product by slug
* @parameter: slug of category
*/
function get_product_category_by_slug($cat_slug){

	$category = get_term_by('slug', $cat_slug, 'product_cat', 'ARRAY_A');
	return $category;
}

/*
* @method: Check if page is a sale or product and apply some rules
*/
function get_store_current_product_category(){

	$actual_link = get_page_url();
	if (strpos($actual_link, 'tienda') !== false) {
		$slug = explode("/tienda/", $actual_link);
		if (isset($slug[1])){
			if (substr($slug[1], -1) == '/') {
				$slug = substr_replace($slug, "", -1);
				return $slug[1];
			}else{
				return $slug[1];
			}
		}
	}else{
		return null;
	}
}

function get_page_url() {
	$pageURL = 'http';
	if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	$pageURL .= "://";
	if ($_SERVER["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} else {
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}
	return $pageURL;
}


function wpse8170_loop() {
	global $wp_query;
	$loop = 'notfound';

	if ( $wp_query->is_page ) {
		$loop = is_front_page() ? 'front' : 'page';
	} elseif ( $wp_query->is_home ) {
		$loop = 'home';
	} elseif ( $wp_query->is_single ) {
		$loop = ( $wp_query->is_attachment ) ? 'attachment' : 'single';
	} elseif ( $wp_query->is_category ) {
		$loop = 'category';
	} elseif ( $wp_query->is_tag ) {
		$loop = 'tag';
	} elseif ( $wp_query->is_tax ) {
		$loop = 'tax';
	} elseif ( $wp_query->is_archive ) {
		if ( $wp_query->is_day ) {
			$loop = 'day';
		} elseif ( $wp_query->is_month ) {
			$loop = 'month';
		} elseif ( $wp_query->is_year ) {
			$loop = 'year';
		} elseif ( $wp_query->is_author ) {
			$loop = 'author';
		} else {
			$loop = 'archive';
		}
	} elseif ( $wp_query->is_search ) {
		$loop = 'search';
	} elseif ( $wp_query->is_404 ) {
		$loop = 'notfound';
	}

	return $loop;
}
