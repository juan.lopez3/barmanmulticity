<?php


/**
 * Mostrar el campo de fecha de cumpleaños en los campos del usuario
 * cuando se va a editar desde administrador
 */
add_action('show_user_profile', 'my_show_extra_profile_fields_birthday');
add_action('edit_user_profile', 'my_show_extra_profile_fields_birthday');

function my_show_extra_profile_fields_birthday($user) {
    ?>

    <table class="form-table">

        <tr>
            <th><label for="birthday">Fecha de nacimiento</label></th>
            <td>
                <input type="text" name="birthday" id="birthday" value="<?php echo esc_attr(get_the_author_meta('birthday', $user->ID)); ?>" class="regular-text date" />
            </td>
            
            <th><label for="gender">Genero</label></th>
            <td>
                <input type="text" name="gender" id="gender" value="<?php echo esc_attr(get_the_author_meta('gender', $user->ID)); ?>" class="regular-text date" />
            </td>            
        </tr>

    </table>
    <?php
}

add_action('personal_options_update',  'my_save_extra_profile_fields_birthday');
add_action('edit_user_profile_update', 'my_save_extra_profile_fields_birthday');

function my_save_extra_profile_fields_birthday($user_id) {
    if (!current_user_can('edit_user', $user_id))
        return false;
    update_usermeta($user_id, 'birthday', $_POST['birthday']);
}
