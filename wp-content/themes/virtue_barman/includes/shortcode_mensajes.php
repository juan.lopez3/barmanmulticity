<?php

function show_messages($atts) {

    ob_start();

    if (isset($_GET['authentication'])) {
        if ($_GET['authentication'] == 'success')
            echo "<div class='cleanlogin-notification success login-form active'><p>" . __('Successfully logged in!', 'cleanlogin') . "</p></div>";
        else if ($_GET['authentication'] == 'failed')
            echo "<div class='cleanlogin-notification error login-form active'><p>" . __('Wrong credentials', 'cleanlogin') . "</p></div>";
        else if ($_GET['authentication'] == 'logout')
            echo "<div class='cleanlogin-notification success login-form active'><p>" . __('Successfully logged out!', 'cleanlogin') . "</p></div>";
    }



    if (isset($_GET['updated'])) {
        if ($_GET['updated'] == 'success')
            echo "<div class='cleanlogin-notification success'><p>" . __('Information updated', 'cleanlogin') . "</p></div>";
        else if ($_GET['updated'] == 'wrongpass')
            echo "<div class='cleanlogin-notification error'><p>" . __('Passwords must be identical', 'cleanlogin') . "</p></div>";
        else if ($_GET['updated'] == 'wrongmail')
            echo "<div class='cleanlogin-notification error'><p>" . __('Error updating email', 'cleanlogin') . "</p></div>";
        else if ($_GET['updated'] == 'failed')
            echo "<div class='cleanlogin-notification error'><p>" . __('Something strange has ocurred', 'cleanlogin') . "</p></div>";
    }

    if (isset($_GET['created'])) {
        if ($_GET['created'] == 'success')
            echo "<div class='cleanlogin-notification success'><p>" . __('Te has registrado exitosamente', 'cleanlogin') . "</p></div>";
        else if ($_GET['created'] == 'created')
            echo "<div class='cleanlogin-notification success'><p>" . __('Nuevo usuario creado', 'cleanlogin') . "</p></div>";
        else if ($_GET['created'] == 'wronguser')
            echo "<div class='cleanlogin-notification error'><p>" . __('Nombre de usuario no válido o usuario ya registrado', 'cleanlogin') . "</p></div>";
        else if ($_GET['created'] == 'wrongpass')
            echo "<div class='cleanlogin-notification error'><p>" . __('Las contraseñas deben ser identicas', 'cleanlogin') . "</p></div>";
        else if ($_GET['created'] == 'wrongmail')
            echo "<div class='cleanlogin-notification error'><p>" . __('Email inválido', 'cleanlogin') . "</p></div>";
        else if ($_GET['created'] == 'wrongcaptcha')
            echo "<div class='cleanlogin-notification error'><p>" . __('CAPTCHA is not valid, please try again', 'cleanlogin') . "</p></div>";
        else if ($_GET['created'] == 'failed')
            echo "<div class='cleanlogin-notification error'><p>" . __('Error al crear usuario', 'cleanlogin') . "</p></div>";
        else if ($_GET['created'] == 'useralreadyexistinhorus')    
        echo "<div class='cleanlogin-notification error'><p>" . __('El usuario ya existe', 'cleanlogin') . "</p></div>";
        
    }


    if (isset($_GET['sent'])) {
        if ($_GET['sent'] == 'success')
            echo "<div class='cleanlogin-notification success'><p>" . __('Recibirá un e-mail con el link de activación', 'cleanlogin') . "</p></div>";
        else if ($_GET['sent'] == 'sent')
            echo "<div class='cleanlogin-notification success'><p>" . __('Recibirá un e-mail con el link de activación', 'cleanlogin') . "</p></div>";
        else if ($_GET['sent'] == 'failed')
            echo "<div class='cleanlogin-notification error'><p>" . __('Se ha encontrado un error en el envío de e-mail', 'cleanlogin') . "</p></div>";
        else if ($_GET['sent'] == 'wronguser')
            echo "<div class='cleanlogin-notification error'><p>" . __('Username is not valid', 'cleanlogin') . "</p></div>";
    }

    return ob_get_clean();
}

add_shortcode('mensajes', 'show_messages');
