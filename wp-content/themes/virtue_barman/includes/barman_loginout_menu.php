<?php 

add_filter( 'wp_nav_menu_items', 'add_loginout_link', 10, 2 );
function add_loginout_link( $items, $args ) {
    if (is_user_logged_in() && $args->theme_location == 'primary_navigation') {
        $items = '<li><a href="'. wp_logout_url(home_url() ) .'">Salir</a></li>'.$items ;
    }
    elseif (!is_user_logged_in() && $args->theme_location == 'primary_navigation') {
		//'. site_url('wp-login.php') .'
		//wp_registration_url()
        $items = '<li><a class="open-login-form" href="#" >Ingresar</a></li>'.$items ;
        $items = '<li><a href="'.get_site_url().'/registro">Registrarse</a></li>'.$items ;
    }
    return $items;
}
