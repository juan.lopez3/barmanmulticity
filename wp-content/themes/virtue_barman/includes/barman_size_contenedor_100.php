<?php
add_action( 'wp_enqueue_scripts', 'size_content_100' );
function size_content_100() {
    /*Ancho del contenedor del post al 100%*/
    $checked = get_post_field('full_width', get_the_ID());
    if($checked){
        wp_enqueue_script('widhtPost', get_stylesheet_directory_uri() . '/assets/js/widthPost.js', array(), filemtime(dirname(__FILE__)), true);
    }
}

