<?php

add_filter('display_posts_shortcode_output', 'be_display_posts_favorites', 10, 7);
function be_display_posts_favorites($output, $atts, $image, $title, $date, $excerpt, $inner_wrapper) {

    // See if 'full_content' is set in shortcode. 
    // Ex: [display-posts full_content="true"]
    if (isset($atts['favorites']) && "true" == $atts['favorites'])
        $date = '<div><div class="favoritos">   ' . wpfp_link(1) . '</div></div>';

    // Now let's rebuild the output. Only the excerpt changed so we're using the original $image, $title, and $date
    $output = '<' . $inner_wrapper . ' class="listing-item">' . $image . $title . $date . $excerpt . '</' . $inner_wrapper . '>';


    // Finally we'll return the modified output
    return $output;
}

add_filter('display_posts_shortcode_output', 'be_display_posts_product', 12, 7);

function be_display_posts_product($output, $atts, $image, $title, $date, $excerpt, $inner_wrapper) {
    if (isset($atts['product']) && "true" == $atts['product']) {
        $tit = do_shortcode(get_the_id());
        $pot = get_post($tit, OBJECT);
        $connected = new WP_Query(array(
            'connected_type' => 'products_to_posts',
            'connected_items' => $pot,
            'nopaging' => true,
        ));

        if (is_mobile() == false) {
            $sharef = do_shortcode('[ssba]');
        } else {
            $sharef = do_shortcode('[ssba]');
        }
        $p = isset($connected->posts[0]) ? $connected->posts[0] : $connected;

        $pr = '<div class="att"><div class="sharebe"><p>Compartir</p>' . $sharef . '</div>';
//        if (is_mobile() == false) {

            $pr .= '<div class="imagen_product_post">' . get_the_post_thumbnail($p->ID, 'shop_thumbnail') . '</div><div class="attached-product"><div class="preparalo"><p>Mejor con </p><div class="titulo_producto">' . $p->post_title . '</div></div>

            <div class="comprar"><a href="' . get_permalink($p->ID) . '">Comprar</a></div>
        </div>';
//        }
        $pr .= '</div>';

        // Now let's rebuild the output. Only the excerpt changed so we're using the original $image, $title, and $date
        $output = '<' . $inner_wrapper . ' class="listing-item">' . $image . $title . $date . $excerpt . $pr . '</' . $inner_wrapper . '>';
    }
    // Finally we'll return the modified output

    return $output;
}

add_filter('display_posts_shortcode_output', 'be_display_posts_related', 13, 7);
function be_display_posts_related($output, $atts, $image, $title, $date, $excerpt, $inner_wrapper) {
    if (isset($atts['related']) && "true" == $atts['related']) {

        $tit = do_shortcode(get_the_id());
        $pot = get_post($tit, OBJECT);
        if (class_exists('Dynamic_Featured_Image')) {
            global $dynamic_featured_image;

            $featured_image = $dynamic_featured_image->get_featured_images($tit);
            $image = '<a href="' . get_permalink($tit) . '" ><img src="' . $featured_image[0]['full'] . '" alt="'.get_the_title($pot).'"  /></a>';
            //You can now loop through the image to display them as required
        }


        // Now let's rebuild the output. Only the excerpt changed so we're using the original $image, $title, and $date
        $output = '<' . $inner_wrapper . ' class="listing-item">' . $image . $title . $date . '</' . $inner_wrapper . '>';
    }

    // Finally we'll return the modified output
    return $output;
}

add_filter('display_posts_shortcode_output', 'be_display_posts_full_content', 11, 7);
function be_display_posts_full_content($output, $atts, $image, $title, $date, $excerpt, $inner_wrapper) {

    // See if 'full_content' is set in shortcode. 
    // Ex: [display-posts full_content="true"]
    if (isset($atts['full_content']) && "true" == $atts['full_content'])
        $excerpt = '<div class="listing-content">' . do_shortcode(get_the_content()) . '</div>';

    // Now let's rebuild the output. Only the excerpt changed so we're using the original $image, $title, and $date
    $output = '<' . $inner_wrapper . ' class="listing-item">' . $image . $title . $date . $excerpt . '</' . $inner_wrapper . '>';

    // Finally we'll return the modified output
    return $output;
}
