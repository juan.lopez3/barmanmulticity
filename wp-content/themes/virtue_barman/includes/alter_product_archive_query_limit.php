<?php


add_action( 'woocommerce_product_query', 'mocion_woocommerce_product_query', 2, 50 );

	/**
	 * Posts per page.
	 *
	 * Set the number of posts per page on a hard way, build in fix for many themes who override the offical loop_shop_per_page filter.
	 *
	 * @since 1.2.0
	 *
	 * @param	object 	$q		Existing query object.
	 * @param	object	$class	Class object.
	 * @return 	object 			Modified query object.
	 */
	function mocion_woocommerce_product_query( $q, $class ) {

		if ( function_exists( 'woocommerce_products_will_display' ) && woocommerce_products_will_display() && $q->is_main_query() ) :
			$q->set( 'posts_per_page', 10000 );
		endif;

	}

