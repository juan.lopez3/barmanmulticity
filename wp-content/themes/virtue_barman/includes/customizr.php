<?php
add_filter('tc_default_widgets', 'add_featured_page_widget');

function add_featured_page_widget($defaults) {
    $defaults['fp_widgets'] = array(
        'name' => __('Featured Pages Widget', 'customizr'),
        'description' => __('Above the featured pages area on home', 'customizr')
    );
    return $defaults;
}

add_action('__before_fp', 'display_my_fp_widget');

function display_my_fp_widget() {
    dynamic_sidebar('fp_widgets');
}

add_filter('tc_post_list_layout', 'my_post_list_layout_options');

function my_post_list_layout_options($layout) {

    //extracts the array as variables
    extract($layout, EXTR_OVERWRITE);

    //set the custom layout parameters
    $content = 'span9';
    $thumb = 'span3'; //<= the sum of content and thumb span suffixes (span[*]) must equal 12 otherwise the content and thumbnail blocks will no fit correctly in any viewport
    $show_thumb_first = true;
    $alternate = wp_is_mobile() ? false : true; //<= this will desactivate the alternate thumbnail / content in mobile viewports
    //returns the recreated $layout array containing your custom variables values
    return compact("content", "thumb", "show_thumb_first", "alternate");
}

