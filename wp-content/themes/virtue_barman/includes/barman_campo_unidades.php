<?php
/*-----------------------------------------------------------------------------------*/
/* Agregar campo para el label  */
/* barman_campo_unidades
-----------------------------------------------------------------------------------*/

//se crea el campo en la pestaña general
add_action( 'woocommerce_product_options_pricing', 'wc_umedida_product_field' );
function wc_umedida_product_field() {
	woocommerce_wp_text_input( array(
	'id' => 'unidad',
	'class' => 'wc_input_unidad short',
	'label' => __( 'Unidad de medida (UM)', 'woocommerce' ),
	'placeholder' => 'mililitros'
	)
	);
}

//guardamos los datos ingresados en el campo
add_action( 'save_post', 'wc_umedida_save_product' );
function wc_umedida_save_product( $product_id ) {
	// si tiene autoguardado no ahcer nada, solo se guarda cuando hacemos click en update
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
		return;
	if ( isset( $_POST['unidad'] ) ) {
		update_post_meta( $product_id, 'unidad', $_POST['unidad'] );
	} else delete_post_meta( $product_id, 'unidad' );
}
