<?php
add_filter ( 'wp_handle_upload_prefilter', 'tc_handle_upload_prefilter' );
function tc_handle_upload_prefilter($file) {
	if (get_post_type ( $_REQUEST ['post_id'] ) == 'product') {
		$img = getimagesize ( $file ['tmp_name'] );
		$minimum = array (
				'width' => '243',
				'height' => '675' 
		);
		$width = $img [0];
		$height = $img [1];
		
		if ($width != $minimum ['width'])
			return array (
					"error" => "La imagen no concuerda con los tamaños requeridos." 
			);
		
		elseif ($height != $minimum ['height'])
			return array (
					"error" => "La imagen no concuerda con los tamaños requeridos." 
			);
		else
			return $file;
	}
	return $file;
}