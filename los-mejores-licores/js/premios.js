// JavaScript Document



//NAV SECCIONES SMOOTH

var $root = $('html, body');
$('a').click(function() {
    var href = $.attr(this, 'href');
    $root.animate({
        scrollTop: $(href).offset().top
    }, 800, function () {
        window.location.hash = href;
    });
    return false;
});






/*boton nav resaltado*/

var sections = $('section'),
  nav = $('nav'),
  nav_height = nav.outerHeight();
 
$(window).on('scroll', function () {
  "use strict";	
  var cur_pos = $(this).scrollTop();
 
  sections.each(function() {
    var top = $(this).offset().top + -410,
        bottom = top + $(this).outerHeight();
 
    if (cur_pos >= top && cur_pos <= bottom) {
      nav.find('a').removeClass('active');
      sections.removeClass('active');
 
      $(this).addClass('active');
      nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active');
    }
  });
});



/* acordion */
var acc = document.getElementsByClassName("accordionx");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        this.nextElementSibling.classList.toggle("show");
    }
}








//ocultar-mostrar nav
$(window).scroll(function(){
	"use strict";
	if ($(this).scrollTop() > 280) {
		$('.cajaBotonesNav').fadeIn();
		} else {
	$('.cajaBotonesNav').fadeOut();
	}
});



/*menu header mobile*/

$( ".kad-navbtn" ).click(function() {
  $( ".kad-nav-collapse" ).slideToggle( "slow" );
});